# Python environment

## Python using virtualenv

Based on python 3.9, one can setup a virtual environments. 

```bash
$ pip3 -V
pip 20.3.4 from /usr/lib/python3/dist-packages/pip (python 3.9)
$ python3 -m pip install virtualenv
```

A virtual environment can be created in order to use this module: 

```bash
$ python3 -m virtualenv ci-uprev.venv
$ source ci-uprev.venv/bin/activate
(...)
$ python -m pip install python-gitlab validators pint tenacity pytest pytest-timeout bump2version
$ pip list 
Package            Version
------------------ ---------
attrs              22.1.0
bump2version       1.0.1
certifi            2022.6.15
charset-normalizer 2.1.0
decorator          5.1.1
idna               3.3
iniconfig          1.1.1
packaging          21.3
Pint               0.19.2
pip                22.1.2
pluggy             1.0.0
py                 1.11.0
pyparsing          3.0.9
pytest             7.1.2
pytest-timeout     2.1.0
python-gitlab      3.8.0
requests           2.28.1
requests-toolbelt  0.9.1
setuptools         63.1.0
tenacity           8.0.1
tomli              2.0.1
urllib3            1.26.11
validators         0.20.0
wheel              0.37.1

$ deactivate
```

To reproduce the same virtual environment that this tools is being tested:

```bash
$ pip install -r requirements.txt
```

### Export changes on the environment

In case some newer packages are required, the easiest way to mark for others 
to use is:

```bash
pip freeze > requirements.txt
```

## Conda

With different *pros and cons*, alternatively to use virtual environments, it 
can be also setup a conda environment for this project use.

### Get conda

It is enough with the use of 
[miniconda](https://docs.conda.io/en/latest/miniconda.html) for linux.

### Create `ci-uprev.git` environment

The easiest way is to get it from 
the `environment.yml` file stored in the repo.

```bash
conda env create -f environment.yml
conda activate ci-uprev 
```

But one can create it from scratch by:

```bash
conda create -c conda-forge --name ci-uprev python=3.10 python-gitlab validators pint tenacity pytest pytest-timeout bump2version -y
conda activate ci-uprev
```

### Export changes on the environment

In case some newer packages are required, the easiest way to mark for others 
to use is:

```bash
conda env export | grep -v "^prefix: " > environment.yml
```
