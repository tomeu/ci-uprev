#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from abstract import UprevTestCase
from fakegitlab import create_random_fake_token
import pytest
from test_gitlabwrapper import _build_gitlab_obj
from unittest import main
from uprev.updaterevision.gitlabwrapper import GitlabWrapper
from uprev.updaterevision.revisionchecker import RevisionChecker


class TestUpdateRevision(UprevTestCase):

    @pytest.mark.timeout(120)
    def test_updaterevision_mesa_in_virglrenderer(self):
        self._prepare_logger("mesa_in_virglrenderer")
        self._prepare_environment({
            'GITLAB_URL': "https://gitlab.freedesktop.org/",
            'ORIGIN_GITLAB_FORK_PROJECT_PATH': "sergi/virglrenderer",
            'ORIGIN_GITLAB_MAIN_PROJECT_PATH': "virgl/virglrenderer",
            'ORIGIN_GITLAB_TOKEN': create_random_fake_token(),
            'REVISION_GITLAB_PROJECT_PATH': "mesa/mesa",
        })
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        RevisionChecker(debug=True, trace=True)

    @pytest.mark.timeout(120)
    def test_updaterevision_piglit_in_mesa(self):
        self._prepare_logger("piglit_in_mesa")
        self._prepare_environment({
            'GITLAB_URL': "https://gitlab.freedesktop.org/",
            'ORIGIN_GITLAB_FORK_PROJECT_PATH': "sergi/mesa",
            'ORIGIN_GITLAB_MAIN_PROJECT_PATH': "mesa/mesa",
            'ORIGIN_GITLAB_TOKEN': create_random_fake_token(),
            'REVISION_GITLAB_PROJECT_PATH': "mesa/piglit",
        })
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        RevisionChecker(debug=True, trace=True)

    # @pytest.mark.timeout(120)
    # def test_notimplemented(self):
    #     self._prepare_logger("notimplemented")
    #     self._prepare_environment({
    #         'GITLAB_URL': "https://gitlab.freedesktop.org/",
    #         'ORIGIN_GITLAB_FORK_PROJECT_PATH': "sergi/bar",
    #         'ORIGIN_GITLAB_MAIN_PROJECT_PATH': "mesa/bar",
    #         'ORIGIN_GITLAB_TOKEN': create_random_fake_token(),
    #         'REVISION_GITLAB_PROJECT_PATH': "mesa/foo",
    #     })
    #     GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
    #     RevisionChecker(debug=True, trace=True)


if __name__ == '__main__':
    main()
