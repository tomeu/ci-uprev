#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from abstract import UprevTestCase
from fakegitlab import (Gitlab, ProjectBranch, ProjectCommit, ProjectFile,
                        ProjectIssue, ProjectJob, ProjectMergeRequest,
                        ProjectPipeline,
                        create_random_fake_token, create_random_fake_sha1_hash)
from random import randint
from unittest import main
from uprev.updaterevision.gitlabwrapper import GitlabWrapper

ORIGIN_URL_SAMPLE = "https://gitlab.freedesktop.org/"
ORIGIN_PROJECT_NAME_SAMPLE = "sergi/virlgrenderer"
REVISION_URL_SAMPLE = ORIGIN_URL_SAMPLE
REVISION_PROJECT_SAMPLE = "sergi/mesa"


def _build_gitlab_obj(self, url, private_token=None):
    return Gitlab(url, private_token=private_token)


class TestGitlabWrapper(UprevTestCase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._prepare_logger("TestGitlabWrapper")

    def assertGitlabWrapper(self, _wrapper, _url, _project):
        self.assertEqual(_wrapper.url, _url)
        self.assertEqual(_wrapper.project, _project)
        self.assertIsNotNone(_wrapper.project_id)
        self.assertEqual(_wrapper.full_url, f"{_url}{_project}")
        # TODO: if there is a token:
        #   TODO: create a new branch
        #   TODO: get a file from the branch
        #   TODO: modify the file in the branch
        #   TODO: commit the file to the branch
        #   TODO: get the previously created branch
        #   TODO: get the previously send commit
        #   TODO: run a pipeline from the previous commit
        #   TODO: see the jobs of the pipeline
        #   TODO: create a merge request from the branch to another
        #   TODO: create an issue
        # else:
        #   TODO: list branches
        #   TODO: list available pipelines
        #   TODO: list commits
        #   TODO: list merge requests
        #   TODO: list issues

    def test_gitlabwrapper_build_with_token(self):
        # prepare area
        fake_token = create_random_fake_token()
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        wrapper = GitlabWrapper(ORIGIN_URL_SAMPLE, ORIGIN_PROJECT_NAME_SAMPLE,
                                token=fake_token, debug=True)
        self.assertIsInstance(wrapper._GitlabWrapper__gitlab_obj, Gitlab)
        self.assertGitlabWrapper(wrapper, ORIGIN_URL_SAMPLE,
                                 ORIGIN_PROJECT_NAME_SAMPLE)

    def test_gitlabwrapper_build_without_token(self):
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        wrapper = GitlabWrapper(REVISION_URL_SAMPLE, REVISION_PROJECT_SAMPLE,
                                debug=True)
        self.assertGitlabWrapper(wrapper, REVISION_URL_SAMPLE,
                                 REVISION_PROJECT_SAMPLE)

    # FIXME: those will be absorbed by the TODOs in assertGitlabWrapper

    def test_get_an_existing_branch(self):
        _project = "bar/foo"
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        wrapper = GitlabWrapper(ORIGIN_URL_SAMPLE, _project, debug=True)
        branch = wrapper.branches.get('barfoo')
        self.assertIsInstance(branch, ProjectBranch)

    def test_get_an_existing_commit(self):
        _project = "bar/foo"
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        wrapper = GitlabWrapper(ORIGIN_URL_SAMPLE, _project, debug=True)
        commit = wrapper.commits.get(create_random_fake_sha1_hash())
        self.assertIsInstance(commit, ProjectCommit)

    def test_get_an_existing_file(self):
        _project = "bar/foo"
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        wrapper = GitlabWrapper(ORIGIN_URL_SAMPLE, _project, debug=True)
        commit = wrapper.files.get(".gitlab-ci.yml", ref='master')
        self.assertIsInstance(commit, ProjectFile)

    def test_get_an_existing_issue(self):
        _project = "bar/foo"
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        wrapper = GitlabWrapper(ORIGIN_URL_SAMPLE, _project, debug=True)
        commit = wrapper.issues.get(randint(1, 10))
        self.assertIsInstance(commit, ProjectIssue)

    def test_get_an_existing_job(self):
        _project = "bar/foo"
        fake_token = create_random_fake_token()
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        wrapper = GitlabWrapper(ORIGIN_URL_SAMPLE, _project,
                                token=fake_token, debug=True)
        commit = wrapper.jobs.get("bar")
        self.assertIsInstance(commit, ProjectJob)

    def test_get_an_existing_merge_request(self):
        _project = "bar/foo"
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        wrapper = GitlabWrapper(ORIGIN_URL_SAMPLE, _project, debug=True)
        commit = wrapper.mergerequests.get(randint(1, 10))
        self.assertIsInstance(commit, ProjectMergeRequest)

    def test_get_an_existing_pipeline(self):
        _project = "bar/foo"
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        wrapper = GitlabWrapper(ORIGIN_URL_SAMPLE, _project, debug=True)
        commit = wrapper.pipelines.get(randint(1, 10))
        self.assertIsInstance(commit, ProjectPipeline)


if __name__ == '__main__':
    main()
