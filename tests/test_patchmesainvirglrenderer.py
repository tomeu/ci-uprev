#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from abstract import UprevTestCase
from fakegitlab import (create_random_fake_token, create_random_fake_sha1_hash,
                        Gitlab)
from test_gitlabwrapper import _build_gitlab_obj
from unittest import main
from uprev.updaterevision.gitlabwrapper import GitlabWrapper
from uprev.updaterevision.pairs import build_uprev_pair

GITLAB_URL = "https://gitlab.freedesktop.org/"
GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj


class TestPatchMesaInVirglrenderer(UprevTestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._prepare_logger("TestPatchMesaInVirglrenderer")

    def test_patch_mesa_in_virglrenderer(self):
        origin_project_obj = GitlabWrapper(
            url=GITLAB_URL, project='sergi/virglrenderer',
            token=create_random_fake_token())
        revision_project_obj = GitlabWrapper(
            url=GITLAB_URL, project='sergi/mesa')
        uprev_obj = build_uprev_pair(origin_project_obj, revision_project_obj)
        commit_sha = create_random_fake_sha1_hash()
        pipeline_id = "999999"
        uprev_obj.patch_constructor(
            uprev_obj, commit_sha, pipeline_id, debug=True)



if __name__ == '__main__':
    main()
