#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from abstract import UprevTestCase
from fakegitlab import create_random_fake_token, create_random_fake_sha1_hash
from os import environ as os_environ
import pytest
from random import randint
from test_gitlabwrapper import _build_gitlab_obj
from unittest import main
from uprev.updaterevision.gitlabwrapper import GitlabWrapper
from uprev.updaterevision.pairs import build_uprev_pair
from uprev.updaterevision.uprevpatch import PatchMesaInVirglrenderer
from uprev.updaterevision.workingbranch import WorkingBranch

ORIGIN_URL_SAMPLE = "https://gitlab.freedesktop.org/"
ORIGIN_PROJECT_SAMPLE = "sergi/virglrenderer"
REVISION_URL_SAMPLE = ORIGIN_URL_SAMPLE
REVISION_PROJECT_SAMPLE = "sergi/mesa"
ORIGIN_BRANCH_NAME = "main"
WORKBRANCH_NAME = "update-mesa"


class TestWorkingBranch(UprevTestCase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._prepare_logger("TestWorkingBranch")

    @pytest.mark.timeout(120)
    def test_workingbranch_merge_weekday(self):
        self._prepare_environment(
            {'GITLAB_URL': "https://gitlab.freedesktop.org/",
             'ORIGIN_GITLAB_FORK_PROJECT_PATH': "sergi/virglrenderer",
             'ORIGIN_GITLAB_MAIN_PROJECT_PATH': "virgl/virglrenderer",
             'REVISION_GITLAB_PROJECT_PATH': "sergi/mesa"})
        token = create_random_fake_token()
        GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
        origin_wrapper = GitlabWrapper(ORIGIN_URL_SAMPLE, ORIGIN_PROJECT_SAMPLE,
                                       token=token, debug=True)
        revision_wrapper = GitlabWrapper(REVISION_URL_SAMPLE,
                                         REVISION_PROJECT_SAMPLE)
        uprev_obj = build_uprev_pair(origin_wrapper, revision_wrapper)
        fake_commit = create_random_fake_sha1_hash()
        fake_pipeline_id = randint(700000, 800000)
        patch_obj = uprev_obj.patch_constructor(
            uprev_obj, fake_commit, fake_pipeline_id, debug=True)
        workingbranch = WorkingBranch(uprev_obj, patch_obj, ORIGIN_BRANCH_NAME,
                                      debug=True, trace=True)
        # TODO: test the possibilities in the MERGE_WEEKDAY
        workingbranch.proceed()
        # TODO: currently showing a working uprev

    # TODO: failed pipelines
    #  - CI cause
    #  - TestFail cause
    #  - Unknown cause
    # TODO: UnexpectedPass


if __name__ == '__main__':
    main()
