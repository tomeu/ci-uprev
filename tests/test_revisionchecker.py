#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from abstract import UprevTestCase
from fakegitlab import create_random_fake_token
import pytest
from unittest import main
from uprev.abstract.envvars import origin_workbranch_prefix, mergerequest_title
from uprev.updaterevision.revisionchecker import RevisionChecker
from uprev.updaterevision.gitlabwrapper import GitlabWrapper
from test_gitlabwrapper import _build_gitlab_obj


def _build_wrapper(self, _url, _project, _token=None):
    GitlabWrapper._GitlabWrapper__build_gitlab_obj = _build_gitlab_obj
    return GitlabWrapper(_url, _project, _token, debug=True, trace=True)


class TestRevisionChecker(UprevTestCase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._prepare_logger("TestRevisionChecker")

    @pytest.mark.timeout(120)
    def test_revisionchecker_force_pipeline_to_uprev(self):
        self._prepare_environment({
            'GITLAB_URL': "https://gitlab.freedesktop.org/",
            'ORIGIN_GITLAB_FORK_PROJECT_PATH': "sergi/virglrenderer",
            'ORIGIN_GITLAB_TOKEN': create_random_fake_token(),
            'REVISION_GITLAB_PROJECT_PATH': "sergi/mesa",
            'REVISION_FIX_PIPELINE_ID': "999999",
            'CI_PROJECT_ID': "16591",
            'UPREV_GITLAB_TOKEN': create_random_fake_token(),
        })
        RevisionChecker._RevisionChecker__build_wrapper = _build_wrapper
        RevisionChecker(debug=True, trace=True)

    # FIXME: due to the random ids used when generating things in fakegitlab
    #  this test can never end. This has to be fixed before enable it back.
    # @pytest.mark.timeout(120)
    # def test_revisionchecker_initial_pipeline_search(self):
    #     self._prepare_environment({
    #         'GITLAB_URL': "https://gitlab.freedesktop.org/",
    #         'ORIGIN_GITLAB_FORK_PROJECT_PATH': "sergi/virglrenderer",
    #         'ORIGIN_GITLAB_TOKEN': create_random_fake_token(),
    #         'REVISION_GITLAB_PROJECT_PATH': "sergi/mesa",
    #         'REVISION_FIX_PIPELINE_ID': "",  # clean previous set
    #         'REVISION_INITIAL_PIPELINE_SEARCH': "850000",
    #         'CI_PROJECT_ID': "16591",
    #         'UPREV_GITLAB_TOKEN': create_random_fake_token(),
    #     })
    #     RevisionChecker._RevisionChecker__build_wrapper = _build_wrapper
    #     RevisionChecker(debug=True, trace=True)


if __name__ == '__main__':
    main()
