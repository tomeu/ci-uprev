#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"


# class MesaJob:
#     __name = None
#     __stage = None
#     __status = None
#     __number = None
#     __depends_on = None
#
#     def __init__(self, name, stage, initial_status, number=None,
#                  dependends_on=None):
#         self.__name = name
#         self.__stage = stage
#         self.__status = initial_status
#         self.__number = number
#         if isinstance(dependends_on, list) and len(dependends_on) > 0:
#             self.__depends_on = dependends_on
#
#     @property
#     def name(self):
#         return self.__name
#
#     @property
#     def stage(self):
#         return self.__stage
#
#     @property
#     def number(self):
#         return self.__number
#
#     @property
#     def dependends_on(self):
#         return self.__depends_on
#
#
# FIXME: this is handmade, and it doesn't scale to know changes. Would be nice
#  if we have somehow a tool to gather this from mesa CI.
mesa_jobs_builder = {
    'manual':
        {'container': {
            'debian/x86_build-base': None,
            'debian/x86_test-base': None,
            'debian/arm_build': None,
            'fedora/x86_build': None,
            'windows_vs2019': None}},
    'created':
        {'container': {
            'debian/x86_build': {
                'depends_on':
                    ['debian/x86_build-base']},
            'debian/i386_build': {
                'depends_on':
                    ['debian/x86_build-base']},
            'debian/x86_build-mingw':
                {'depends_on':
                    ['debian/x86_build-base']},
            'debian/ppc64el_build': {
                'depends_on':
                    ['debian/x86_build-base']},
            'debian/s390x_build': {
                'depends_on':
                    ['debian/x86_build-base']},
            'debian/android_build': {
                'depends_on':
                    ['debian/x86_build-base']},
            'debian/x86_test-gl': {
                'depends_on':
                    ['debian/x86_test-base']},
            'debian/x86_test-vk': {
                'depends_on':
                    ['debian/x86_test-base']},
            'kernel+rootfs_amd64': {
                'depends_on':
                    ['debian/x86_build-base']},
            'kernel+rootfs_arm64': {
                'depends_on':
                    ['debian/arm_build']},
            'kernel+rootfs_armhf': {
                'depends_on':
                    ['debian/arm_build']},
            'debian/arm_test': {
                'depends_on':
                    ['kernel+rootfs_arm64', 'kernel+rootfs_armhf']},
            'windows_build_vs2019': {
                'depends_on':
                    ['windows_vs2019']},
            'windows_test_vs2019': {
                'depends_on':
                    ['windows_vs2019']}},
         'build-x86_64': {
             'debian-testing': {'depends_on': []},
             'debian-testing-asan': {'depends_on': []},
             'debian-testing-msan': {'depends_on': []},
             'debian-clover-testing': {'depends_on': []},
             'debian-gallium': {'depends_on': []},
             'debian-release': {'depends_on': []},
             'fedora-release': {'depends_on': []},
             'debian-clang': {'depends_on': []},
             'debian-clover': {'depends_on': []},
             'debian-vulkan': {'depends_on': []}},
         'build-misc': {
             'debian-android': {'depends_on': []},
             'debian-armhf': {'depends_on': []},
             'debian-arm64': {'depends_on': []},
             'debian-arm64-asan': {'depends_on': []},
             'debian-arm64-build-test': {'depends_on': []},
             'windows-vs2019': {'depends_on': []},
             'debian-i386': {'depends_on': []},
             'debian-s390x': {'depends_on': []},
             'debian-ppc64el': {'depends_on': []},
             'debian-mingw32-x86_64': {'depends_on': []}},
         # 'amd': {'radv_stoney_vkcts:amd64': {'number': 3, 'depends_on': []},
         #         'vkcts-kabini-valve': {'unlock': 'manual', 'depends_on': []},
         #         'vkcts-polaris10-valve': {
         #             'unlock': 'manual',
         #             'depends_on': []},
         #         'vkcts-vega10-valve': {'unlock': 'manual', 'depends_on': []},
         #         'vkcts-renoir-valve': {'unlock': 'manual', 'depends_on': []},
         #         'vkcts-navi10-valve': {'unlock': 'manual', 'depends_on': []},
         #         'vkcts-navi21-valve': {'unlock': 'manual', 'depends_on': []},
         #         'vkcts-navi21-llvm-valve': {
         #             'unlock': 'manual',
         #             'depends_on': []},
         #         'vkcts-vangogh-valve': {'unlock': 'manual', 'depends_on': []},
         #         'glcts-navi10-valve': {'unlock': 'manual', 'depends_on': []},
         #         'gles-navi10-valve': {'unlock': 'manual', 'depends_on': []},
         #         'radv-fossils': {'depends_on': []},
         #         'vkd3d-kabini-valve': {'unlock': 'manual', 'depends_on': []},
         #         'vkd3d-polaris10-valve': {
         #             'unlock': 'manual',
         #             'depends_on': []},
         #         'vkd3d-vega10-valve': {'unlock': 'manual', 'depends_on': []},
         #         'vkd3d-renoir-valve': {'unlock': 'manual', 'depends_on': []},
         #         'vkd3d-navi10-valve': {'unlock': 'manual', 'depends_on': []},
         #         'vkd3d-navi21-valve': {'unlock': 'manual', 'depends_on': []},
         #         'vkd3d-vangogh-valve': {'unlock': 'manual', 'depends_on': []},
         #         'radeonsi-stoney-gl:amd64': {'number': 5, 'depends_on': []},
         #         'radeonsi-stoney-traces:amd64': {'depends_on': []},
         #         'radeonsi-stoney-piglit-gl:amd64': {'depends_on': []},
         #         'radeonsi-stoney-va:amd64': {'depends_on': []}},
         # 'intel': {'crocus-g41': {'unlock': 'manual', 'depends_on': []},
         #           'crocus-hsw': {'unlock': 'manual', 'depends_on': []},
         #           'crocus-g41-traces': {'unlock': 'manual', 'depends_on': []},
         #           'crocus-hsw-traces': {'unlock': 'manual', 'depends_on': []},
         #           'i915-g33': {'unlock': 'manual', 'depends_on': []},
         #           'i915-g33-traces': {'unlock': 'manual', 'depends_on': []},
         #           'anv-tgl-vk': {'number': 10, 'depends_on': []},
         #           'iris-apl-deqp': {
         #               'number': 3,
         #               'unlock': 'manual',
         #               'depends_on': []},
         #           'iris-apl-egl': {'depends_on': []},
         #           'iris-glk-deqp': {'number': 2},
         #           'iris-glk-egl': {'depends_on': []},
         #           'iris-amly-deqp': {
         #               'number': 3,
         #               'unlock': 'manual',
         #               'depends_on': []},
         #           'iris-kbl-deqp': {'number': 3, 'depends_on': []},
         #           'iris-whl-deqp': {'number': 4, 'depends_on': []},
         #           'iris-cml-deqp': {'number': 4, 'depends_on': []},
         #           'iris-amly-egl': {'unlock': 'manual', 'depends_on': []},
         #           'iris-kbl-piglit': {'number': 3, 'depends_on': []},
         #           'iris-apl-traces': {'depends_on': []},
         #           'iris-glk-traces': {'depends_on': []},
         #           'iris-amly-traces': {'unlock': 'manual', 'depends_on': []},
         #           'iris-kbl-traces': {'depends_on': []},
         #           'iris-whl-traces': {'depends_on': []},
         #           'iris-cml-traces': {'depends_on': []},
         #           'intel-tgl-skqp': {'unlock': 'manual', 'depends_on': []},
         #           'intel-whl-skqp': {'unlock': 'manual', 'depends_on': []}},
         'nouveau': {
             'gm20b-gles-full': {'unlock': 'manual', 'depends_on': []},
             'gm20b-gles': {'unlock': 'manual', 'depends_on': []}},
         # 'arm': {
         #     'lima-mali450-test:arm64': {'depends_on': []},
         #     'lima-mali450-piglit-gpu:arm64': {'number': 2, 'depends_on': []},
         #     'panfrost-t720-gles2:arm64': {'depends_on': []},
         #     'panfrost-t860-gl:arm64': {'number': 3, 'depends_on': []},
         #     'panfrost-t860-traces:arm64': {'depends_on': []},
         #     'panfrost-g52-gl:arm64': {'number': 3, 'depends_on': []},
         #     'panfrost-g52-piglit-gl:arm64': {
         #         'number': 2,
         #         'unlock': 'manual',
         #         'depends_on': []},
         #     'panfrost-g72-gl:arm64': {'number': 3, 'depends_on': []}},
         # 'broadcom': {
         #     'vc4-rpi3:armhf': {'number': 3, 'depends_on': []},
         #     'vc4-rpi3-egl:armhf': {'depends_on': []},
         #     'vc4-rpi3-piglit-quick_gl:armhf': {'number': 4, 'depends_on': []},
         #     'v3d-rpi4-gles:armhf': {'number': 8, 'depends_on': []},
         #     'v3d-rpi4-egl:armhf': {'depends_on': []},
         #     'v3d-rpi4-piglit:armhf': {'number': 4, 'depends_on': []},
         #     'v3d-rpi4-traces:arm64': {'unlock': 'manual', 'depends_on': []},
         #     'v3dv-rpi4-vk:arm64': {'number': 8, 'depends_on': []}},
         # 'freedreno': {
         #     'a306_gl': {'number': 5, 'depends_on': []},
         #     'a306_piglit_gl': {'unlock': 'manual', 'depends_on': []},
         #     'a306_piglit_shader': {'unlock': 'manual', 'depends_on': []},
         #     'a306-traces': {'depends_on': []},
         #     'a530_gl': {'number': 6, 'depends_on': []},
         #     'a530_piglit_gl': {'unlock': 'manual', 'depends_on': []},
         #     'a530-traces': {'depends_on': []},
         #     'a618_vk': {'number': 7, 'depends_on': []},
         #     'a618_vk_full': {
         #         'number': 2,
         #         'unlock': 'manual',
         #         'depends_on': []},
         #     'a630_gl': {'number': 4, 'depends_on': []},
         #     'a630_egl': {'depends_on': []},
         #     'a630_gles_asan': {'depends_on': []},
         #     'a630_skqp': {'depends_on': []},
         #     'a630_vk': {'depends_on': []},
         #     'a630_vk_full': {
         #         'number': 2,
         #         'unlock': 'manual',
         #         'depends_on': []},
         #     'a630_vk_asan': {'depends_on': []},
         #     'a630_piglit': {'depends_on': []},
         #     'a630-traces': {'depends_on': []}},
         # 'etnaviv': {
         #     'gc2000_gles2': {'unlock': 'manual', 'depends_on': []},
         #     'gc2000_piglit': {'unlock': 'manual', 'depends_on': []}},
         # 'software-renderer': {
         #     'llvmpipe-piglit-cl': {'depends_on': []},
         #     'llvmpipe-traces': {'depends_on': []},
         #     'llvmpipe': {'depends_on': []},
         #     'llvmpipe-deqp-asan': {'depends_on': []},
         #     'softpipe': {'depends_on': []},
         #     'softpipe-asan-gles31': {'depends_on': []},
         #     'lavapipe': {'depends_on': []},
         #     'lavapipe-vk-asan': {'depends_on': []}},
         # 'layered-backends': {
         #     'test-d3d12-quick_gl': {'depends_on': []},
         #     'test-d3d12-quick_shader': {'depends_on': []},
         #     'virpipe-on-gl': {'depends_on': []},
         #     'virgl-on-gl': {'number': 3, 'depends_on': []},
         #     'virgl-on-gles': {'number': 3, 'depends_on': []},
         #     'virgl-traces': {'depends_on': []},
         #     'virgl-iris-traces': {'unlock': 'manual', 'depends_on': []},
         #     'zink': {'depends_on': []},
         #     'zink-anv-tgl': {'depends_on': []},
         #     'test-spirv2dxil-windows': {'depends_on': []},
         #     'test-dozen-deqp': {'depends_on': []}}
}}
