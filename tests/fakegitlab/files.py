#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from os.path import exists as os_path_exists
from unittest import mock

SAMPLES = 'tests/fakegitlab/samples'


class ProjectFile(mock.MagicMock):
    __file_name = None
    __ref = None
    __parent = None
    __gl = None

    def __init__(self, _file_name=None, _branch_ref=None, _parent=None,
                 _gl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__file_name = _file_name
        self.__ref = _branch_ref
        self.__parent = _parent
        self.__gl = _gl

    @property
    def file_name(self):
        return self.__file_name

    @property
    def file_path(self):
        return self.__file_name

    @property
    def ref(self):
        return self.__ref

    def decode(self):
        project_name = self.__parent.project.name
        _relative_path = f"{SAMPLES}/{project_name}/{self.__file_name}"
        if os_path_exists(_relative_path):
            self.__gl.debug(f"access to {_relative_path}")
            with open(_relative_path, 'r+b') as file_descriptor:
                return file_descriptor.read()
        self.__gl.debug(f"not found {_relative_path}")
        raise FileNotFoundError


class ProjectFileManager(mock.MagicMock):
    __project = None
    __parent = None
    __gl = None
    __files = None

    def __init__(self, _project=None, _parent=None, _gl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__project = _project
        self.__parent = _parent
        self.__gl = _gl
        self.__files = {}

    @property
    def project(self):
        return self.__project

    def get(self, name, ref):
        return self.__build_file(name, ref)

    # def create(self, name, ref):
    #     raise NotImplemented

    def __build_file(self, name, ref):
        if name in self.__files.keys():
            if ref in self.__files[name].keys():
                self.__gl.debug(f"file({name=}, {ref=}) found")
                return self.__files[name][ref]
            file_obj = ProjectFile(name, ref, self, self.__gl)
            self.__files[name][ref] = file_obj
            self.__gl.debug(f"file({name=}, {ref=}) wasn't in {ref=}, created")
            return file_obj
        file_obj = ProjectFile(name, ref, self, self.__gl)
        self.__files[name] = {}
        self.__files[name][ref] = file_obj
        self.__gl.debug(f"file({name=}, {ref=}) didn't exist, created")
        return file_obj
