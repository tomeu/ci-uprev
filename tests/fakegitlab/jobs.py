#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from datetime import datetime
from .exceptions import GitlabAuthenticationError
from unittest import mock
from random import randint
from uprev.abstract import bold, end


class ProjectJob(mock.MagicMock):
    __job_name = None
    __job_id = None
    __job_stage = None
    __job_status = None
    __job_started_at = None
    __parent = None
    __gl = None
    __trigger_jobs = None
    __dependency_jobs = None
    __unlock_status = None

    def __init__(self, _job_name=None, _parent=None, _gl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__job_name = _job_name
        self._assign_id()
        # FIXME: pipeline?
        self.__job_stage = ""
        self.__job_status = 'created'
        self.__job_started_at = None
        self.__parent = _parent
        self.__gl = _gl
        # self.__gl.debug(f"New Job({self.__job_name}), "
        #                 f"assign id {self.__job_id}")
        self.__trigger_jobs = []
        self.__dependency_jobs = []

    def _assign_id(self):
        self.__job_id = randint(int(1e5), int(1e6))

    @property
    def id(self):
        return self.__job_id

    @property
    def name(self):
        return self.__job_name

    @property
    def stage(self):
        return self.__job_stage

    @stage.setter
    def stage(self, value):
        self.__job_stage = value

    @property
    def trigger_jobs(self):
        return self.__trigger_jobs

    @property
    def dependency_jobs(self):
        return self.__dependency_jobs

    def cancel(self):
        self.__job_status = 'canceled'
        self.__job_started_at = None

    def retry(self):
        self.play()

    def play(self):
        if self._are_dependencies_satisfied():
            if self._there_is_an_unlock_status():
                # self.__gl.debug(f"Job {self.__job_name} unlocked")
                self.__job_status = self.__unlock_status
            else:
                # self.__gl.debug(f"{self.__job_name}.play()")
                self.__job_started_at = datetime.now()
                self.__job_status = 'pending'
            # self.__gl.debug(f"{id(self)} {self.__job_name!r} {self.__job_id} "
            #                 f"job started at {self.__job_started_at}")
            # self.status
        # else:
        #     self.__gl.debug(f"Ignore play() on {self.__job_name!r} because "
        #                     f"not all dependencies are satisfied")

    def artifacts(self):
        raise NotImplemented

    @property
    def status(self):
        if self.__job_started_at is not None:
            lapsed_time = datetime.now() - self.__job_started_at
            # self.__gl.debug(f"{id(self)} {self.__job_name!r} {self.__job_id} "
            #                 f"{bold}{self.__job_status}{end} {lapsed_time=}")
            if self.__job_status in ['created']:
                self.__job_status = 'pending'
                return self.__job_status
            if self.__job_status in ['pending']:
                change_time = randint(1, 5)
                # job takes between 1 to 5 seconds to change state
                # from pending to running
            else:
                change_time = randint(5, 20)
                # job takes between 5 to 20 seconds to change state
                # from running to success
            if lapsed_time.seconds > change_time:
                if self.__job_status == 'pending':
                    self.__job_status = 'running'
                elif self.__job_status == 'running':
                    self.__job_status = 'success'
                    self.__job_started_at = None
                    if len(self.__trigger_jobs) > 0:
                        for job in self.__trigger_jobs:
                            # self.__gl.debug(f"{self.__job_name} trigger "
                            #                 f"{job.name}")
                            job.play()
                # TODO: there could be jobs that fail, and we have to prepare
                #  artifacts for them
        # else:
        #     self.__gl.debug(f"{id(self)} {self.__job_name!r} "
        #                     f"{self.__job_id} {self.__job_status}")
        return self.__job_status

    def set_status(self, value):
        self.__job_status = value

    def unlock_status(self, value):
        """
        Once the dependencies are satisfied, instead of start the loop of
        status changes, set this status. This, for example, work to set a
        second phase manual jobs, like happen in mesa.
        :param value:
        :return:
        """
        self.__unlock_status = value
        # self.__gl.debug(f"{self.__job_name} will have an unlock status "
        #                 f"{self.__unlock_status}")

    def _are_dependencies_satisfied(self):
        if len(self.__dependency_jobs) > 0:
            satisfied = {}
            for job in self.__dependency_jobs:
                if job.status == 'success':
                    satisfied[job.name] = True
                else:
                    satisfied[job.name] = False
            if not all(satisfied.values()):
                # self.__gl.debug(f"job {self.__job_name} has dependencies "
                #                 f"unfinished: {satisfied}")
                return False
        return True

    def _there_is_an_unlock_status(self):
        return self.__unlock_status is not None and \
               self.__job_status == 'created'


class ProjectJobManager(mock.MagicMock):
    __project = None
    __parent = None
    __gl = None
    __job_ids = None

    def __init__(self, _project=None, _parent=None, _gl=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__project = _project
        self.__parent = _parent
        self.__gl = _gl
        self.__jobs = {}
        self.__jobs_by_id = {}
        self.__jobs_by_name = {}

    @property
    def project(self):
        return self.__project

    def get(self, name):
        # self.__gl.debug(f"get({name!r})")
        # if self.__gl.private_token is None:
        #     raise GitlabAuthenticationError(response_code=401,
        #                                     error_message='401 Unauthorized')
        if isinstance(name, int):
            return self.__get_job_by_id(name)
        if isinstance(name, str):
            if name.isdecimal():
                return self.__get_job_by_id(int(name))
            return self.__get_job_by_name(name)
        return self.__get_fresh_job(name)

    def __get_job_by_id(self, job_id):
        if job_id in self.__jobs_by_id:
            job_obj = self.__jobs_by_id[job_id]
            # self.__gl.debug(f"requested job id {job_id} and return cached "
            #                 f"{job_obj.name!r}")
            return job_obj
        return self.__get_fresh_job(job_id)

    def __get_job_by_name(self, job_name):
        if job_name in self.__jobs_by_name:
            job_obj = self.__jobs_by_name[job_name]
            # self.__gl.debug(f"requested job name {job_name!r} and return "
            #                 f"cached {job_obj.id}")
            return job_obj
        return self.__get_fresh_job(job_name)

    def __get_fresh_job(self, name):
        job_obj = ProjectJob(name, self, self.__gl)
        while job_obj.id in self.__jobs_by_id.keys():
            job_obj._assign_id()
        self.__jobs_by_id[job_obj.id] = job_obj
        if job_obj.name in self.__jobs_by_name:
            self.__gl.debug(f"Ups! repeated job name {job_obj.name}")
        self.__jobs_by_name[job_obj.name] = job_obj
        # self.__gl.debug(f"generated a fresh job with the name "
        #                 f"{job_obj.name!r} and id {job_obj.id}")
        return job_obj

    def list(self, all=None, per_page=None, page=None):
        jobs_list = list(self.__jobs_by_name.values())
        if isinstance(all, bool) and all is True:
            # self.__gl.debug(f"requested the complete list of jobs "
            #                 f"{jobs_list}")
            return jobs_list
        if per_page is None:
            per_page = 5
        if page is None:
            page = 1
        shift = (page-1)*per_page
        # self.__gl.debug(f"requested the list of jobs "
        #                 f"{jobs_list[shift:shift+per_page]}")
        return jobs_list[shift:shift+per_page]

