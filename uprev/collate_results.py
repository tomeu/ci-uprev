#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from argparse import ArgumentParser
from .abstract import default_gitlab_url, default_project_path, gitlab_token
from .collateresults import (ResultsCollector, DEFAULT_URL,
                             DEFAULT_PROJECT_NAME, DEFAULT_JOB_NAMES_PATTERN)
from traceback import print_exc


# --- command line interface


def cli_arguments():
    parser = ArgumentParser(
        description="CLI tool to gather information from artifacts in a "
                    "gitlab pipeline")
    parser.add_argument(
        '-d', '--debug', action='store_true',
        help="debug flag to have execution traces.")
    # group about how to access the information
    access_group = parser.add_argument_group('input from',
                                             '(mutually exclusive)')
    mutually_exclusive_access = access_group.add_mutually_exclusive_group(
        required=True)
    mutually_exclusive_access.add_argument(
        '-c', '--commit', nargs='?', help="hash of the commit to inspect")
    mutually_exclusive_access.add_argument(
        '-m', '--merge-request', nargs='?', help="merge request id to inspect")
    mutually_exclusive_access.add_argument(
        '-p', '--pipeline', nargs='?', help="pipeline id to inspect")
    # group about point to the data sources
    data_sources_group = parser.add_argument_group('sources')
    data_sources_group.add_argument(
        '-u', '--url', nargs='?', default=default_gitlab_url(DEFAULT_URL),
        help=f"address of where the project is (default \"{DEFAULT_URL}\") "
             f"one can use an environment variable named GITLAB_URL")
    data_sources_group.add_argument(
        '-n', '--name', nargs='?',
        default=default_project_path(DEFAULT_PROJECT_NAME),
        help=f"project name (default \"{DEFAULT_PROJECT_NAME}\") one can use "
             f"an environment variable named GITLAB_PROJECT_PATH")
    # # group about the gitlab token
    # token_group = parser.add_argument_group(
    #     'token source', '(mutually exclusive) overrides what would be set in '
    #                     'the environment variable named GITLAB_TOKEN')
    # token_exclusive = token_group.add_mutually_exclusive_group(required=False)
    # token_exclusive.add_argument(
    #     '-t', '--token', nargs='?',
    #     help="gitlab token (readonly better) to access the information in the "
    #          "repository")
    # token_exclusive.add_argument(
    #     '--token-file', nargs='?', help="file that has the gitlab token")
    # group about filtering data
    filtering_group = parser.add_argument_group('filtering')
    filtering_group.add_argument(
        '-j', '--jobs', nargs='?', default=DEFAULT_JOB_NAMES_PATTERN,
        help="regexp for the job names to explore")
    # group about output of the tool
    output_group = parser.add_argument_group('output')
    output_group.add_argument(
        '--csv', action='store_true', default=False,
        help="produce a csv file as output that is a merge of the same "
             "artifact per pipeline (default)")
    output_group.add_argument(
        '--patch', action='store_true', default=False,
        help="produce a patch that may be applied to the repository")
    output_group.add_argument(
        '--json', action='store_true', default=False,
        help="produce a json format report per pipeline")
    # note at the end to scape spacial symbols
    parser.epilog = "Note: " \
                    "pipeline number may start with '#' like the merge " \
                    "request one can start with '!'. Remember to scape them " \
                    "in the command line"
    return parser.parse_args()


def manage_exit(exit_code, exit_message):
    print(
        f"Execution failed and exited with error {exit_code}.\n"
        f"\n{exit_message}\n"
    )
    exit(exit_code)


def main():
    args = cli_arguments()
    token = None
    try:
        token = gitlab_token()
    except AttributeError as exception:
        manage_exit(-1, f"{exception}")
    try:
        if not any([args.csv, args.patch, args.json]):
            csv = True
        else:
            csv = args.csv
        manager = ResultsCollector(url=args.url, project=args.name,
                                   jobs_regex=args.jobs, commit=args.commit,
                                   merge_request=args.merge_request,
                                   pipeline=args.pipeline, token=token,
                                   csv=csv, patch=args.patch, json=args.json,
                                   debug=args.debug)
        manager.build_output()
    except AttributeError as exception:
        if args.debug:
            manage_exit(-2, f"{exception}\n{print_exc()}")
        else:
            manage_exit(-2, f"{exception}")
    except Exception as exception:
        manage_exit(1, f"Unexpected exception to be debug"
                       f"\n{exception}\n{print_exc()}")


if __name__ == '__main__':
    main()
