#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from ..abstract import (bold, italic, underline,
                        blue, green, brown, purple, red, orange, yellow, end,
                        DEFAULT_LOG_FORMAT)

DEFAULT_PIPELINE_STATUS = 'success'
DEFAULT_PIPELINE_OWNER = 'marge-bot'

# DEFAULT_GITLAB_CI_YML = '.gitlab-ci.yml'

DEFAULT_PATCH_FILE_NAME = 'new_{revision}_reference_in_{origin}.patch'

DEFAULT_MR_TAGS = ['ci']

DEFAULT_COMMIT_OBJECT_CREATION_WAIT_TIME = 0.1  # s
DEFAULT_PIPELINE_OBJECT_CREATION_WAIT_TIME = 1.5  # s
DEFAULT_PIPELINE_ID_ASSIGN_WAIT_TIME = 0.1  # s
DEFAULT_PIPELINE_PROGRESS_CHECK = 5  # s
DEFAULT_PIPELINE_RETRIES = 2
