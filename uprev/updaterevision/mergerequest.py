#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from ..abstract import Logger, blue, red, yellow, end
from ..abstract import uprev_pipeline_source, uprev_pipeline_is_schedule
from .defaults import (DEFAULT_MR_TAGS,
                       DEFAULT_PIPELINE_OBJECT_CREATION_WAIT_TIME)
from .gitlabwrapper import get_current_execution_info
from tenacity import retry, stop_after_attempt, wait_exponential
from time import sleep


class MergeRequest(Logger):
    __uprev_obj = None
    __source_branch = None
    __target_branch = None
    __title = None
    __expectations_updated = None
    __merge_request_id = None

    def __init__(self, uprev_object,
                 source_branch, target_branch, title,
                 expectations_updated, *args, **kwargs):
        super(MergeRequest, self).__init__(*args, **kwargs)
        self.__uprev_obj = uprev_object
        self.__source_branch = source_branch
        self.__target_branch = target_branch
        self.__title = title
        self.__expectations_updated = expectations_updated
        self.debug(f"Build a merge request manager object for "
                   f"{blue}{self.__uprev_obj.origin.full_url}{end}. "
                   f"From {yellow}{self.__source_branch.name}{end} "
                   f"to {yellow}{self.__target_branch.name}{end} "
                   f"with title {yellow}{self.__title}{end}")

    def __str__(self):
        if self.__merge_request_id is not None:
            return f"MergeRequest({self.__merge_request_id})"
        return "MergeRequest"

    @property
    def __merge_request_obj(self):
        if self.__merge_request_id is not None:
            return self.__uprev_obj.main_origin.mergerequests.get(
                self.__merge_request_id)

    @property
    def __merge_request_state(self):
        if self.__merge_request_id is not None:
            return self.__merge_request_obj.state

    @property
    def __merge_request_status(self):
        if self.__merge_request_id is not None:
            return self.__merge_request_obj.merge_status

    def create(self):
        self.__merge_request_id = self.__build_merge_request()
        self.__wait_merge_request_creation()
        self.__launch_merge_request_pipeline()

    # -- first descendant level

    def __build_merge_request(self):
        try:
            if not uprev_pipeline_is_schedule():
                _title = f"Draft: {self.__title}"
                self.info(f"Pipeline source "
                          f"{yellow}{uprev_pipeline_source()}{end}, tag the "
                          f"merge request as {yellow}draft{end}")
            else:
                _title = f"{self.__title}"
            _merge_request_obj = self.__create_new_merge_request(_title)
            return _merge_request_obj.iid
        except Exception as exception:
            self.error(f"Something when wrong creating a merge request to "
                       f"{yellow}{self.__uprev_obj.main_origin.project}{end}: "
                       f"{exception}")
            raise exception

    def __wait_merge_request_creation(self):
        state = self.__merge_request_state
        status = self.__merge_request_status
        while state not in ['opened'] and status not in ['can_be_merged']:
            self.debug(f"Waiting merge request creation to complete: "
                       f"state: {yellow}{state}{end} "
                       f"merge_status: {yellow}{status}{end}")
            sleep(1)  # TODO: improve the waiting
            # FIXME: don't wait forever
            state = self.__merge_request_state
            status = self.__merge_request_status
        self.debug(f"Merge request creation complete: "
                   f"state {yellow}{state}{end} "
                   f"merge_status: {yellow}{status}{end}")
        _merge_request_obj = self.__merge_request_obj
        self.debug(f"Waiting merge request pipeline creation")
        while not hasattr(_merge_request_obj, 'head_pipeline') or \
                _merge_request_obj.head_pipeline is None or \
                not 'id' in _merge_request_obj.head_pipeline:
            sleep(1)
            # FIXME: check that better
            _merge_request_obj = self.__merge_request_obj
        pipeline_id = _merge_request_obj.head_pipeline['id']
        self.info(f"Merge request {yellow}{_merge_request_obj.iid}{end} "
                  f"has a pipeline {yellow}{pipeline_id}{end}")

    def __launch_merge_request_pipeline(self):
        if not hasattr(self.__merge_request_obj, "head_pipeline") or \
                not isinstance(self.__merge_request_obj.head_pipeline,
                               dict) or \
                'id' not in self.__merge_request_obj.head_pipeline:
            self.debug(f"Waiting merge reqeust to have pipeline assigned")
            sleep(DEFAULT_PIPELINE_OBJECT_CREATION_WAIT_TIME)
        _pipeline_id = self.__merge_request_obj.head_pipeline['id']

        _pipeline_obj = self.__get_pipeline(self.__uprev_obj.origin,
                                            _pipeline_id)
        self.debug(f"Prepared a pipeline {yellow}{_pipeline_id}{end}: "
                   f"{blue}{_pipeline_obj.web_url}{end}")
        # TODO: there is already a method to do that in WorkingBranch
        _pipeline_jobs = _pipeline_obj.jobs.list(all=True)

        _project_jobs = []
        for _pipeline_job in _pipeline_jobs:
            _project_job = self.__get_job(_pipeline_job.id)
            _project_jobs.append(_project_job)
        jobs_to_trigger = self.__uprev_obj.manual_trigger_jobs(_project_jobs)

        # jobs_to_trigger = self.__uprev_obj.manual_trigger_jobs(
        #     [self.__get_job(job.id) for job in _pipeline_jobs])

        for job in jobs_to_trigger:
            job.play()

        # TODO: maybe in the future the requesters may even like to use
        #  'merge_when_pipeline_succeeds'
        #  or something like 'assign to marge'

    # -- second descendant level

    def __check_existing_merge_request_titles(self, title):
        for merge_request_obj in \
                self.__uprev_obj.main_origin.iterate_merge_requests(
                    state='opened'):
            self.debug(f"issue {merge_request_obj.iid} has title "
                       f"{merge_request_obj.title!r}")
            if merge_request_obj.title == title:
                return merge_request_obj
        return None

    def __create_new_merge_request(self, title):
        _previous_merge_request = \
            self.__check_existing_merge_request_titles(title)
        if _previous_merge_request is not None:
            self.warning(
                f"It has been found a merge request "
                f"{yellow}{_previous_merge_request.iid}{end} "
                f"{blue}{_previous_merge_request.web_url}{end} {red}open and "
                f"not yet merged{end} with the same subject from the branch "
                f"{yellow}{_previous_merge_request.source_branch}{end}")
        _description = \
            self.__prepare_mergerequest_description(_previous_merge_request)
        merge_request_obj = \
            self.__uprev_obj.origin.mergerequests.create(
                {'source_branch': self.__source_branch.encoded_id,
                 'target_branch': self.__target_branch.encoded_id,
                 'target_project_id': self.__uprev_obj.main_origin.project_id,
                 'title': title,
                 'labels': DEFAULT_MR_TAGS,
                 'remove_source_branch': True,
                 'squash': True,
                 'allow_collaboration': True,
                 'description': _description})
        # TODO: dynamically made this to avoid hardcoding and guesses.
        self.debug(f"Created a merge request Gitlab object "
                   f"{yellow}{merge_request_obj.iid}{end} with "
                   f"{yellow}{merge_request_obj.author['name']}{end}. See "
                   f"{blue}{merge_request_obj.web_url}{end}.")
        merge_request_obj = \
            self.__uprev_obj.main_origin.mergerequests.get(
                merge_request_obj.iid)
        if _previous_merge_request is not None:
            _previous_merge_request.notes.create(
                {'body': f"This merge request has been expired by a [newer one]"
                         f"({merge_request_obj.web_url})"})
        return merge_request_obj

    def __prepare_mergerequest_description(self, previous_merge_request):
        self.debug(f"Prepare merge request description")
        preheader = get_current_execution_info(self.__uprev_obj.main_origin)
        header = f"request to move the mesa reference to a newer one."
        if previous_merge_request is not None:
            header = f"{header}\n" \
                     f"There is a previous " \
                     f"[merge request]({previous_merge_request.web_url}) " \
                     f"still not merged that the current one expires."
            # TODO: /cc people involved in the previous merge request
        if preheader is not None:
            header = f"{preheader}, {header}"
        else:
            header = f"{header.capitalize()}"
        self.debug(f"Merge request description header: {header!r}")
        expectations_text = f" (updating the expectation files, check the diff)"
        # FIXME: hardcoded the 'virglrenderer' name
        body = f"There has been found a newer commit in mesa that passes the " \
               f"tests of virglrenderer" \
               f"{expectations_text if self.__expectations_updated else ''}, " \
               "so it is a good candidate to move on."
        self.debug(f"Merge request description body: {body!r}")
        return f"""{header}\n\n{body}"""

    # TODO: there is already a method to do that in WorkingBranch
    @retry(reraise=True, stop=stop_after_attempt(7),
           wait=wait_exponential(multiplier=1, min=1, max=60))
    def __get_job(self, job_id):
        return self.__uprev_obj.origin.jobs.get(job_id)

    @retry(reraise=True, stop=stop_after_attempt(7),
           wait=wait_exponential(multiplier=1, min=1, max=60))
    def __get_pipeline(self, source, pipeline_id):
        return source.pipelines.get(pipeline_id)
