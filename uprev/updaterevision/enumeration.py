#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"


from enum import Enum, auto


class JobFailCause(Enum):
    CI_issue = auto()
    UnexpectedPass = auto()
    Fail = auto()
    Crash = auto()
    OtherTests = auto()

    def __repr__(self):
        return f"{self.name}"


class IssueType(Enum):
    CI = auto()
    Fail = auto()
    Unknown = auto()

    def __repr__(self):
        return f"{self.name}"


class IssueTypeExtraInfo(Enum):
    pipeline_obj = auto()

    def __repr__(self):
        return f"{self.name}"


# TODO: instead of string compares with the status,
#  use those enums to compare integers with meaning
# FIXME: check the python-gitlab to verify the list is complete and correct

class JobStatus(Enum):
    failed = auto()
    warning = auto()
    pending = auto()
    running = auto()
    manual = auto()
    scheduled = auto()
    canceled = auto()
    success = auto()
    skipped = auto()
    created = auto()


class PipelineStatus(Enum):
    pending = auto()
    running = auto()
    success = auto()
    passed = auto()
    failed = auto()
    skipped = auto()
    canceled = auto()
    unknown = auto()
