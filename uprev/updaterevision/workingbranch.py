#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from ..abstract import (Logger, mergerequest_title, ci_issues_title,
                        testfail_issues_title, unknown_issues_title,
                        uprev_merge_weekday)
from ..collateresults import ResultsCollector
from ..collateresults import DEFAULT_JOB_NAMES_PATTERN, Artifacts
from ..collateresults.output.patch import Patch
from datetime import datetime
from .defaults import (bold, blue, green, brown, purple, red, orange, yellow,
                       end, DEFAULT_COMMIT_OBJECT_CREATION_WAIT_TIME,
                       DEFAULT_PIPELINE_OBJECT_CREATION_WAIT_TIME,
                       DEFAULT_PIPELINE_ID_ASSIGN_WAIT_TIME,
                       DEFAULT_PIPELINE_PROGRESS_CHECK,
                       DEFAULT_PIPELINE_RETRIES)
from .enumeration import JobFailCause, IssueType, IssueTypeExtraInfo
from gitlab import GitlabCreateError, GitlabGetError
from .issuecreation import IssueCreator
from .mergerequest import MergeRequest
from os import getcwd as os_getcwd
from os import chdir as os_chdir
from pprint import pformat
from re import search as re_search
from shutil import rmtree as shutil_rmtree
import sys
from tempfile import mkdtemp as tempfile_mkdtemp
from time import sleep
from time import strptime as time_strptime
from tenacity import retry, stop_after_attempt, wait_exponential


class WorkingBranch(Logger):
    __uprev_mngr_obj = None
    __patch_obj = None

    __commit_id = None
    __pipeline_id = None
    __origin_branch_name = None
    __new_branch_name = None
    __title = None
    __failed_jobs_info = None

    def __init__(self, uprev_object, patch_obj, new_branch_name,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__uprev_mngr_obj = uprev_object
        self.__patch_obj = patch_obj
        self.__commit_id = patch_obj.revision_commit_id
        self.__pipeline_id = patch_obj.revision_pipeline_id
        merge_request_title = mergerequest_title()
        self.debug(f"branch and merge request will be made to "
                   f"{blue}{self.__uprev_mngr_obj.origin.full_url}{end}")
        self.__input_sanitization(patch_obj.origin_branch_name, new_branch_name,
                                  merge_request_title)
        self.debug(
            f"expected to have the file {brown}"
            f"{self.__patch_obj.temporary_storage}/b/"
            f"{self.__patch_obj.uprev_patch_file}"
            f"{end} with the content to be committed")
        try:
            if not self.__prepare_branch():
                return
            self.__patch_uprev_in_branch()
        except (KeyboardInterrupt, Exception) as exception:
            _branch_obj = self.__get_branch(self.__new_branch_name)
            self.info(f"After an exception {exception} "
                      f"remove the branch "
                      f"{yellow}{self.__new_branch_name}{end}")
            _branch_obj.delete()
            raise exception

    def __str__(self):
        return "WorkingBranch"

    def proceed(self):
        t0 = datetime.now()
        try:
            self.__branch_pipeline_procedure()
            self.debug(f"Branch pipeline procedure completed in "
                       f"{purple}{datetime.now() - t0}{end} seconds")
        except (KeyboardInterrupt, Exception) as exception:
            _branch_obj = self.__get_branch(self.__new_branch_name)
            self.info(f"After an exception '{exception}' "
                      f"remove the branch "
                      f"{yellow}{self.__new_branch_name}{end}")
            _branch_obj.delete()
            self.debug(f"Branch pipeline procedure completed in "
                       f"{purple}{datetime.now() - t0}{end} seconds")
            raise exception

    # -- first descendant level

    def __input_sanitization(self, origin_branch_name, new_branch_name,
                             merge_request_title):
        self.debug(f"input received:\n"
                   f"- origin_branch_name: {origin_branch_name}\n"
                   f"- new branch name: {new_branch_name}\n"
                   f"- merge request title: {merge_request_title}")
        if origin_branch_name is not None:
            self.__origin_branch_name = origin_branch_name
        else:
            self.__origin_branch_name = \
                self.__uprev_mngr_obj.origin.default_branch
        self.__new_branch_name = \
            self.__check_branch_name_policy(new_branch_name)
        self.__title = merge_request_title

    def __prepare_branch(self):
        """
        Create a branch for this procedure.
        :return: bool
        """
        try:
            _branch_obj = self.__uprev_mngr_obj.origin.branches.create(
                {'branch': self.__new_branch_name,
                 'ref': self.__origin_branch_name})
            self.debug(f"Branch {yellow}{self.__new_branch_name}{end} created "
                       f"from {yellow}{self.__origin_branch_name}{end} in "
                       f"{blue}{self.__uprev_mngr_obj.origin.full_url}{end}")
        except GitlabCreateError as exception:
            if exception.response_code == 400:
                if exception.error_message == 'Branch already exists':
                    self.error(f"Branch {self.__new_branch_name} "
                               f"{red}already exist{end}. Specify a different "
                               f"one with '--branch' or the merge request "
                               f"cannot be created (probably there is one "
                               f"already in progress)")
                    return False
                else:  # TODO: catch other cases and work them properly
                    raise exception
            elif exception.response_code == 403:
                if exception.error_message == '403 Forbidden':
                    reason = "Token not available"
                elif exception.error_message == 'insufficient_scope':
                    reason = "Token may not have write rights"
                else:
                    reason = "Incorrect token"
                raise AttributeError(
                    f"{reason}, review '--token' or '--token-file' argument or "
                    f"the environment variable GITLAB_TOKEN")
        return True

    def __patch_uprev_in_branch(self):
        _local_file_path = \
            f"{self.__patch_obj.temporary_storage}/b/" \
            f"{self.__patch_obj.uprev_patch_file}"
        _branch_obj = self.__get_branch(self.__new_branch_name)
        _revision_project = self.__uprev_mngr_obj.revision.project.split('/')[1]
        _commit_msg = f"uprev {_revision_project}\nTo " \
                      f"commit {self.__commit_id} based on the success of " \
                      f"the pipeline {self.__pipeline_id}."
        commit_info = {'branch': _branch_obj.encoded_id,
                       'commit_message': _commit_msg,
                       'actions': [{
                           'action': 'update',
                           'file_path': self.__patch_obj.uprev_patch_file,
                           'content': open(_local_file_path).read()
                       }]}
        commit = self.__uprev_mngr_obj.origin.commits.create(commit_info)
        self.debug(f"Commit made and visible in {blue}{commit.web_url}{end}. "
                   f"Message: {yellow}{commit.message!r}{end}")

    def __branch_pipeline_procedure(self):
        """
        The patched branch has an associated pipeline that is necessary to
        succeed to create the merge request.

        In case this pipeline fails, it will relaunch it because it can be a
        flake in some test. But when doesn't, an alternative procedure is
        launched about how it has to be managed.
        :return:
        """
        pipeline_id, status, attempt = None, None, 0
        _expectations_updated = False
        self.__failed_jobs_info = {}
        while status not in ['success', 'blocked']:
            t0 = datetime.now()
            if pipeline_id is None or status is None:
                pipeline_id = self.__launch_branch_pipeline()
            self.__wait_branch_pipeline(pipeline_id)
            status = self.__get_pipeline(pipeline_id).status
            self.debug(f"Pipeline execution took "
                       f"{purple}{datetime.now() - t0}{end} seconds")
            if status in ['failed']:
                self.info(f"Pipeline {yellow}{pipeline_id}{end} ends with "
                          f"{red}{status}{end} collect information about the "
                          f"reason")
                collection, failure_cause = self.__collate_results(pipeline_id,
                                                                   attempt)
                if attempt < DEFAULT_PIPELINE_RETRIES:
                    if self.__branch_pipeline_retry(pipeline_id, failure_cause):
                        self.debug(f"It may be flaky failure (retry "
                                   f"{attempt+1}/{DEFAULT_PIPELINE_RETRIES})")
                        attempt += 1
                        continue
                    else:
                        self.debug(f"No more flaky attempts")
                if self.__check_pipeline_failure(pipeline_id, failure_cause,
                                                 collection):
                    # a new set of opportunities due to the expectations update
                    pipeline_id, status, attempt = None, None, 0
                    _expectations_updated = True
                else:
                    break
            elif status in ['cancelled']:
                self.info(f"Pipeline {yellow}{pipeline_id}{end} ends with "
                          f"{orange}{status}{end}. Nothing else to do here")
                break
            elif status in ['success', 'blocked']:
                self.info(f"Pipeline {yellow}{pipeline_id}{end} ends with "
                          f"{green}{status}{end}")
                if self.__is_merge_day():
                    self.__launch_merge_request(_expectations_updated)

    # -- second descendant level

    def __check_branch_name_policy(self, branch_name):
        date_in_name = re_search(r'\d{4}\d{2}\d{2}', branch_name)
        if date_in_name is None:
            today = datetime.strftime(datetime.now(), '%Y%m%d')
            modified_branch_name = f"{branch_name}-{today}"
        else:
            modified_branch_name = f"{branch_name}"
        ctr_at_end = re_search(r'\D(\d{3})$', branch_name)
        if ctr_at_end is None:
            ctr = 1
            branch_without_ctr = f"{modified_branch_name}"
        else:
            ctr = int(ctr_at_end.group(1))
            branch_without_ctr = f"{modified_branch_name[:ctr_at_end.start()]}"
        while ctr < 1000:
            name_candidate = f"{branch_without_ctr}-{ctr:03}"
            try:
                self.__uprev_mngr_obj.origin.branches.get(name_candidate)
                self.debug(f"already exist a branch with name {name_candidate}")
            except GitlabGetError as exception:
                if exception.response_code == 404 and \
                        exception.error_message == '404 Branch Not Found':
                    self.debug(f"Found the first free in the sequence "
                               f"{name_candidate}")
                    return name_candidate
                else:
                    self.debug(f"Unmanaged case {exception.response_code}: "
                               f"{exception.error_message} for branch named "
                               f"{name_candidate}")
            ctr += 1
        raise IndexError(f"overflow the branch counter")

    def __launch_branch_pipeline(self):
        """
        Once the branch arrives to the server, a CI pipeline is associated.
        It takes a bit of time for the server to build all the corresponding
        elements, so this method checks it to know when the things are read.
        :return:
        """
        self.debug(f"Launch {yellow}{self.__new_branch_name}{end} branch "
                   f"pipeline")
        _branch_obj = self.__get_branch(self.__new_branch_name)
        _branch_commit_id = _branch_obj.commit['id']
        self.debug(f"Branch commit {yellow}{_branch_commit_id}{end}")
        _commit_obj = None
        self.debug(f"Waiting for the commit object to be available")
        while _commit_obj is None:
            _commit_obj = self.__get_commit(_branch_commit_id)
            # FIXME: empirical
            sleep(DEFAULT_COMMIT_OBJECT_CREATION_WAIT_TIME)
        self.debug(f"Waiting for the pipeline to be available")
        while _commit_obj.last_pipeline is None:
            _commit_obj = self.__get_commit(_branch_commit_id)
            # FIXME: empirical
            sleep(DEFAULT_PIPELINE_OBJECT_CREATION_WAIT_TIME)
        self.debug(f"Waiting for the pipeline id")
        while 'id' not in _commit_obj.last_pipeline:
            _commit_obj = self.__get_commit(_branch_commit_id)
            # FIXME: empirical
            sleep(DEFAULT_PIPELINE_ID_ASSIGN_WAIT_TIME)
        pipeline_id = int(_commit_obj.last_pipeline['id'])
        self.debug(f"Pipeline for the commit {yellow}{_branch_commit_id}{end} "
                   f"in the branch {yellow}{self.__new_branch_name}{end} "
                   f"id {yellow}{pipeline_id}{end}: "
                   f"{blue}{_commit_obj.last_pipeline['web_url']}{end}")
        self.__launch_manual_jobs_in_pipeline(pipeline_id)
        return pipeline_id

    def __wait_branch_pipeline(self, pipeline_id):
        """
        The branch associated pipeline has manual jobs to be triggered. Then,
        the progress of the jobs is monitored to know how this is working.
        :param pipeline_id: int
        :return:
        """
        try:
            # FIXME: in virglrenderer there are only two layers of job
            #  execution. First the build ones and then the tests.
            #  But in mesa, with >200 jobs, there are like two stages with
            #  those two layers. So there is an initial set of jobs to be
            #  play() and then a set of jobs to be monitor. But then there is
            #  a second set to be play() with it set of jobs to monitor.
            # FIXME: Not all the jobs in mesa have to be play() because some
            #  seem to fail unless there are certain conditions that allow them
            #  to be played.
            for function in [self.__manual_trigger_jobs, self.__test_jobs]:
                _monitor_jobs = function(pipeline_id)
                # periodical check the state of the manual triggered jobs
                self.debug(f"Filtered to monitor {len(_monitor_jobs)} "
                           f"({[job.name for job in _monitor_jobs]})")
                self.__check_evolution_of_jobs_set(_monitor_jobs)
        except (KeyboardInterrupt, Exception) as exception:
            _pipeline_obj = \
                self.__uprev_mngr_obj.origin.pipelines.get(pipeline_id)
            self.info(f"After an exception {exception} "
                      f"cancel the pipeline {pipeline_id}")
            _pipeline_obj.cancel()
            raise exception

    def __collate_results(self, pipeline_id, attempt):
        """
        When a pipeline didn't succeed, the information that each test job has
        provided has to be collected for further review.
        :param pipeline_id:
        :return:
        """
        Artifacts.clean('failures')
        collection = ResultsCollector(
            url=self.__uprev_mngr_obj.origin.url,
            project=self.__uprev_mngr_obj.origin.project,
            jobs_regex=DEFAULT_JOB_NAMES_PATTERN, pipeline=f"#{pipeline_id}",
            artifacts_set={'failures.csv'},
            token=self.__uprev_mngr_obj.origin.private_token,
            debug=self.is_in_debug(), trace=self.is_in_trace())
        self.debug(f"Build {collection!r}")
        failures_artifacts = Artifacts('failures', self.is_in_debug())
        self.debug(f"Get the {failures_artifacts!r} singleton")
        if not hasattr(failures_artifacts, "jobs_by_pipeline") or \
                not isinstance(failures_artifacts.jobs_by_pipeline, dict):
            raise AssertionError(f"The artifacts collected failed to be build")
        if f"{pipeline_id}" not in failures_artifacts.jobs_by_pipeline:
            raise AssertionError(
                f"There aren't artifacts to work for this {pipeline_id} "
                f"pipeline: {list(failures_artifacts.jobs_by_pipeline.keys())}")
        pipeline_failure_artifacts = \
            failures_artifacts.jobs_by_pipeline[f"{pipeline_id}"]
        if not isinstance(pipeline_failure_artifacts, dict):
            raise AssertionError(f"Cannot distinguish the jobs in the pipeline")
        failure_cause = self.__review_failure_cause(pipeline_id, attempt,
                                                    pipeline_failure_artifacts)
        self.debug(f"Main cause of the failure is {red}{failure_cause}{end}")
        return collection, failure_cause

    def __branch_pipeline_retry(self, pipeline_id, failure_cause):
        if failure_cause in [JobFailCause.Crash, JobFailCause.Fail,
                             JobFailCause.UnexpectedPass]:
            pipeline_obj = self.__get_pipeline(pipeline_id)
            self.debug(f"Retry failed jobs in pipeline "
                       f"{yellow}{pipeline_id}{end}")
            pipeline_obj.retry()
            return True
        else:
            self.debug(f"No reason for retry")
            return False

    def __check_pipeline_failure(self, pipeline_id, failure_cause, collection):
        """
        Check the reason behind the failed jobs:
        1. Job(s) didn't work due to CI failure cause
           Then create an issue (to the virgl object) reporting that
        2. Job(s) report in failures.csv
           a. There are "UnexpectedPass"
              So the expectations can be updated
              then return true to relaunch a pipeline for
              this expectations patched branch
           b. There are "Fail" test
              Then the developers have to be reported with
              (an issue to mesa? virgl?)
        :param pipeline_id:
        :return:
        """
        if failure_cause == JobFailCause.CI_issue:
            _template = {
                IssueType.CI: {IssueTypeExtraInfo.pipeline_obj:
                               self.__get_pipeline(pipeline_id)}}
            issue = IssueCreator(self.__uprev_mngr_obj.uprev,
                                 self.__get_branch(self.__new_branch_name),
                                 ci_issues_title(), template=_template,
                                 debug=self.is_in_debug())
            issue.create()
            return False
        elif failure_cause in [JobFailCause.Crash, JobFailCause.Fail]:
            _template = {
                IssueType.Fail: {IssueTypeExtraInfo.pipeline_obj:
                                 self.__get_pipeline(pipeline_id)}}
            _description = self.__failed_jobs_info_to_text()
            # TODO: Instead of an issue, here it can be created a merge request
            #  with a WIP in the subject, so the developers can solve the
            #  problem and get merged the uprev.
            issue = IssueCreator(self.__uprev_mngr_obj.main_origin,
                                 self.__get_branch(self.__new_branch_name),
                                 testfail_issues_title(), template=_template,
                                 description=_description,
                                 debug=self.is_in_debug())
            issue.create()
            # TODO: check if there is a previous attempt to uprev (for example
            #  from yesterday) that skips the MR (because it wasn't the
            #  MERGE_WEEKDAY) that today can be generate a MR from there
            #  together with the recently generated Issue.
            # self. __find_previous_success_uprev()
            return False
        elif failure_cause == JobFailCause.UnexpectedPass:
            self.__patch_expectations_in_branch(collection)
            return True
        # TODO: this is an issue by a cause to be reviewed
        _template = {
            IssueType.Unknown: {IssueTypeExtraInfo.pipeline_obj:
                                self.__get_pipeline(pipeline_id)}}
        issue = IssueCreator(self.__uprev_mngr_obj.uprev,
                             self.__get_branch(self.__new_branch_name),
                             unknown_issues_title(), template=_template,
                             debug=self.is_in_debug())
        issue.create()
        return False

    def __is_merge_day(self):
        merge_weekday = uprev_merge_weekday()
        try:
            if merge_weekday is None:
                self.debug(f"Merge weekday hasn't set, so {green}proceed{end}")
                return True
            if merge_weekday.lower() in ["any", "all"]:
                self.debug(f"Merge week day set to "
                           f"{yellow}{merge_weekday}{end}, so "
                           f"{green}proceed{end}")
                return True
            merge_weekday_as_int = time_strptime(merge_weekday, '%A').tm_wday
            today_weekday_as_int = datetime.now().weekday()
            if merge_weekday_as_int == today_weekday_as_int:
                self.debug(f"Today is {yellow}{merge_weekday}{end}, "
                           f"so {green}proceed{end}")
                return True
            self.info(f"Today is not a merge day, so {orange}skip{end}")
            return False
        except ValueError as exception:
            self.warning(f"Review MERGE_WEEKDAY variable, because it cannot be "
                         f"interpreted: {exception}")
            return True
        except Exception as exception:
            self.error(f"Review the merge day procedure: {exception}")
            return False

    def __launch_merge_request(self, expectations_updated):
        """
        Having the pipeline succeed with the modifications required, it's time
        to ask the developers for review to have this merged.
        :return:
        """
        _source_branch = self.__get_branch(self.__new_branch_name)
        _target_branch = self.__get_branch(self.__origin_branch_name)
        merge_request_obj = MergeRequest(self.__uprev_mngr_obj,
                                         _source_branch, _target_branch,
                                         self.__title, expectations_updated,
                                         debug=self.is_in_debug(),
                                         trace=self.is_in_trace())
        merge_request_obj.create()

    # -- third descendant level

    def __launch_manual_jobs_in_pipeline(self, pipeline_id):
        """
        Given a pipeline id, launch the manual jobs.
        :param pipeline_id:
        :return:
        """
        _jobs_to_trigger = self.__manual_trigger_jobs(pipeline_id)
        if len(_jobs_to_trigger) == 0:
            raise LookupError(f"Cannot find jobs to be manually launched in "
                              f"pipeline {pipeline_id}")
        self.debug(f"Filtered to start {len(_jobs_to_trigger)} "
                   f"({[job.name for job in _jobs_to_trigger]})")
        for job in _jobs_to_trigger:
            self.info(f"launch {yellow}{job.name}{end} job in pipeline "
                      f"{yellow}{pipeline_id}{end}")
            self.__launch_job(job)

    def __check_evolution_of_jobs_set(self, jobs_list):  # , pipeline_id):
        """
        Given a list of jobs, monitor their progress until the condition
        to say their pipeline has ended.
        :param jobs_list: list
        :return:
        """
        _previous_complete, _complete, _manual, _ctr = 0, -1, 0, 0
        while _complete + _manual != len(jobs_list):
            refreshed_jobs = [self.__get_job(job.id)
                              for job in jobs_list]
            status_msg, _previous_complete = "", _complete
            _complete, _manual = [0]*2
            for job in refreshed_jobs:
                if job.status in ['success']:
                    color = green
                    _complete += 1
                elif job.status in ['canceled', 'failed']:
                    color = red
                    _complete += 1
                elif job.status in ['created', 'pending']:
                    color = orange
                elif job.status in ['manual']:
                    color = bold
                    _manual += 1
                else:
                    color = blue
                status_msg = f"{status_msg}{job.name}: " \
                             f"{color}{job.status}{end}\n"
            # FIXME: this logging has been made to see but minimal printing
            #  maybe overengineered and it's something to be encapsulated
            if self.is_in_trace():
                # _pipeline_obj = self.__get_pipeline(pipeline_id)
                # try:
                #     _pipeline_status = _pipeline_obj.status
                # except:
                #     _pipeline_status = f'{bold}unknown{end}'
                if _previous_complete != _complete:
                    if _ctr != 0:
                        sys.stdout.write("\r")
                        for _ in range(len(jobs_list)+1):
                            sys.stdout.write("\x1b[2K")  # delete the last line
                            sys.stdout.write("\x1b[1A")  # cursor up one line
                    sys.stdout.write(
                        f"jobs status({_ctr*DEFAULT_PIPELINE_PROGRESS_CHECK}):"
                        f"\n{status_msg}")
                    sys.stdout.flush()
                sys.stdout.write(".")  # just to know that it is alive
                sys.stdout.flush()
            # FIXME: empirical
            sleep(DEFAULT_PIPELINE_PROGRESS_CHECK)
            _ctr += 1
        sys.stdout.write("\r")

    def __test_jobs(self, pipeline_id):
        _pipeline_obj = self.__uprev_mngr_obj.origin.pipelines.get(pipeline_id)
        _pipeline_jobs = _pipeline_obj.jobs.list(all=True)
        return self.__uprev_mngr_obj.test_jobs(
            [self.__get_job(job.id) for job in _pipeline_jobs])

    def __review_failure_cause(self, pipeline_id, attempt,
                               pipeline_failure_artifacts):
        self.debug(f"Review failure cause of pipeline "
                   f"{yellow}{pipeline_id}{end}")
        issues = {k: 0 for k in list(JobFailCause)}
        if pipeline_id not in self.__failed_jobs_info:
            self.__failed_jobs_info[pipeline_id] = {}
        if attempt not in self.__failed_jobs_info[pipeline_id]:
            self.__failed_jobs_info[pipeline_id][attempt] = {}
        failed_jobs_info = self.__failed_jobs_info[pipeline_id][attempt]
        for job_artifact in pipeline_failure_artifacts.values():
            job_name = job_artifact.job_name
            job_status = job_artifact.job_status
            if job_artifact.job_status in ['success']:
                # self.debug(f"job {yellow}{job_artifact.job_name}{end} "
                #            f"ends with {green}{job_artifact.job_status}{end} "
                #            f"status")
                if job_name in failed_jobs_info:
                    self.debug(f"Job {yellow}{job_name}{end} "
                               f"has {green}{job_status}{end} "
                               f"in this retry.")
                continue
            if job_name not in failed_jobs_info:
                failed_jobs_info[job_name] = {}
            if not job_artifact.artifact_exists:
                cause = JobFailCause.CI_issue
                issues[cause] += 1
                self.__store_test_by_result(job_artifact, failed_jobs_info)
                self.debug(f"job {yellow}{job_name}{end} "
                           f"ends with {red}{job_status}{end} "
                           f"due to {red}{cause.name}{end}")
            lst = list(job_artifact.content_per_result.keys())
            if len(lst) == 1 and JobFailCause.UnexpectedPass.name in lst:
                cause = JobFailCause.UnexpectedPass
                issues[cause] += 1
                self.__store_test_by_result(job_artifact, failed_jobs_info)
                tests = pformat(job_artifact.content_per_result)
                self.debug(f"job {yellow}{job_name}{end} "
                           f"ends with {red}{job_status}{end} due "
                           f"to {red}{cause.name}{end}:\n{tests}\n")
            elif JobFailCause.Crash.name in lst:
                cause = JobFailCause.Crash
                issues[cause] += 1
                self.__store_test_by_result(job_artifact, failed_jobs_info)
                tests = pformat(job_artifact.content_per_result)
                self.debug(f"job {yellow}{job_name}{end} "
                           f"ends with {red}{job_status}{end} due "
                           f"to {red}{cause.name}{end}:\n{tests}")
            elif JobFailCause.Fail.name in lst:
                cause = JobFailCause.Fail
                issues[cause] += 1
                self.__store_test_by_result(job_artifact, failed_jobs_info)
                tests = pformat(job_artifact.content_per_result)
                self.debug(f"job {yellow}{job_name}{end} "
                           f"ends with {red}{job_status}{end} due "
                           f"to {red}{cause.name}{end}:\n{tests}")
            else:
                cause = JobFailCause.OtherTests
                issues[cause] += 1
                self.__store_test_by_result(job_artifact, failed_jobs_info)
                tests = pformat(job_artifact.content_per_result)
                self.debug(f"job {yellow}{job_name}{end} "
                           f"ends with {red}{job_status}{end} "
                           f"due to {red}{lst}{end}:\n{tests}")
            if len(list(failed_jobs_info[job_name].keys())) > 1:
                lst = ','.join(f"{red}{status}{end}"
                               for status in failed_jobs_info[job_name].keys())
                self.debug(f"job {yellow}{job_name}{end} had more than one "
                           f"status in the retries: {lst}")
        self.debug(f"issues summary: {issues}")
        if issues[JobFailCause.OtherTests] > 0:
            return JobFailCause.OtherTests
        elif issues[JobFailCause.CI_issue] > 0:
            return JobFailCause.CI_issue
        elif issues[JobFailCause.Crash] > 0:
            return JobFailCause.Crash
        elif issues[JobFailCause.Fail] > 0:
            return JobFailCause.Fail
        elif issues[JobFailCause.UnexpectedPass] > 0:
            return JobFailCause.UnexpectedPass
        raise RuntimeError("Ups! This should never happen")

    # def __find_previous_success_uprev(self):
    #     yesterday_pipelines = self.__get_uprev_from_yesterday()
    #     if len(yesterday_pipelines) == 0:
    #         self.warning(f"No success pipelines from yesterday!")
    #     success = pformat([pipeline.ref for pipeline in yesterday_pipelines])
    #     self.info(f"success pipelines from yesterday: {success}")

    def __failed_jobs_info_to_text(self):
        # FIXME: this is an ugly set of nested loops
        lines = []
        for pipeline_id in self.__failed_jobs_info:
            pipeline_obj = \
                self.__uprev_mngr_obj.origin.pipelines.get(pipeline_id)
            pipeline_link = f"[{pipeline_id}]({pipeline_obj.web_url})"
            pipeline_set = self.__failed_jobs_info[pipeline_id]
            for attempt in pipeline_set:
                line = f"\nIn the attempt {attempt} " \
                       f"on the pipeline {pipeline_link}:\n"
                lines.append(line)
                failed_jobs_info = pipeline_set[attempt]
                jobs_as_list = list(failed_jobs_info.keys())
                jobs_header = '|'.join(jobs_as_list)
                line = f"| |{jobs_header}|"
                lines.append(line)
                line = f"|-|{':-:|'*len(jobs_as_list)}"
                lines.append(line)
                rows = {}
                for job_name in failed_jobs_info:
                    failed_tests_in_job = failed_jobs_info[job_name]
                    for test_result in failed_tests_in_job:
                        tests = failed_tests_in_job[test_result]
                        for test_name in tests:
                            if test_name not in rows:
                                rows[test_name] = {}
                            if job_name not in rows[test_name]:
                                rows[test_name][job_name] = test_result
                for test_name, cells in rows.items():
                    line = f"|{test_name}"
                    for job_name in jobs_as_list:
                        if job_name in cells:
                            line = f"{line}|{cells[job_name]}"
                        else:
                            line = f"{line}|"
                    line = f"{line}|"
                    lines.append(line)
        return '\n'.join(lines)

    def __patch_expectations_in_branch(self, collection):
        temporary_storage = tempfile_mkdtemp()
        current_workdir = os_getcwd()
        try:
            os_chdir(temporary_storage)
            expectation_outputs = collection.build_output(patch=True,
                                                          collect_objs=True)
            for output_obj in expectation_outputs:
                if isinstance(output_obj, Patch):
                    expectationspatch_obj = output_obj
                    break
            else:
                raise RuntimeError(f"No Patch object found")
            self.debug(f"patch to update expectations in "
                       f"{brown}{temporary_storage}/expectations.patch{end}")
            expectationspatch_obj.apply_patch(self.__new_branch_name)
        except Exception as exception:
            self.error(f"Something went wrong patching the branch with "
                       f"updated expectations: {exception}")
            raise exception
        finally:  # if there is an exception it will be executed before reraise
            os_chdir(current_workdir)
            shutil_rmtree(temporary_storage)

    # -- fourth descendant level

    @retry(reraise=True, stop=stop_after_attempt(7),
           wait=wait_exponential(multiplier=1, min=1, max=60))
    def __launch_job(self, job):
        job.play()

    def __manual_trigger_jobs(self, pipeline_id):
        _pipeline_obj = self.__get_pipeline(pipeline_id)
        _pipeline_jobs = _pipeline_obj.jobs.list(all=True)
        return self.__uprev_mngr_obj.manual_trigger_jobs(
            [self.__get_job(job.id) for job in _pipeline_jobs])

    # -- fifth descendant level

    @retry(reraise=True, stop=stop_after_attempt(7),
           wait=wait_exponential(multiplier=1, min=1, max=60))
    def __get_job(self, job_id):
        return self.__uprev_mngr_obj.origin.jobs.get(job_id)

    @retry(reraise=True, stop=stop_after_attempt(7),
           wait=wait_exponential(multiplier=1, min=1, max=60))
    def __get_branch(self, branch_name):
        return self.__uprev_mngr_obj.origin.branches.get(branch_name)

    # @retry(reraise=True, stop=stop_after_attempt(7),
    #        wait=wait_exponential(multiplier=1, min=1, max=60))
    # def __get_uprev_from_yesterday(self):
    #     # FIXME: this checks the full list of branches. Starting from the
    #     #  oldest even when all are found it continues looking
    #     _per_page = 5
    #     _page = 0
    #     _selected = []
    #     yesterday = (datetime.today() - timedelta(days=1)).strftime("%Y%m%d")
    #     day_before = (datetime.today() - timedelta(days=2)).strftime("%Y%m%d")
    #     while True:
    #         list_of_pipelines = self.__pipelines_list(per_page=_per_page,
    #                                                   page=_page)
    #         if len(list_of_pipelines) == 0:  # empty page mean, aren't more
    #             break
    #         for pipeline in list_of_pipelines:
    #             if pipeline.status not in ['success']:
    #                 continue
    #             if not pipeline.ref.startswith(DEFAULT_BRANCH_NAME):
    #                 continue
    #             if pipeline.ref.count(day_before):
    #                 return _selected
    #             if not pipeline.ref.count(yesterday):
    #                 continue
    #             _selected.append(pipeline)
    #         _page += 1
    #     return _selected

    @retry(reraise=True, stop=stop_after_attempt(7),
           wait=wait_exponential(multiplier=1, min=1, max=60))
    def __get_pipeline(self, pipeline_id):
        return self.__uprev_mngr_obj.origin.pipelines.get(pipeline_id)

    @retry(reraise=True, stop=stop_after_attempt(7),
           wait=wait_exponential(multiplier=1, min=1, max=60))
    def __get_commit(self, commit_id):
        return self.__uprev_mngr_obj.origin.commits.get(commit_id)

    @staticmethod
    def __store_test_by_result(job_artifact, failed_jobs_info):
        job_name = job_artifact.job_name
        for test_result in job_artifact.content_per_result:
            if test_result not in failed_jobs_info[job_name]:
                failed_jobs_info[job_name][test_result] = []
            failed_jobs_info[job_name][test_result] += \
                job_artifact.content_per_result[test_result]

    # -- sixth descendant level

    @retry(reraise=True, stop=stop_after_attempt(7),
           wait=wait_exponential(multiplier=1, min=1, max=60))
    def __pipelines_list(self, *args, **kwargs):
        return self.__uprev_mngr_obj.origin.pipelines.list(*args, **kwargs)
