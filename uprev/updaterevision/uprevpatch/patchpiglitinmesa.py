#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from .abstractpatch import AbstractPatch
from copy import copy
from ..defaults import brown, red, bold, yellow, end
from os import fsync as os_fsync
from os import makedirs as os_makedirs
from os import remove as os_remove
from os.path import exists as os_path_exists
from re import search as re_search

REF_PATTERN = r"git checkout (\w.*)"


class PatchPiglitInMesa(AbstractPatch):
    __pattern_ref_found = None

    __file_descriptor = None

    # -- first descendant level

    def _initialise_parser(self):
        self.__pattern_ref_found = False

    def _update_references(self):
        self.__update_piglit_references()

    # -- second descendant level

    def get_files_to_patch(self):
        self.__get_build_piglit_sh()

    def __update_piglit_references(self):
        _absolute_temp_file_name = \
            f"{self.temporary_storage}/b/{self.uprev_patch_file}"
        self._pattern_actions = {
            REF_PATTERN: self.__replace_commit,
        }
        with open(_absolute_temp_file_name, 'r+b') as self.__file_descriptor:
            self.debug(f"open {brown}{_absolute_temp_file_name}{end}")
            i, line = 1, self.__file_descriptor.readline().decode()
            while line is not None:
                self._parse_line(i, line)
                if len(self._pattern_actions) == 0:
                    self.debug(f"No more patterns to work with, "
                               f"parsing {red}stop{end} condition")
                    break
                line = self.__file_descriptor.readline().decode()
                i += 1
        self.__file_descriptor = None

    # -- third descendant level

    @staticmethod
    def __line_match(pattern, line):
        return re_search(pattern, line) is not None

    def __get_build_piglit_sh(self):
        _relative_file_name = self.uprev_patch_file
        _path_to_tmp = f"{self.temporary_storage}/a"
        _absolute_temp_file_name = f"{_path_to_tmp}/{_relative_file_name}"
        if not os_path_exists(_path_to_tmp):
            self.error(f"Directory {_path_to_tmp} doesn't exist!")
            return False
        if os_path_exists(_absolute_temp_file_name):
            self.warning(f"File {brown}{_absolute_temp_file_name}{end} found. "
                         f"Remove before get it from git.")
            os_remove(_absolute_temp_file_name)
        if _relative_file_name.count('/') > 0:
            _relative_directories, _ = _relative_file_name.rsplit('/', 1)
            os_makedirs(f"{_path_to_tmp}/{_relative_directories}")
        try:
            _bytes_stream = self.origin_project_files_obj.get(
                _relative_file_name, ref=self.origin_branch_name).decode()
        except Exception as exception:
            self.error(f"Found an {red}{type(exception)}{end} error: "
                       f"{bold}{exception}{end} "
                       f"for {brown}{_relative_file_name}{end}")
            return False
        with open(_absolute_temp_file_name, 'x') as f:
            f.write(_bytes_stream.decode())
        return True

    def __replace_commit(self, number, line):
        self.debug(f"found line {number}: {line!r}")
        self._pattern_actions.pop(REF_PATTERN)
        old_hash = re_search(REF_PATTERN, line).group(1)
        self.debug(f"replace hash {yellow}{old_hash}{end} by "
                   f"{yellow}{self.revision_commit_id}{end}")
        self.__replace_same_size_string(line, old_hash, self.revision_commit_id)
        self.__pattern_ref_found = True

    # -- fourth descendant level

    def __replace_same_size_string(self, line, old, new):
        if self.__file_descriptor is None:
            return
        if len(old) != len(new):
            raise AssertionError(f"replacement by a same size string "
                                 f"is mandatory")
        self.__file_descriptor.seek(-len(line), 1)
        self.__file_descriptor.write(line.replace(old, new).encode())
        self.__file_descriptor.flush()
        os_fsync(self.__file_descriptor.fileno())
