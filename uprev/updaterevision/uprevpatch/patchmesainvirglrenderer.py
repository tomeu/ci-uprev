#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"


from .abstractpatch import AbstractPatch

from ..defaults import brown, red, bold, yellow, end
from os import fsync as os_fsync
from re import search as re_search


VARIABLES_PATTERN = "^variables:\n$"
MESA_PIPELINE_ID_PATTERN = r"MESA_PIPELINE_ID: (\d+)"
INCLUDE_PATTERN = "^include:\n$"
PROJECT_MESA_PATTERN = r"project: '(\w.*)/mesa'"
REF_PATTERN = r"ref: (\w.*)"


class PatchMesaInVirglrenderer(AbstractPatch):
    __pattern_variables_found = None
    __pattern_mesa_pipeline_id_found = None
    __pattern_include_found = None
    __pattern_project_mesa_found = None
    __pattern_ref_found = None

    __file_descriptor = None

    # -- first descendant level

    def _initialise_parser(self):
        self.__pattern_variables_found = False
        self.__pattern_mesa_pipeline_id_found = False
        self.__pattern_include_found = False
        self.__pattern_project_mesa_found = False
        self.__pattern_ref_found = False

    def _update_references(self):
        self.__update_mesa_references()

    # -- second descendant level

    def __update_mesa_references(self):
        _absolute_temp_file_name = \
            f"{self.temporary_storage}/b/{self.uprev_patch_file}"
        self._pattern_actions = {
            VARIABLES_PATTERN: self.__variables_group,
            MESA_PIPELINE_ID_PATTERN: self.__replace_pipeline_id,
            INCLUDE_PATTERN: self.__include_group,
            PROJECT_MESA_PATTERN: self.__in_mesa_project,
            REF_PATTERN: self.__replace_commit,
        }
        with open(_absolute_temp_file_name, 'r+b') as self.__file_descriptor:
            self.debug(f"open {brown}{_absolute_temp_file_name}{end}")
            i, line = 1, self.__file_descriptor.readline().decode()
            while line is not None:
                self._parse_line(i, line)
                if len(self._pattern_actions) == 0:
                    self.debug(f"No more patterns to work with, "
                               f"parsing {red}stop{end} condition")
                    break
                line = self.__file_descriptor.readline().decode()
                i += 1
        self.__file_descriptor = None

    # -- third descendant level

    def __variables_group(self, number, line):
        self.debug(f"found line {number}: {line!r}")
        self._pattern_actions.pop(VARIABLES_PATTERN)
        self.__pattern_variables_found = True

    def __replace_pipeline_id(self, number, line):
        if self.__pattern_variables_found:
            self.debug(f"found line {number}: {line!r}")
            self._pattern_actions.pop(MESA_PIPELINE_ID_PATTERN)
            old_id = re_search(MESA_PIPELINE_ID_PATTERN, line).group(1)
            if int(old_id) >= int(self.revision_pipeline_id):
                raise AssertionError(f"Cannot replace by an older pipeline "
                                     f"({self.revision_pipeline_id}) than the "
                                     f"current one ({old_id})")
            self.debug(f"replace pipeline id {yellow}{old_id}{end} "
                       f"by {yellow}{self.revision_pipeline_id}{end}")
            self.__replace_same_size_string(line, old_id,
                                            f"{self.revision_pipeline_id}")
            # FIXME: this doesn't work when the old pipeline id is < 1M
            #  and the newer is above that million
            self.__pattern_mesa_pipeline_id_found = True

    def __include_group(self, number, line):
        self.debug(f"found line {number}: {line!r}")
        self._pattern_actions.pop(INCLUDE_PATTERN)
        self.__pattern_include_found = True

    def __in_mesa_project(self, number, line):
        if self.__pattern_include_found:
            self.debug(f"found line {number}: {line!r}")
            self._pattern_actions.pop(PROJECT_MESA_PATTERN)
            # FIXME: this cannot be done because the forks can have different
            #  sizes and the replacement doesn't change the size of the file.
            #  Solve that, will also solve the issue of the overflow on the
            #  pipeline id when reach 1M
            # old_mesa_fork = re_search(PROJECT_MESA_PATTERN, line).group(1)
            # if old_mesa_fork != self.__mesa_fork:
            #     self.debug(f"replace mesa fork {yellow}{old_mesa_fork}{end} "
            #                f"by {yellow}{self.__mesa_fork}{end}")
            #     self.__replace_same_size_string(line, old_mesa_fork,
            #                                     self.__mesa_fork)
            self.__pattern_project_mesa_found = True

    def __replace_commit(self, number, line):
        if self.__pattern_include_found and self.__pattern_project_mesa_found:
            self.debug(f"found line {number}: {line!r}")
            self._pattern_actions.pop(REF_PATTERN)
            old_hash = re_search(REF_PATTERN, line).group(1)
            self.debug(f"replace hash {yellow}{old_hash}{end} by "
                       f"{yellow}{self.revision_commit_id}{end}")
            self.__replace_same_size_string(line, old_hash, self.revision_commit_id)
            self.__pattern_ref_found = True

    # -- fourth descendant level

    def __replace_same_size_string(self, line, old, new):
        if self.__file_descriptor is None:
            return
        if len(old) != len(new):
            raise AssertionError(f"replacement by a same size string "
                                 f"is mandatory")
        self.__file_descriptor.seek(-len(line), 1)
        self.__file_descriptor.write(line.replace(old, new).encode())
        self.__file_descriptor.flush()
        os_fsync(self.__file_descriptor.fileno())
