#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from ...abstract import origin_branch_name
from copy import copy
from ..defaults import (DEFAULT_PATCH_FILE_NAME, bold, brown, red, purple, end)
from os import getcwd as os_getcwd
from os import system as os_system
from os import makedirs as os_makedirs
from os import remove as os_remove
from os.path import exists as os_path_exists
from os.path import getsize as os_path_getsize
from pint import Quantity, UnitRegistry
from re import search as re_search
from shutil import copytree as shutil_copytree
from shutil import rmtree as shutil_rmtree
from tempfile import mkdtemp as tempfile_mkdtemp
from uprev.abstract import Logger


class AbstractPatch(Logger):
    __uprev_obj = None

    __origin_project_name = None
    __origin_project_files_obj = None
    __origin_branch_name = None
    __revision_project_name = None
    __revision_commit_id = None
    __revision_pipeline_id = None

    __temporary_storage = None

    _pattern_actions = None

    def __init__(self, uprev_object, commit_id, pipeline_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__uprev_obj = uprev_object
        self.__origin_project_name = self.__uprev_obj.origin.project
        self.__origin_project_files_obj = self.__uprev_obj.origin.files
        self.__origin_branch_name = origin_branch_name() \
                                    or self.__uprev_obj.origin.default_branch
        self.__revision_project_name = self.__uprev_obj.revision.project
        self.__revision_commit_id = commit_id
        self.__revision_pipeline_id = pipeline_id
        self._initialise_parser()
        if self._prepare_files_to_patch():
            self._update_references()
            self._generate_patch()

    def __str__(self):
        return "output patch"

    def __del__(self):
        if self.__temporary_storage is not None:
            self.debug(f"Remove {brown}{self.__temporary_storage}{end}")
            shutil_rmtree(self.__temporary_storage)

    @property
    def origin_project_name(self):
        return self.__origin_project_name

    @property
    def origin_project_files_obj(self):
        return self.__origin_project_files_obj

    @property
    def origin_branch_name(self):
        return self.__origin_branch_name

    @property
    def revision_project_name(self):
        return self.__revision_project_name

    @property
    def revision_commit_id(self):
        return self.__revision_commit_id

    @property
    def revision_pipeline_id(self):
        return self.__revision_pipeline_id

    @property
    def temporary_storage(self):
        return self.__temporary_storage

    @property
    def uprev_patch_file(self):
        return self.__uprev_obj.files_to_patch.__next__()

    # -- first descendant level

    def _initialise_parser(self):
        raise NotImplementedError  # subclasses must implement that

    def _prepare_files_to_patch(self):
        """
        Prepare a temporary directory with the current content in the gitlab
        project, so the suggestions from the artifacts can be applied to later
        generate a patch file.
        :return:
        """
        if self.temporary_storage is None:
            self._use_temporary_storage()
        a_directory = f"{self.temporary_storage}/a/"
        b_directory = f"{self.temporary_storage}/b/"
        if not os_path_exists(a_directory):
            os_makedirs(a_directory)
        self._get_files_to_patch()
        shutil_copytree(a_directory, b_directory)
        return True

    def _update_references(self):
        raise NotImplementedError  # subclasses must implement that

    def _generate_patch(self):
        patch_file_name = DEFAULT_PATCH_FILE_NAME.replace(
            '{revision}', self.__revision_project_name.split('/')[1])
        patch_file_name = patch_file_name.replace(
            '{origin}', self.__origin_project_name.split('/')[1])
        output_file = f"{os_getcwd()}/{patch_file_name}"
        os_system(f"cd {self.temporary_storage}; "
                  f"diff -Nru a b > {output_file}")
        file_size = Quantity(os_path_getsize(output_file),
                             UnitRegistry().byte).to_compact()
        self.info(f"Generated a {purple}{file_size:.3f}{end} file with "
                  f"'{brown}./{patch_file_name}{end}' name")

    # -- second descendant level

    def _use_temporary_storage(self):
        if self.__temporary_storage is None:
            self.__temporary_storage = tempfile_mkdtemp()
            self.debug(f"Create {brown}{self.__temporary_storage}{end}")

    def _get_files_to_patch(self):
        for file_name in self.__uprev_obj.files_to_patch:
            self._get_file_to_patch(file_name)

    # -- third descendant level

    def _get_file_to_patch(self, relative_file_name):
        _absolute_temp_file_name = \
            f"{self.temporary_storage}/a/{relative_file_name}"
        _path_to_tmp = f"{self.temporary_storage}/a"
        _absolute_temp_file_name = f"{_path_to_tmp}/{relative_file_name}"
        if not os_path_exists(_path_to_tmp):
            self.error(f"Directory {_path_to_tmp} doesn't exist!")
            return False
        if os_path_exists(_absolute_temp_file_name):
            self.warning(f"File {brown}{_absolute_temp_file_name}{end} found. "
                         f"Remove before get it from git.")
            os_remove(_absolute_temp_file_name)
        if relative_file_name.count('/') > 0:
            _relative_directories, _ = relative_file_name.rsplit('/', 1)
            os_makedirs(f"{_path_to_tmp}/{_relative_directories}")
        try:
            _bytes_stream = self.origin_project_files_obj.get(
                relative_file_name, ref=self.origin_branch_name).decode()
        except Exception as exception:
            self.error(f"Found an {red}{type(exception)}{end} error: "
                       f"{bold}{exception}{end} "
                       f"for {brown}{relative_file_name}{end}")
            return False
        with open(_absolute_temp_file_name, 'x') as f:
            f.write(_bytes_stream.decode())
        return True

    def _parse_line(self, number, line):
        for pattern, action in copy(self._pattern_actions).items():
            if self._line_match(pattern, line):
                action(number, line)

    @staticmethod
    def _line_match(pattern, line):
        return re_search(pattern, line) is not None