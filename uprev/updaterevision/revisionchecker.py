#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"


from ..abstract import (Logger, origin_gitlab_url,
                        origin_gitlab_fork_project_path, origin_gitlab_token,
                        origin_branch_name, origin_workbranch_prefix,
                        revision_gitlab_url, revision_gitlab_project_path,
                        revision_fix_pipeline_id,
                        revision_initial_pipeline_search)
from datetime import datetime
from .defaults import (DEFAULT_PIPELINE_STATUS, DEFAULT_PIPELINE_OWNER,
                       blue, purple, red, green, yellow, end)
from gitlab import GitlabGetError
from .gitlabwrapper import GitlabWrapper
from .pairs import build_uprev_pair
from .uprevpatch import PatchMesaInVirglrenderer, PatchPiglitInMesa
from .workingbranch import WorkingBranch
from re import match as re_match
from validators import url as validator_url


class RevisionChecker(Logger):
    __origin_gitlab_url = None
    __origin_gitlab_project_path = None
    __revision_gitlab_url = None
    __revision_gitlab_project_path = None
    __uprev_mngr_obj = None

    __origin_branch_name = None
    __origin_workbranch_prefix = None
    __revision_fix_pipeline_id = None
    __revision_initial_pipeline_search = None

    __origin_obj = None
    __revision_obj = None
    __revision_commit = None  # that has passed the virgl jobs
    __revision_pipeline_id = None
    __working_branch = None

    def __init__(self, *args, **kwargs):
        super(RevisionChecker, self).__init__(*args, **kwargs)
        t0 = datetime.now()
        self.__prepare_environment()
        if self.__revision_fix_pipeline_id is None:
            self.__search_last_stable_commit_in_revision()
        try:
            patch_obj = self.__prepare_uprev_patch()
        except AssertionError as exception:
            self.error(exception)
            return
        self.__working_branch = WorkingBranch(self.__uprev_mngr_obj, patch_obj,
                                              self.__origin_workbranch_prefix,
                                              debug=self.is_in_debug(),
                                              trace=self.is_in_trace())
        self.__working_branch.proceed()
        self.debug(f"Process completed in {purple}{datetime.now() - t0}{end} "
                   f"seconds")

    def __del__(self):
        if self.__origin_obj is not None:
            del self.__origin_obj
        if self.__revision_obj is not None:
            del self.__revision_obj

    def __str__(self):
        return "RevisionChecker"

    def __repr__(self):
        return f"RevisionChecker({self.__origin_obj}{self.__revision_obj})"

    # -- first descendant level

    def __prepare_environment(self):
        self.__origin_gitlab_url = origin_gitlab_url()
        self.__origin_gitlab_project_path = origin_gitlab_fork_project_path()
        self.__revision_gitlab_url = revision_gitlab_url()
        self.__revision_gitlab_project_path = revision_gitlab_project_path()
        self.__input_sanitization()
        self.__origin_obj = self.__build_wrapper(
            self.__origin_gitlab_url, self.__origin_gitlab_project_path,
            origin_gitlab_token())
        self.__revision_obj = self.__build_wrapper(
            self.__revision_gitlab_url, self.__revision_gitlab_project_path)
        self.__uprev_mngr_obj = build_uprev_pair(self.__origin_obj,
                                                 self.__revision_obj)
        self.__origin_branch_name = origin_branch_name() \
                                    or self.__origin_obj.default_branch
        self.__origin_workbranch_prefix = \
            f"{origin_workbranch_prefix()}-" \
            f"{self.__revision_gitlab_project_path.split('/')[1]}"
        self.__revision_fix_pipeline_id = revision_fix_pipeline_id()
        self.__revision_initial_pipeline_search = \
            revision_initial_pipeline_search()
        self.__input_sanitization_with_dependencies()
        if self.__origin_branch_name is None:
            self.__origin_branch_name = self.__origin_obj.default_branch

    def __search_last_stable_commit_in_revision(self):
        for _pipeline_candidate in \
                self.__revision_obj.iterate_pipelines(status='success'):
            if self.__revision_initial_pipeline_search is not None and \
                    int(_pipeline_candidate.id) >= \
                    int(self.__revision_initial_pipeline_search):
                print(f"Ignoring {yellow}{_pipeline_candidate.id}{end} "
                      f"until find {self.__revision_initial_pipeline_search}",
                      end='\r')
                continue
            self.debug(f"check if {yellow}{_pipeline_candidate.id}{end} "
                       f"pipeline meets the conditions")
            if not self.__status_condition(_pipeline_candidate):
                continue
            if not self.__user_owner(_pipeline_candidate):
                continue
            if not self.__candidate_pipeline_jobs_present(_pipeline_candidate):
                continue
            self.__revision_commit = _pipeline_candidate.sha
            self.__revision_pipeline_id = _pipeline_candidate.id
            self.info(f"Found the commit {yellow}{self.__revision_commit}{end} "
                      f"as the one that meets the conditions in pipeline "
                      f"{yellow}{self.__revision_pipeline_id}{end}")
            if self.is_in_debug():
                _commit_obj = \
                    self.__revision_obj.commits.get(self.__revision_commit)
                self.debug(
                    f"commit: {blue}{_commit_obj.web_url}{end} "
                    f"pipeline: {blue}{_pipeline_candidate.web_url}{end}")
            break
        else:
            # FIXME
            raise Exception("No pipeline succeed")

    def __prepare_uprev_patch(self):
        return self.__uprev_mngr_obj.patch_constructor(
            uprev_object=self.__uprev_mngr_obj,
            commit_id=self.__revision_commit,
            pipeline_id=self.__revision_pipeline_id,
            debug=self.is_in_debug(), trace=self.is_in_trace())

    # -- second descendant level

    def __input_sanitization(self):
        self.debug(
            f"input received:\n"
            f"- origin_gitlab_url: {self.__origin_gitlab_url}\n"
            f"- origin_gitlab_project_path: "
            f"{self.__origin_gitlab_project_path}\n"
            f"- revision_gitlab_url: {self.__revision_gitlab_url}\n"
            f"- revision_gitlab_project_path: "
            f"{self.__revision_gitlab_project_path}")
        self.__url_validation(self.__origin_gitlab_url,
                              self.__origin_gitlab_project_path)
        self.__url_validation(self.__revision_gitlab_url,
                              self.__revision_gitlab_project_path)

    def __build_wrapper(self, _url, _project, _token=None):
        return GitlabWrapper(_url, _project, _token, debug=self.is_in_debug(),
                             trace=self.is_in_trace())

    def __input_sanitization_with_dependencies(self):
        self.debug(
            f"input received:\n"
            f"- origin_branch_name: {self.__origin_branch_name}\n"
            f"- origin_workbranch_prefix: {self.__origin_workbranch_prefix}\n"
            f"- revision_fix_pipeline_id: {self.__revision_fix_pipeline_id}\n"
            f"- revision_initial_pipeline_search: "
            f"{self.__revision_initial_pipeline_search}")
        if self.__origin_branch_name is not None:
            self.__validate_branch_exist(self.__origin_obj,
                                         self.__origin_branch_name)
        if self.__origin_workbranch_prefix is not None:
            if not isinstance(self.__origin_workbranch_prefix, str):
                raise AssertionError("Review the content of the "
                                     "ORIGIN_BRANCH_PREFIX")
        if self.__revision_fix_pipeline_id is not None:
            self.__validate_pipeline_id(self.__revision_fix_pipeline_id)
        if self.__revision_initial_pipeline_search is not None:
            self.__pipeline_string_validation(
                self.__revision_initial_pipeline_search)

    def __status_condition(self, pipeline):
        if hasattr(pipeline, 'status') and \
               pipeline.status in [DEFAULT_PIPELINE_STATUS]:
            self.debug(f"\tpipeline {yellow}{pipeline.id}{end} "
                       f"{green}is{end} in {DEFAULT_PIPELINE_STATUS} "
                       f"status")
            return True
        else:
            self.debug(f"\tpipeline {yellow}{pipeline.id}{end} "
                       f"{red}is not{end} in {DEFAULT_PIPELINE_STATUS} "
                       f"status ({pipeline.status})")
            return False

    def __user_owner(self, pipeline, user=DEFAULT_PIPELINE_OWNER):
        if hasattr(pipeline, 'user') and \
                'username' in pipeline.user and \
                pipeline.user['username'] == user:
            self.debug(f"\tpipeline {yellow}{pipeline.id}{end} "
                       f"{green}has{end} {DEFAULT_PIPELINE_OWNER} "
                       f"as owner")
            return True
        else:
            self.debug(f"\tpipeline {yellow}{pipeline.id}{end} "
                       f"{red}has not{end} {DEFAULT_PIPELINE_OWNER} "
                       f"as owner")
            return False

    def __candidate_pipeline_jobs_present(self, pipeline):
        _job_names = [job.name for job in pipeline.jobs.list(all=True)]
        for _job_pattern in \
                self.__uprev_mngr_obj.candidate_pipeline_jobs_present:
            found = False
            for _job_name in _job_names:
                match = re_match(_job_pattern, _job_name)
                if match is not None:
                    found = True
            if not found:
                self.debug(f"\tpipeline {yellow}{pipeline.id}{end} jobs "
                           f"pattern {_job_pattern} {red}not found{end}.")
                return False
            else:
                self.debug(f"\t\tpipeline {yellow}{pipeline.id}{end} jobs "
                           f"pattern {_job_pattern} {green}found{end}.")
        self.debug(f"\tpipeline {yellow}{pipeline.id}{end} all the job "
                   f"patterns {green}found{end}.")
        return True


    def __uprev_mesa_in_virglrenderer(self):
        if self.__projects_tuple() == ('mesa', 'virglrenderer'):
            return True
        return False

    def __uprev_piglit_in_mesa(self):
        if self.__projects_tuple() == ('piglit', 'mesa'):
            return True
        return False

    # -- third descendant level

    def __url_validation(self, url, project):
        project_full_name = f"{url}{project}"
        if not validator_url(project_full_name):
            raise AttributeError(
                f"Invalid project url \"{project_full_name}\". "
                f"Review \"--url {url}\" and \"--name {project}\" arguments")
        self.debug(f"validated the address {blue}{project_full_name}{end}")

    @staticmethod
    def __validate_branch_exist(gitlab_wrapper, branch_name):
        try:
            gitlab_wrapper.branches.get(branch_name)
        except GitlabGetError:
            raise AttributeError(f"branch {yellow}{branch_name}{end} not found "
                                 f"in {blue}{gitlab_wrapper.full_url}{end}")

    def __validate_pipeline_id(self, fix_pipeline_id):
        self.__pipelines = self.__pipeline_string_validation(fix_pipeline_id)
        self.__revision_pipeline_id = self.__pipelines[0]
        self.__commit = \
            self.__revision_obj.pipelines.get(self.__revision_pipeline_id).sha
        self.__revision_commit = self.__commit

    # -- fourth descendant level

    def __pipeline_string_validation(self, pipeline):
        match = re_match(r"^[#]?(\d+)$", pipeline)
        if match is None:
            raise AttributeError("Cannot understand the pipeline id as "
                                 "such. Review the '--pipeline' parameter")
        self.debug(f"validated the pipeline string {yellow}{pipeline!r}{end}")
        return [int(match.group(1))]

    def __projects_tuple(self):
        if self.__origin_gitlab_project_path.count('/'):
            origin_project = self.__origin_gitlab_project_path.split('/')[1]
        else:
            origin_project = None
        if self.__revision_gitlab_project_path.count('/'):
            revision_project = self.__revision_gitlab_project_path.split('/')[1]
        else:
            revision_project = None
        return revision_project, origin_project
