#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"


from .defaults import blue, green, red, yellow, end
from gitlab import Gitlab, GitlabAuthenticationError, GitlabGetError
from ..abstract import Logger, uprev_gitlab_token, get_job_id, get_project_id
from validators import url as validator_url


class GitlabWrapper(Logger):
    __url = None
    __project = None
    __gitlab_obj = None
    __project_obj = None
    __token = None

    def __init__(self, url, project, token=None, *args, **kwargs):
        super(GitlabWrapper, self).__init__(*args, **kwargs)
        self.__input_sanitization(url, project)
        self.__bind_to_gitlab(token)

    def __del__(self):
        if self.__gitlab_obj is not None:
            self.debug(f"close {self.__project} Gitlab object")
            self.__gitlab_obj.session.close()

    def __str__(self):
        if self.__project is not None:
            return f"{self.__project}"
        return f"GitlabWrapper"

    @property
    def url(self):
        return self.__url

    @property
    def project(self):
        return self.__project

    @property
    def project_id(self):
        return self.__project_obj.id

    @property
    def full_url(self):
        return f"{self.__url}{self.__project}"

    # interface internal components
    @property
    def branches(self):
        return self.__project_obj.branches

    @property
    def commits(self):
        return self.__project_obj.commits

    @property
    def default_branch(self):
        return self.__project_obj.default_branch

    @property
    def files(self):
        return self.__project_obj.files

    @property
    def jobs(self):
        return self.__project_obj.jobs

    @property
    def mergerequests(self):
        return self.__project_obj.mergerequests

    @property
    def pipelines(self):
        return self.__project_obj.pipelines

    @property
    def issues(self):
        return self.__project_obj.issues

    @property
    def private_token(self):
        return self.__gitlab_obj.private_token

    def iterate_pipelines(self, status=None):
        return self._iterate_member(self.pipelines, status=status,
                                    identifier_name="id")

    def iterate_issues(self, state=None):
        return self._iterate_member(self.issues, state=state,
                                    identifier_name="iid")

    def iterate_merge_requests(self, state=None):
        if state not in [None, 'all', 'merged', 'opened', 'closed']:
            raise AssertionError(f"Trying to iterate merge request of an "
                                 f"invalid status {state}")
        return self._iterate_member(self.mergerequests, state=state,
                                    identifier_name="iid")

    def get_another_project(self, project, token=None):
        return GitlabWrapper(self.__url, project, token,
                             debug=self.is_in_debug())

    # -- first descendant level

    def __input_sanitization(self, url, project):
        self.debug(
            f"input received:\n"
            f"- url: {url}\n"
            f"- project: {project}")
        self.__url_validation(url, project)

    def __bind_to_gitlab(self, token):
        if isinstance(token, str) and len(token) == 0:
            self.warning(f"Received an empty string as token")
            token = None
        try:
            self.__gitlab_obj = self.__build_gitlab_obj(self.__url,
                                                        private_token=token)
            self.info(f"build the gitlab object "
                      f"{blue}{self.__gitlab_obj.url}{end}")
            self.__project_obj = self.__get_gitlab_project()
            self.__project = self.__project_obj.path_with_namespace
            self.info(f"build the project object "
                      f"{blue}{self.__project_obj.web_url}{end}")
        except GitlabAuthenticationError:  # bad token
            raise AttributeError(
                "Incorrect token, review '--token' or '--token-file' argument,"
                "or the environment variables GITLAB_TOKEN{,_FILE}")
        except GitlabGetError:  # bad project name
            raise AttributeError(
                "Project not found, review '--{origin,revision}-project' "
                "arguments, or the environment variables "
                "{,ORIGIN_,REVISION_}GITLAB_PROJECT_PATH")
        except ConnectionError:  # bad url
            raise AttributeError(
                "Connection error, review the '--{origin,revision}-url' "
                "arguments, or the environment variables "
                "{,ORIGIN_,REVISION_}GITLAB_URL")
        if token is not None:
            self.debug(f"bound {green}with{end} token")
            self.__token = token
        else:
            self.debug(f"bound {red}without{end} token")

    def _iterate_member(self, member, state=None, status=None,
                        identifier_name=None):
        """
        Given a member of the project (aka pipelines, issues, jobs, etc) query
        by pages to the object to protect when there are very many of them in
        the system.
        It can be used the state argin to filter by the state the members have.
        It can be used the identifier argin to specify if the member.get has to
        be made with a specific property of the object.
        :param member:
        :param state:
        :param identifier:
        :return:
        """
        _member_name = f"{self.__project}::{member.path.split('/')[-1]}"
        _page, _per_page = 1, 5
        _i = None
        if identifier_name is None:
            identifier_name = "id"  # default
        kwargs = {}
        if state is not None:
            kwargs['state'] = state
        if status is not None:
            kwargs['status'] = status
        while True:
            kwargs['per_page'] = _per_page
            kwargs['page'] = _page
            _page_lst = member.list(**kwargs)
            self.debug(f"{_member_name} page {_page}, {len(_page_lst)} "
                       f"elements")
            for _i, _element in enumerate(_page_lst):
                self.debug(f"{_member_name} page {_page} iteration {_i}: "
                           f"element id {_element.id}")
                try:
                    identifier_value = self.__member_id_value(
                        _member_name, _element, identifier_name)
                    yield self.__member_obj_get(
                        _member_name, member, identifier_value)
                except GitlabGetError as exception:
                    self.warning(f"Exception getting {_member_name} element "
                                 f"with id {_element.id}: {exception}")
            if _i is None:
                self.debug(f"{_member_name} received an empty list")
                break
            if _i < _per_page-1:
                self.debug(f"{_member_name} incomplete page with {_i+1} "
                           f"element(s), so {red}stop{end} condition.")
                break
            _page += 1

    # -- second descendant level

    def __url_validation(self, url, project):
        if url[-1] != "/":
            url = f"{url}/"
        project_full_name = f"{url}{project}"
        if not validator_url(project_full_name):
            raise AttributeError(
                f"Invalid project url \"{project_full_name}\". "
                f"Review \"--url {url}\" and \"--name {project}\" arguments")
        self.debug(f"validated the address {blue}{project_full_name}{end}")
        self.__url = url
        self.__project = project

    def __build_gitlab_obj(self, url, private_token):
        return Gitlab(url, private_token=private_token)

    def __get_gitlab_project(self):
        return self.__gitlab_obj.projects.get(self.__project)

    @staticmethod
    def __member_id_value(name, obj, attribute):
        if hasattr(obj, attribute):
            return getattr(obj, attribute)
        else:
            raise AttributeError(f"{name} object doesn't have attribute "
                                 f"{attribute}")

    def __member_obj_get(self, name, obj, _id):
        try:
            return obj.get(_id)
        except GitlabGetError as exception:
            if exception.response_code == 404:
                self.warning(f"Problem getting an element of {name}. Suggested "
                             f"to check the identifier used to do the get "
                             f"operation")
            raise exception


def get_current_execution_info(wrapper):
    this_project_id = get_project_id()
    if this_project_id is not None:
        this_wrapper_obj = \
            wrapper.get_another_project(this_project_id, uprev_gitlab_token())
        wrapper.debug(f"Build another project wrapper: "
                      f"{yellow}{this_wrapper_obj}{end}")
        current_job = this_wrapper_obj.jobs.get(get_job_id())
        this_wrapper_obj.debug(f"Current job id {yellow}{current_job.id}{end} "
                               f"at {blue}{current_job.web_url}{end}")
        job_link = f"[{current_job.name} ({current_job.id})]" \
                   f"({current_job.web_url})"
        return f"From the execution of {job_link}"
