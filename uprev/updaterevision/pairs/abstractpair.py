#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from ...abstract import (Logger, origin_branch_name,
                         origin_gitlab_main_project_path, uprev_gitlab_url,
                         uprev_project_path, uprev_gitlab_token)
from ..defaults import yellow, end
from ..gitlabwrapper import GitlabWrapper
from ..uprevpatch.abstractpatch import AbstractPatch


class AbstractPair(Logger):
    """
    This tree of inheriting classes that starts at this point are made to
    manage the pair of projects "revision/origin" about the uprev the revision
    in the origin project.
    """
    __origin_obj = None
    __principal_origin_obj = None
    __revision_obj = None
    __uprev_obj = None

    def __init__(self, origin_wrapper, revision_wrapper, *args, **kwargs):
        super().__init__(*args, *kwargs)
        self.__origin_obj = origin_wrapper
        self.__revision_obj = revision_wrapper
        main_project_path = origin_gitlab_main_project_path()
        self.__uprev_obj = GitlabWrapper(uprev_gitlab_url(),
                                         uprev_project_path(),
                                         uprev_gitlab_token(),
                                         debug=self.is_in_debug())
        if main_project_path == self.__origin_obj.project:
            self.__principal_origin_obj = self.__origin_obj
        else:
            self.__principal_origin_obj = self.__origin_obj.get_another_project(
                main_project_path, self.__origin_obj.private_token)
        self.debug(f"The merge requests and FailTest issues will be created in "
                   f"{yellow}{self.__principal_origin_obj.project}{end} and "
                   f"other kind of issues (CI and Unknown) will be in created "
                   f"in {yellow}{self.__uprev_obj.project}{end}")

    def __str__(self):
        return f"{self.__revision_obj.project} uprev " \
               f"{self.__origin_obj.project}"

    @property
    def origin(self):
        return self.__origin_obj

    @property
    def main_origin(self):
        return self.__principal_origin_obj

    @property
    def revision(self):
        return self.__revision_obj

    @property
    def uprev(self):
        return self.__uprev_obj

    @property
    def origin_branch_name(self):
        return origin_branch_name() or self.__origin_obj.default_branch

    @property
    def candidate_pipeline_jobs_present(self) -> list:
        """
        Returns a list of regular expresions for names of jobs in the pipeline
        that is required to be present. If the list is empty, this inhibits
        this filtering.
        :return: list
        """
        raise NotImplementedError  # subclasses must implement that

    @property
    def patch_constructor(self) -> AbstractPatch:
        raise NotImplementedError  # subclasses must implement that

    @property
    def files_to_patch(self):  # -> generator
        raise NotImplementedError  # subclasses must implement that

    def manual_trigger_jobs(self, jobs_list: list) -> list:
        raise NotImplementedError  # subclasses must implement that

    def test_jobs(self, jobs_list: list) -> list:
        raise NotImplementedError  # subclasses must implement that
