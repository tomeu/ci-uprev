#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from .abstractpair import AbstractPair
from ..uprevpatch import PatchPiglitInMesa
from ..uprevpatch.abstractpatch import AbstractPatch


class PiglitInMesa(AbstractPair):

    __mesa_manual_jobs = ['debian/x86_build-base', 'debian/x86_test-base',
                          'debian/arm_build', 'fedora/x86_build',
                          'windows_vs2019']

    @property
    def candidate_pipeline_jobs_present(self) -> list:
        return []

    @property
    def patch_constructor(self) -> AbstractPatch:
        return PatchPiglitInMesa

    @property
    def files_to_patch(self):  # -> generator
        yield ".gitlab-ci/container/build-piglit.sh"

    def manual_trigger_jobs(self, jobs_list: list) -> list:
        _manual_jobs = []
        jobs_list.reverse()
        for job in jobs_list:
            if job.name in self.__mesa_manual_jobs:
                _manual_jobs.append(job)
        self.debug(f"From {len(jobs_list)} found {len(_manual_jobs)} to be "
                   f"triggered manually")
        return _manual_jobs

    def test_jobs(self, jobs_list: list) -> list:
        _test_jobs = []
        for job in jobs_list:
            if job.name not in self.__mesa_manual_jobs:
                _test_jobs.append(job)
        return _test_jobs
