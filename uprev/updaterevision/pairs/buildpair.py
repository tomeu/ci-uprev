#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from .mesainvirglrenderer import MesaInVirglrenderer
from .piglitinmesa import PiglitInMesa


def build_uprev_pair(origin_wrapper, revision_wrapper):
    if origin_wrapper.project.count('/') != 1 or \
            revision_wrapper.project.count('/') != 1:
        raise NameError(f"Project name must have two parts "
                        f"(review {origin_wrapper.project} "
                        f"or {revision_wrapper.project})")
    _origin_fork, _origin_name = origin_wrapper.project.split('/')
    _revision_fork, _revision_name = revision_wrapper.project.split('/')
    pairs = {('virglrenderer', 'mesa'): MesaInVirglrenderer,
             ('mesa', 'piglit'): PiglitInMesa}.get(
        (_origin_name, _revision_name), None)
    if pairs is None:
        raise NotImplementedError(f"Cannot uprev "
                                  f"{_revision_name} in {_origin_name}")
    return pairs(origin_wrapper, revision_wrapper,
                 debug=origin_wrapper.is_in_debug(),
                 trace=origin_wrapper.is_in_trace())
