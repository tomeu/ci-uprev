# Update revision

[[_TOC_]]

From a clone of the repository (or a branch checkout) this can be called by:

```bash
export MERGE_WEEKDAY="all"
export ORIGIN_GITLAB_PROJECT_PATH="sergi/virglrenderer"
export ORIGIN_GITLAB_TOKEN=$VIRGLRENDERER_GITLAB_TOKEN
export ORIGIN_GITLAB_FORK_PROJECT_PATH="sergi/virglrenderer"
export ORIGIN_GITLAB_MAIN_PROJECT_PATH="virgl/virglrenderer"
export REVISION_GITLAB_PROJECT_PATH="mesa/mesa"
export UPREV_GITLAB_TOKEN=(...)
ci-update_revision -d --trace
```

(Note: this has been developed for python 3.10 and 3.9, see the 
[Python environment](README.md#python-environment) section for further 
information)

This tool is made to look for a candidate to move forward the 
*[mesa](https://gitlab.freedesktop.org/mesa/mesa)* used in 
*[virglrenderer](https://gitlab.freedesktop.org/virgl/virglrenderer)* (remember 
that this virglrenderer *origin* and mesa *revision* are in progress to be 
generalised). So, starting from the last commit to *mesa main branch* (or 
other options, See [Specify pipeline](README.md@specify-pipeline)) check if 
there is a pipeline that meets certain conditions:
1. It must have 'succeed'
2. Has to be triggered by [marge](https://gitlab.freedesktop.org/marge-bot)
3. Has to have some jobs following a certain pattern name. Like in the *mesa 
   uprev in virglrenderer* is the need to have *debian-testing* jobs in the 
   mesa pipeline. 

(This criteria may change if we find a better sequence of rules, specially the 
rule 3).

## output

The output of this tool is a Merge Request to the gitlab project specified in 
the environment variable `ORIGIN_GITLAB_MAIN_PROJECT_PATH` that is thought to 
be the principal repo of the project. There will be a working branch to 
prepare this merge request made in the project specified in 
`ORIGIN_GITLAB_MAIN_PROJECT_PATH`.

Eventually this can also provide a patch to update the `expectations` of 
*origin* project (*virglrenderer* in the first use of this tool). There are 
test that were failing but with the *uprev* they are passing now. This uses the
included brother tool [collate results](../collate_results/README.md).

There is another user case to be mention, and that is when the pipeline cannot 
succeed because there are tests failing (and they aren't inhibited in the 
`expectations`). In this case, what the tool does is open an issue in the repo 
where the MR was expected providing information about what had happened that 
block the requested uprev.

There is a variable called `MERGE_WEEKDAY` that can be used to only do the MR
the desired day of the week. But this doesn't stop the issue creation in case 
some problem has been found.

A second and a third type of issues can be created by the tool. In case it 
finds a problem that is related with the CI system, or something that it cannot 
classify in the previous two categories, it will create an issue in the project
that is running the uprev job (the *gfx-ci/ci-uprev* or a fork of it).

## Options and parametrization

This tool will provide some traces in the console, but if someone likes to read
more about the things happening, use the argument `-d` or `--debug`.

There are very many environment variables that this tool uses. See the help of 
the tool for further information.

## Procedure

Once the tool has found a candidate that meets the conditions and has created
the corresponding patch. It will create a branch where this patch is applied 
and the pipeline of this committed change will be started.

The tool will monitor the evolution of the pipeline and will react to that:

1. If the pipeline did **not** *succeed*:
   1. [collate results](../collate_results/README.md) and see why.
      1. If there are test that haven't finished with results:
         * Create an issue (in the ci-uprev project) with the 
           information, to review what happen and how it can be solved.
         * **TBD**: It has to be checked if there is already an issue about 
           the same problem. So it is necessary to distinguish subtypes of this
           situation. Then, if what happen is another sample of something 
           already reported, append this new information to it.
      2. If there are failing test, check the types of them:
         * If there is *only* "UnexpectedPass": lets upgrade the expectations
           and launch the pipeline with them. Go to step 1 with this new
           pipeline execution.
         * If there are other types, like "Fail" or "Crash" (that persist even
           with the retries of the job), create a *merge request* with a `WIP`
           prefix so the developers can see and work on it.
           * Originally here, the idea was to create an issue to the *origin* 
             project with some collected the information. The MR can have this 
             information as well, but allows to have a process to fix it.
         * It has to be check if there is already a MR open that hasn't been 
           merged. In this case, we may have to append to it the new situation.
2. When the resulting pipeline succeed (perhaps directly or with the updated 
   expectations), this is a good candidate to move forward the *revision* 
   project uprev in the *origin* project so:
   * Create a MR to *origin* project to be reviewed by the developers team.
     * Each project could desire a different uprev period. For this feature, 
       there is the `MERGE_WEEKDAY` that could inhibit the creation of the MR
       depending on the day of the week (this could be also extended if 
       developers requests something different).
     * Here, again, it has to be checked if there is already one open but not 
       already merged.


![procedure summary](./procedure.svg)

### Specify pipeline

There are two options (mutually exclusive) to interfere in the pipeline 
selection from the *revision* project. 
1. One is to specify directly, and don't search, the pipeline to go with: 
   `REVISION_FIX_PIPELINE_ID`.
2. It can be also specified that the pipeline to go has to be previous to a
   specific one: `REVISION_INITIAL_PIPELINE_SEARCH`. So the search will start
   below the given pipeline.

### Specify forks and principal repos

There is a [tree of environment variables](../docs/envvars.png) with information
about their default values, in case they have, and also alternative source in 
case one wasn't defined.
```bash
export MERGE_WEEKDAY="all"
export ORIGIN_GITLAB_PROJECT_PATH="sergi/mesa"
export ORIGIN_GITLAB_TOKEN=$MESA_GITLAB_TOKEN
export ORIGIN_GITLAB_FORK_PROJECT_PATH="sergi/mesa"
export ORIGIN_GITLAB_MAIN_PROJECT_PATH="mesa/mesa"
export REVISION_GITLAB_PROJECT_PATH="mesa/piglit"
export UPREV_GITLAB_TOKEN=(...)
ci-update_revision -d --trace
```
Note: remember to set the gitlab token with write access. See the 
[section](#readwrite-access-to-the-repositories) describing it.

### ReadWrite access to the repositories

This tool requires to do actions that require authentication to the server. 
This information can be provided with the environment variable `GITLAB_TOKEN`
or it can be also provided in an indirect way with the `GITLAB_TOKEN_FILE` 
whose content is an string to find a file that contains the token itself.

## TODOs

- [ ] Improve the way the newer point of *revision* project to use.
  - the pipeline has to be triggered by 
    [marge](https://gitlab.freedesktop.org/marge-bot)
    as this is the one that may mean fully tested release.
- [x] Allow to specify the *revision* project reference to use. Saying the 
  pipeline to use or to drive the search to start looking on an older that a 
  known one.
  - This has been thought to help to trace back when something has start to 
    happen.
- [x] Allow to use, as the reference for the branch, a different one than the
  default, that is the master from (the fork in use of) virglrenderer. 
- [x] Manage the reporting exception when happen the [first issue in the 
  procedure](README.md#-merge-request)
- [x] Use the [collate results](https://gitlab.freedesktop.org/sergi/virglrenderer/-/tree/collate_results_tool)
  for the [second stage of the procedure](README.md#-merge-request)
- [x] In case of test failures, like described in the 
  [second stage of the procedure](README.md#-merge-request) generate an issue 
- in *origin* project.
- [ ] Improve the patch that uprev (modify the commit hash and the reference 
  pipeline id) to not be limited to a same size string replacement. 

## Design

### Class diagram

![Class diagram](./class_diagram.svg)

### Sequence diagram

![Sequence diagram](./sequence_diagram.svg)

## Python environment

Help of the python setup to use this tool is the same than what is described 
for all those tools here. Check 
[Python environment](../../python.md#python-environment) section of the tools 
readme.

## Diagrams

The are written using [mermaid](https://mermaid-js.github.io/mermaid/#/) 
(there is a [live editor](https://mermaid.live/))

To regenerate those `*.mmd` files:

```
mermaid-cli -i uprev/updaterevision/procedure.mmd -o uprev/updaterevision/procedure.svg -t forest -b transparent
mermaid-cli -i uprev/updaterevision/class_diagram.mmd -o uprev/updaterevision/class_diagram.svg -t forest -b transparent
mermaid-cli -i uprev/updaterevision/sequence_diagram.mmd -o uprev/updaterevision/sequence_diagram.svg -t forest -b transparent
```