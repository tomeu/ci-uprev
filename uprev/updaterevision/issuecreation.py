#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"


from ..abstract import Logger, blue, yellow, end
from ..abstract import uprev_pipeline_source, uprev_pipeline_is_schedule
from .defaults import DEFAULT_MR_TAGS
from .enumeration import IssueType, IssueTypeExtraInfo
from gitlab.exceptions import GitlabAuthenticationError
from gitlab.v4.objects import ProjectBranch
from .gitlabwrapper import GitlabWrapper, get_current_execution_info


class IssueCreator(Logger):
    __gitlab_wrapper = None
    __working_branch = None
    __title = None
    __description = None
    __template = None
    __extra_info = None
    __issue_id = None

    def __init__(self, wrapper, working_branch, title, description=None,
                 template=None,
                 *args, **kwargs):
        super(IssueCreator, self).__init__(*args, **kwargs)
        self.__sanitize_input(wrapper, working_branch, title, description,
                              template)
        self.__gitlab_wrapper = wrapper
        self.__working_branch = working_branch
        self.__title = title
        self.__description = description
        if template is not None:
            self.__template = list(template.keys())[0]
            self.__extra_info = template[self.__template]
        self.debug(f"Build a issue manager object for "
                   f"{blue}{self.__gitlab_wrapper.full_url}{end}. "
                   f"Issue in branch {yellow}{self.__working_branch.name}{end} "
                   f"with title {yellow}{self.__title}{end}")

    def __str__(self):
        if self.__issue_id is not None:
            return f"IssueCreator({self.__issue_id})"
        return "IssueCreator"

    @property
    def __issue_obj(self):
        if self.__issue_id is not None:
            return self.__gitlab_wrapper.issues.get(self.__issue_id)

    @property
    def __issue_state(self):
        if self.__issue_id is not None:
            return self.__issue_obj.state

    def create(self):
        self.__issue_id = self.__build_issue()

    # -- first descendant level

    @staticmethod
    def __sanitize_input(wrapper, working_branch, title, description, template):
        if not isinstance(wrapper, GitlabWrapper):
            raise AssertionError(f"It is required to have a gitlab wrapper "
                                 f"object")
        if not isinstance(working_branch, ProjectBranch):
            raise AssertionError(f"The working branch name must be a "
                                 f"Gitlab ProjectBranch")
        if not isinstance(title, str):
            raise AssertionError(f"The issue title must be a string")
        if description is None and template is None:
            raise AssertionError(f"Issue description must be set by explicit "
                                 f"text or by the template feature")
        if description is not None:
            if not isinstance(description, str):
                raise AssertionError(f"Description must be a string")
        if template is not None:
            if not isinstance(template, dict):
                raise AssertionError(f"Review the template feature, it must "
                                     f"be a dictionary")
            # check there is only one key and it is in the enumeration
            if len(template) != 1:
                raise AssertionError(f"Template must have one single key")
            _issue_type = list(template.keys())[0]
            if _issue_type not in [_type for _type in IssueType]:
                raise AssertionError(f"The single key of the template must be "
                                     f"one of the enumerated types")
            _extra_info = template[_issue_type]
            if not isinstance(_extra_info, dict):
                raise AssertionError(f"The extra information in the template "
                                     f"must be a dictionary")

            if not set(_extra_info.keys()).issubset(
                    [_type for _type in IssueTypeExtraInfo]):
                raise AssertionError(f"The extra information in the template "
                                     f"must have contain the expected keys")

    def __build_issue(self):
        try:
            if not uprev_pipeline_is_schedule():
                _title = f"Draft: {self.__title}"
                self.info(f"Pipeline source {uprev_pipeline_source()}, tag the "
                          f"issue as draft")
            else:
                _title = f"{self.__title}"
            _description = self.__build_description()
            self.debug(f"Before create an issue check if there is one already "
                       f"with the title {_title!r}")
            _issue_obj = self.__check_existing_issue_titles(_title)
            if _issue_obj is not None:
                self.__append_to_issue(_issue_obj, _description)
            else:
                _issue_obj = self.__create_issue(_title, _description)
            return _issue_obj.iid
        except GitlabAuthenticationError as exception:
            has_token = self.__gitlab_wrapper.private_token is not None and \
                        len(self.__gitlab_wrapper.private_token) > 0
            self.error(f"Authentication problem creating an issue on project "
                       f"{self.__gitlab_wrapper.project} "
                       f"(with{'' if has_token else 'out'} token): "
                       f"{exception}")
            raise exception
        except Exception as exception:
            self.error(f"Something went wrong creating an issue: "
                       f"{type(exception)} -> {exception}")
            raise exception

    # -- second descendant level

    def __build_description(self):
        description = ""
        if self.__template is not None:
            pipeline_obj = self.__extra_info[IssueTypeExtraInfo.pipeline_obj]
            description = {
                IssueType.CI: self.__prepare_ci_cause_description,
                IssueType.Fail: self.__prepare_fail_cause_description,
                IssueType.Unknown: self.__prepare_unknown_cause_description
            }[self.__template](pipeline_obj, self.__working_branch)
            self.debug(f"Created description from template {self.__template}:\n"
                       f"{description!r}")
        if self.__description is not None:
            if self.__template is not None:
                description = f"{description}\n\n"
            description = f"{description}{self.__description}"
            self.debug(f"Message in the description from specified text:\n"
                       f"{description!r}")
        return description

    def __check_existing_issue_titles(self, title):
        for issue_obj in self.__gitlab_wrapper.iterate_issues(state='opened'):
            self.debug(f"issue {issue_obj.iid} has title {issue_obj.title!r}")
            if issue_obj.title == title:
                return issue_obj
        return None

    def __append_to_issue(self, issue_obj, description):
        issue_obj.notes.create({'body': description})
        self.info(f"Append a note to the issue {issue_obj.iid}: "
                  f"{blue}{issue_obj.web_url}{end}")

    def __create_issue(self, title, description):
        issue_obj = self.__gitlab_wrapper.issues.create(
            {'title': title,
             'description': description,
             'labels': DEFAULT_MR_TAGS})
        issue_obj.save()
        self.info(f"build an issue {issue_obj.iid}: "
                  f"{blue}{issue_obj.web_url}{end}")
        return issue_obj

    # -- third descendant level

    # TODO: introduce information about what had happen
    def __prepare_ci_cause_description(self, pipeline_obj, branch_obj):
        header = self.__description_header(pipeline_obj, branch_obj)
        description = f"""{header}

There are issues with the Continuous Integration."""
        return description

    # TODO: introduce information about what had happen
    def __prepare_fail_cause_description(self, pipeline_obj, branch_obj):
        header = self.__description_header(pipeline_obj, branch_obj)
        description = f"""{header}

There are Jobs with failing tests and it is perhaps a regression."""
        return description

    # TODO: introduce information about what had happen
    def __prepare_unknown_cause_description(self, pipeline_obj, branch_obj):
        header = self.__description_header(pipeline_obj, branch_obj)
        description = f"""{header}

There had been an unmanaged issue."""
        return description

    # -- fourth descendant level

    # FIXME: this has to produce a correct markdown text
    def __description_header(self, pipeline_obj, branch_obj):
        preheader = get_current_execution_info(self.__gitlab_wrapper)
        pipeline_link = f"[{pipeline_obj.id}]({pipeline_obj.web_url})"
        branch_link = f"[{branch_obj.name}]({branch_obj.web_url})"
        header = f"check the pipeline {pipeline_link} and what happen with " \
                 f"in the branch {branch_link}."
        if preheader is not None:
            return f"""{preheader}, {header}"""
        else:
            return f"""{header.capitalize()}"""
