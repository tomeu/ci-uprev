#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from .defaults import (DEFAULT_GITLAB_URL, DEFAULT_UPREV_PROJECT_PATH,
                       DEFAULT_MERGEREQUEST_TITLE, DEFAULT_ISSUES_TITLE,
                       DEFAULT_CI_ISSUES_TITLE, DEFAULT_UNKNOWN_ISSUES_TITLE)
from os import environ as os_environ
from os.path import exists as os_path_exists
from os.path import expanduser as os_path_expanduser


def _get_environment_variable(env_name):
    try:
        value = os_environ[env_name]
        if isinstance(value, str) and len(value) > 0:
            return value
        return None
    except KeyError:
        return None


# -- general environment variables

def default_gitlab_url(default=None):
    return _get_environment_variable('GITLAB_URL') or default \
           or DEFAULT_GITLAB_URL


def default_project_path(default=None):
    return _get_environment_variable('GITLAB_PROJECT_PATH') or default


def __prepend_prefix(prefix, variable_name):
    if isinstance(prefix, str):
        return f"{prefix}_{variable_name}"
    else:
        return f"{variable_name}"


def _get_token_envvar(prefix=None):
    return _get_environment_variable(__prepend_prefix(prefix, 'GITLAB_TOKEN'))


def _get_token_file_envvar(prefix=None):
    return _get_environment_variable(__prepend_prefix(prefix,
                                                      'GITLAB_TOKEN_FILE'))


def __select_source(token, token_file):
    """
    Given two possibilities, give prevalence if the string with the token is
    set but alternatively look for the indicated file whose content is the
    token.
    :param token: str | None
    :param token_file: str | None
    :return: str | None
    """
    _tokens_present = [token, token_file].count(None)
    if _tokens_present == 1:
        if isinstance(token, str) and len(token) > 0:
            return token
        if isinstance(token_file, str) and len(token_file) > 0:
            if os_path_exists(token_file):
                with open(os_path_expanduser(token_file), 'r') as f:
                    return f.read().strip()
    elif _tokens_present == 0:
        raise AttributeError(f"Multiple tokens provided, "
                             f"choose only one way to pass this information")


def _get_token(prefix=None):
    """
    Follow the hierarchy to find the token to be used.
    The one with prevalence is the value given in the environment variable
    [{prefix}_]GITLAB_TOKEN, if not defined then there is an indirect way using
    the environment variable [{prefix}_]GITLAB_TOKEN_FILE that contents the
    file name where this value can be found.

    :param token: str | None
    :param token_file: str | None
    :param prefix: str | None
    :return: str | None
    """
    _envvar_token = _get_token_envvar(prefix)
    _envvar_token_file = _get_token_file_envvar(prefix)
    token = __select_source(_envvar_token, _envvar_token_file)
    if token is not None:
        return token


def gitlab_token():
    return _get_token()


# -- origin environment variables

def origin_gitlab_url():
    return _get_environment_variable('ORIGIN_GITLAB_URL') \
           or default_gitlab_url()


def origin_gitlab_project_path():
    return _get_environment_variable('ORIGIN_GITLAB_PROJECT_PATH') \
           or default_project_path()


def origin_gitlab_token():
    return _get_token(prefix='ORIGIN') \
           or _get_token()


def origin_branch_name():
    return _get_environment_variable('ORIGIN_BRANCH_NAME')


def origin_workbranch_prefix():
    """
    Concatenate this prefix with the name of the revision project, the date
    and a counter:
    "update_{{revision}}_`date +%Y%m%d`_`ctr`
    :return:
    """
    return _get_environment_variable('ORIGIN_WORKBRANCH_PREFIX') or "update"


def origin_gitlab_fork_project_path():
    return _get_environment_variable('ORIGIN_GITLAB_FORK_PROJECT_PATH') \
           or origin_gitlab_project_path()


def origin_gitlab_main_project_path():
    return _get_environment_variable('ORIGIN_GITLAB_MAIN_PROJECT_PATH') \
           or origin_gitlab_project_path()


def mergerequest_title():
    return _replace_name_patterns(
        _get_environment_variable('MERGEREQUEST_TITLE')
        or DEFAULT_MERGEREQUEST_TITLE)


def issues_title():
    return _replace_name_patterns(
        _get_environment_variable('ISSUES_TITLE') or DEFAULT_ISSUES_TITLE)


def ci_issues_title():
    return _replace_name_patterns(
        _get_environment_variable('CI_ISSUES_TITLE') or DEFAULT_CI_ISSUES_TITLE)


def testfail_issues_title():
    return _replace_name_patterns(
        _get_environment_variable('TESTFAIL_ISSUES_TITLE') or issues_title())


def unknown_issues_title():
    return _replace_name_patterns(
        _get_environment_variable('UNKNOWN_ISSUES_TITLE') or
        DEFAULT_UNKNOWN_ISSUES_TITLE)


# -- uprev environment variables


def uprev_gitlab_url():
    return _get_environment_variable('CI_SERVER_URL') \
           or default_gitlab_url()


def uprev_project_path(default=None):
    return _get_environment_variable('CI_PROJECT_PATH') or default or \
           DEFAULT_UPREV_PROJECT_PATH


def uprev_gitlab_token():
    return _get_token(prefix='UPREV') \
           or _get_token()


def uprev_merge_weekday():
    return _get_environment_variable('MERGE_WEEKDAY') or "all"


def uprev_pipeline_source():
    return _get_environment_variable('CI_PIPELINE_SOURCE')


def uprev_pipeline_is_schedule():
    return uprev_pipeline_source() == "schedule"


# -- revision environment variables

def revision_gitlab_url():
    return _get_environment_variable('REVISION_GITLAB_URL') \
           or default_gitlab_url()


def revision_gitlab_project_path():
    return _get_environment_variable('REVISION_GITLAB_PROJECT_PATH')


def revision_fix_pipeline_id():
    return _get_environment_variable('REVISION_FIX_PIPELINE_ID')


def revision_initial_pipeline_search():
    return _get_environment_variable('REVISION_INITIAL_PIPELINE_SEARCH')


# -- CI environment variables

def get_job_id():
    return _get_environment_variable('CI_JOB_ID')


def get_pipeline_id():
    return _get_environment_variable('CI_PIPELINE_ID')


def get_project_id():
    return _get_environment_variable('CI_PROJECT_ID')


# -- jinja-like naming replacement


def _replace_name_patterns(text):
    for _pattern, _getter in [('{revision}', revision_gitlab_project_path),
                              ('{origin}', origin_gitlab_project_path)]:
        if text.count(_pattern) > 0:
            _pattern_project = _getter()
            if _pattern_project is not None:
                if _pattern_project.count('/') > 0:
                    _fragment = _pattern_project.split('/')[1]
                else:
                    _fragment = _pattern_project
                text = text.replace(_pattern, _fragment)
    return text
