#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from .defaults import DEFAULT_LOG_FORMAT
import logging


class Logger:
    _logger = None
    __debug = None
    __trace = None

    def __init__(self, debug=False, trace=False):
        super(Logger, self).__init__()
        self._logger = logging.getLogger(__package__)
        for channel in self._logger.handlers:
            if isinstance(channel, logging.StreamHandler):
                break
        else:  # only add the stream handler if it hasn't yet
            channel = logging.StreamHandler()
            formatter = logging.Formatter(DEFAULT_LOG_FORMAT)
            channel.setFormatter(formatter)
            self._logger.addHandler(channel)
        self.__debug = debug
        self.__trace = trace
        if self.__debug:
            self._logger.setLevel(logging.DEBUG)
            channel.setLevel(logging.DEBUG)
        else:
            self._logger.setLevel(logging.INFO)
            channel.setLevel(logging.INFO)

    def is_in_debug(self):
        return self.__debug

    def is_in_trace(self):
        return self.__trace

    def debug(self, msg):
        self._logger.debug(msg, extra={'obj': str(self)})

    def info(self, msg):
        self._logger.info(msg, extra={'obj': str(self)})

    def warning(self, msg):
        self._logger.warning(msg, extra={'obj': str(self)})

    def error(self, msg):
        self._logger.error(msg, extra={'obj': str(self)})


class Singleton:
    _logger = None
    _debug = None
    _instances = None

    def __new__(cls, debug=False):
        if cls._logger is None:
            cls._logger = logging.getLogger(__package__)
        for channel in cls._logger.handlers:
            if isinstance(channel, logging.StreamHandler):
                break
        # yes, this is an else of the 'for' loop. In case it didn't 'break'
        else:  # only add the stream handler if it hasn't yet
            channel = logging.StreamHandler()
            formatter = logging.Formatter(DEFAULT_LOG_FORMAT)
            channel.setFormatter(formatter)
            cls._logger.addHandler(channel)
        cls._debug = debug
        if cls._debug:
            cls._logger.setLevel(logging.DEBUG)
            channel.setLevel(logging.DEBUG)
        else:
            cls._logger.setLevel(logging.INFO)
            channel.setLevel(logging.INFO)
        if cls._instances is None:
            cls._instances = {}

    def is_in_debug(self):
        return self._debug

    def debug(self, msg):
        self._logger.debug(msg, extra={'obj': str(self)})

    def info(self, msg):
        self._logger.info(msg, extra={'obj': str(self)})

    def warning(self, msg):
        self._logger.warning(msg, extra={'obj': str(self)})

    def error(self, msg):
        self._logger.error(msg, extra={'obj': str(self)})
