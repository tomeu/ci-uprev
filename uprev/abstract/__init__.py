#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"


from .defaults import (bold, italic, underline,
                       blue, green, brown, purple, red, orange, yellow, end,
                       DEFAULT_LOG_FORMAT)
from .envvars import (default_gitlab_url, default_project_path, gitlab_token)
from .envvars import (origin_gitlab_url, origin_gitlab_project_path,
                      origin_gitlab_token, origin_branch_name,
                      origin_workbranch_prefix, origin_gitlab_fork_project_path,
                      origin_gitlab_main_project_path)
from .envvars import mergerequest_title
from .envvars import (ci_issues_title, testfail_issues_title,
                      unknown_issues_title)
from .envvars import (uprev_gitlab_url, uprev_project_path, uprev_gitlab_token,
                      uprev_merge_weekday, uprev_pipeline_source,
                      uprev_pipeline_is_schedule)
from .envvars import (revision_gitlab_url, revision_gitlab_project_path,
                      revision_fix_pipeline_id,
                      revision_initial_pipeline_search)
from .envvars import (get_job_id, get_pipeline_id, get_project_id)
from .logger import Logger, Singleton
