#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"


# https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797
bold = '\033[1m'
italic = '\033[3m'
underline = '\033[4m'
blue = '\033[38;5;27m'
green = '\033[38;5;46m'
brown = '\033[38;5;130m'
purple = '\033[38;5;129m'
red = '\033[38;5;160m'
orange = '\033[38;5;166m'
yellow = '\033[38;5;190m'
end = '\033[0m'

DEFAULT_LOG_FORMAT = f'%(levelname)s - {bold}%(obj)s{end} - %(message)s'

DEFAULT_GITLAB_URL = "https://gitlab.freedesktop.org/"
DEFAULT_UPREV_PROJECT_PATH = "sergi/ci-uprev"

DEFAULT_MERGEREQUEST_TITLE = 'ci: Uprev {revision} to the latest version'
DEFAULT_ISSUES_TITLE = 'ci: Cannot uprev {revision} to the latest version'
DEFAULT_CI_ISSUES_TITLE = 'ci: Cannot uprev {revision} to the latest version ' \
                          'of {origin}'
DEFAULT_UNKNOWN_ISSUES_TITLE = 'ci: Cannot uprev {revision} to the latest ' \
                               'version of {origin}'
