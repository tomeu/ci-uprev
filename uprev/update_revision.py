#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"


from argparse import ArgumentParser, RawTextHelpFormatter
from .abstract import origin_gitlab_token
from .updaterevision import RevisionChecker
from traceback import print_exc


def _build_envvar_help_message(dct, variable_name):
    if variable_name in dct.keys():
        help_text = dct[variable_name]
        if help_text.count('%%'):
            help_text = help_text.replace('%%', '%')
        return f"{variable_name}: {help_text}"


def cli_arguments():
    parser = ArgumentParser(
        description="CLI tool to update the mesa used in the "
                    "continuous integration",
        formatter_class=RawTextHelpFormatter)
    parser.add_argument(
        '-d', '--debug', action='store_true',
        help="debug flag to have execution traces.")
    parser.add_argument(
        '--trace', action='store_true',
        help="trace flag to have a more verbose output")

    # Enviroment variables by blocks
    _epilog_header = "All the parameters can be set up with environment " \
                     "variables, but there are even more variables to fine " \
                     "tune this tool. The parameters prevail above the " \
                     "environment variables."
    _origin_url = {
        'ORIGIN_GITLAB_URL': "Specific gitlab url above GITLAB_URL.",
        'GITLAB_URL': "Url address where the projects are."}
    _origin_project = {
        'ORIGIN_GITLAB_FORK_PROJECT_PATH':
            "Specific gitlab project, above ORIGIN_GITLAB_PROJECT_PATH, "
            "where the working branch is made and the CI issues created",
        'ORIGIN_GITLAB_MAIN_PROJECT_PATH':
            "Specific gitlab project, above ORIGIN_GITLAB_PROJECT_PATH, to "
            "define where the merge requests and test issues would be made",
        'ORIGIN_GITLAB_PROJECT_PATH':
            "Specific gitlab project above GITLAB_PROJECT_PATH.",
        'GITLAB_PROJECT_PATH':
            "Path within the gitlab to the project."}
    _origin_branch_name = {
        'ORIGIN_BRANCH_NAME':
            "Specify the branch from where the to work on the merge request "
            "will start (by default is 'master|main')."}
    _revision_url = {
        'REVISION_GITLAB_URL':
            "Specific gitlab url above GITLAB_URL.",
        'GITLAB_URL': None}
    _revision_project = {
        'REVISION_GITLAB_PROJECT_PATH': "Project path in the revision url."}
    _token_text = "Gitlab token to access a project."
    _token = {
        'GITLAB_TOKEN': "Gitlab token to access a project."}
    _token_file = {
        'GITLAB_TOKEN_FILE':
            "Indirect way to store the token in a file and only command where "
            "the tool can find it and read it."}
    _origin_workbranch_prefix = {
        'ORIGIN_WORKBRANCH_PREFIX':
            "Prefix of the work branch where the preparation of the merge "
            "request is made. By default this prefix is 'update' and the next "
            "fields will be appended: 'update-{revision}-%%Y%%m%%d-NNN."}
    _revision_fix_pipeline_id = {
        'REVISION_FIX_PIPELINE_ID':
            "Instead of look for a good mesa candidate, specify the pipeline "
            "to use."}
    _revision_initial_pipeline_search = {
        'REVISION_INITIAL_PIPELINE_SEARCH':
            "When look for pipeline candidates, discard newer than the "
            "specified"}
    _mergerequest_title = {
        'MERGEREQUEST_TITLE':
            ""}

    parser.epilog = f"""
{_epilog_header}

Environment variables summary:
* Generic:
\t{_build_envvar_help_message(_origin_url, 'GITLAB_URL')}
\t{_build_envvar_help_message(_origin_project, 'GITLAB_PROJECT_PATH')}
\t{_build_envvar_help_message(_token, 'GITLAB_TOKEN')}
\t{_build_envvar_help_message(_token_file, 'GITLAB_TOKEN_FILE')}
* Origin:
\t{_build_envvar_help_message(_origin_url, 'ORIGIN_GITLAB_URL')}
\t{_build_envvar_help_message(_origin_project, 'ORIGIN_GITLAB_PROJECT_PATH')}
\tORIGIN_GITLAB_TOKEN[_FILE]: Specific token (value or file with the value) 
\t                            above GITLAB_TOKEN[_FILE]
\t{_build_envvar_help_message(_origin_branch_name, 'ORIGIN_BRANCH_NAME')}
\t{_build_envvar_help_message(_origin_workbranch_prefix, 'ORIGIN_WORKBRANCH_PREFIX')}

\t{_build_envvar_help_message(_origin_project, 'ORIGIN_GITLAB_FORK_PROJECT_PATH')}
\t{_build_envvar_help_message(_origin_project, 'ORIGIN_GITLAB_MAIN_PROJECT_PATH')}

\tMERGEREQUEST_TITLE: (...)
\tISSUE_TITLE: (...)
\tCI_ISSUE_TITLE: (...)
\tTESTFAIL_ISSUE_TITLE: (...)
\tUNKNOWN_ISSUE_TITLE: (...)
* ci-uprev
\tMERGE_WEEKDAY: (...)
* revision
\t{_build_envvar_help_message(_revision_url, 'REVISION_GITLAB_URL')}
\t{_build_envvar_help_message(_revision_project, 'REVISION_GITLAB_PROJECT_PATH')}

\t{_build_envvar_help_message(_revision_fix_pipeline_id, 'REVISION_FIX_PIPELINE_ID')}
\t{_build_envvar_help_message(_revision_initial_pipeline_search, 'REVISION_INITIAL_PIPELINE_SEARCH')}
        """
    return parser.parse_args()


def manage_exit(exit_code, exit_message):
    print(f"Execution failed and exited with error {exit_code}.\n"
          f"\n{exit_message}\n")
    exit(exit_code)


def main():
    args = cli_arguments()
    try:
        RevisionChecker(debug=args.debug, trace=args.trace)
    except AttributeError as exception:
        if args.debug:
            manage_exit(-2, f"{exception}\n{print_exc()}")
        else:
            manage_exit(-2, f"{exception}")
    except KeyboardInterrupt:
        manage_exit(-3, f"User cancel the execution")
    except Exception as exception:
        manage_exit(1, f"Unexpected exception to be debug"
                       f"\n{exception}\n{print_exc()}")


if __name__ == '__main__':
    main()
