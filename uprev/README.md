# Tools for the update revision on Continuous Integration

Continuous integration goal could be to reduce the load on the developers and
allow them to delegate boring tasks to something automatic that reports about
what happen. So this directory has been thought to contain some tools in this
direction.

1. [Collate results](./collateresults/README.md): This tool is to navigate
   in the CI (a commit, a MR or a pipeline) and collect interesting results
   for the developers to simplify the task.
2. [Update revision](./updaterevision/README.md): This tools is to propose
   a newer commit to point of mesa for the virglrenderer CI.

Both combined simplifies the review of a developer about the status of 
virglrenderer with the currently latest mesa.

