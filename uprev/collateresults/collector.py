#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from datetime import datetime
from .defaults import (DEFAULT_URL, DEFAULT_PROJECT_NAME,
                       blue, purple, yellow, end,
                       DEFAULT_INPUT_CSV_FILES)
from gitlab import Gitlab, GitlabAuthenticationError, GitlabGetError
from ..abstract import Logger
from .output import CSV as output_CSV
from .output import Patch as output_Patch
from .output import Json as output_Json
from .pipelines import Pipeline
from re import compile as re_compile
from re import error as re_error
from re import findall as re_findall
from re import match as re_match
from validators import url as validator_url
from validators.hashes import sha1 as validator_sha1


class ResultsCollector(Logger):
    __url = None
    __project = None
    __jobs_regex = None
    __commit = None
    __merge_request = None
    __pipelines = None
    __token = None
    __gitlab_obj = None
    __project_obj = None
    __csv = None
    __patch = None
    __json = None

    def __init__(self, url=DEFAULT_URL, project=DEFAULT_PROJECT_NAME,
                 jobs_regex=None, commit=None, merge_request=None,
                 pipeline=None, artifacts_set=None, token=None,
                 csv=None, patch=None, json=None, *args, **kwargs):
        super(ResultsCollector, self).__init__(*args, **kwargs)
        t0 = datetime.now()
        self.__input_sanitization(
            url, project, jobs_regex, commit, merge_request, pipeline,
            artifacts_set, csv, patch, json)
        self.__bind_to_gitlab(token)
        self.__repository_exploration(artifacts_set)
        self.debug(f"Process completed in {purple}{datetime.now() - t0}{end} "
                   f"seconds")

    def __del__(self):
        if self.__gitlab_obj is not None:
            self.debug(f"close the Gitlab object")
            self.__gitlab_obj.session.close()

    def __str__(self):
        if self.__project is not None:
            return f"{self.__project}"
        return f"ResultsCollector"

    def __repr__(self):
        return f"ResultsCollector({self.__url}{self.__project}, " \
               f"{self.__jobs_regex}, " \
               f"{self.commit or self.merge_request or self.pipelines})"

    @property
    def commit(self):
        return self.__commit

    @property
    def merge_request(self):
        if self.__merge_request is not None:
            return f"!{self.__merge_request}"

    @property
    def pipelines(self):
        if self.__pipelines is not None or \
                (isinstance(self.__pipelines, list) and
                 len(self.__pipelines) == 0):
            return ''.join([f"#{pipeline}" for pipeline in self.__pipelines])

    # -- first descendant level

    def __input_sanitization(self,
                             url, project, jobs_regex,
                             commit, merge_request, pipeline, artifacts_set,
                             csv, patch, json):
        self.debug(
            f"input received:\n"
            f"- url: {url}\n"
            f"- project: {project}\n"
            f"- jobs regular expression filter: {jobs_regex}\n"
            f"- commit: {commit}\n"
            f"- merge request: {merge_request}\n"
            f"- pipeline: {pipeline}\n"
            f"- artifacts_set: {artifacts_set}\n"
            f"- csv: {csv}\n"
            f"- patch: {patch}\n"
            f"- json: {json}"
        )
        self.__url_validation(url, project)
        self.__regexp_validation(jobs_regex)
        self.__repository_string_validation(commit, merge_request, pipeline)
        self.__artifacts_set_validation(artifacts_set)
        self.__output_options_validation(csv, patch, json)
        self.debug(f"sanitized {self!r}")

    def __bind_to_gitlab(self, token):
        try:
            self.__gitlab_obj = Gitlab(url=self.__url, private_token=token)
            self.info(f"build the gitlab object "
                      f"{blue}{self.__gitlab_obj.url}{end}")
            self.__project_obj = self.__gitlab_obj.projects.get(self.__project)
            self.info(f"build the project object "
                      f"{blue}{self.__project_obj.web_url}{end}")
        except GitlabAuthenticationError:  # bad token
            raise AttributeError("Incorrect token, "
                                 "review '--token' or '--token-file' argument")
        except GitlabGetError:  # bad project name
            raise AttributeError("Project not found, "
                                 "review '--name' argument")
        except ConnectionError:  # bad url
            raise AttributeError("Connection error, "
                                 "review the '--url' argument")
        self.__token = token

    def __repository_exploration(self, artifacts_set):
        if self.__commit is not None:
            self.__explore_the_commit()
        if self.__merge_request is not None:
            self.__explore_the_merge_request()
        if self.__pipelines is not None:
            self.__explore_the_pipelines(artifacts_set)

    def build_output(self, csv=None, patch=None, json=None, collect_objs=False):
        # FIXME: generalize this artifact explicit name
        t0 = datetime.now()
        output_objs = []
        for method, vble in ([self.__build_output_csv, csv],
                             [self.__build_output_patch, patch],
                             [self.__build_output_json, json]):
            output_obj = method(vble, collect_objs)
            if output_obj is not None:
                output_objs.append(output_obj)
        self.debug(f"output production took {purple}{datetime.now()-t0}{end} "
                   f"seconds")
        # FIXME: artifacts can expire
        if collect_objs:
            return output_objs

    # -- second descendant level

    ####################
    # input sanitization

    def __url_validation(self, url, project):
        project_full_name = f"{url}{project}"
        if not validator_url(project_full_name):
            raise AttributeError(
                f"Invalid project url \"{project_full_name}\". "
                f"Review \"--url {url}\" and \"--name {project}\" arguments")
        self.debug(f"validated the address {blue}{project_full_name}{end}")
        self.__url = url
        self.__project = project

    def __regexp_validation(self, jobs_regex):
        try:
            re_compile(jobs_regex)
        except re_error:
            raise AttributeError(
                f"Invalid jobs regular expression. Review \"--jobs\" argument")
        self.__jobs_regex = jobs_regex

    def __repository_string_validation(self,
                                       commit, merge_request, pipeline):
        # this works with one of the 3 arguments, but only one
        if [commit, merge_request, pipeline].count(None) != 2:
            raise AttributeError(
                f"This tool needs to specify one, and only one, of the 3 "
                f"arguments: '--commit', '--merge-request' or '--pipeline'")
        if commit is not None:
            self.__commit_string_validation(commit)
        if merge_request is not None:
            self.__merge_request_string_validation(merge_request)
        if pipeline is not None:
            self.__pipeline_string_validation(pipeline)

    def __artifacts_set_validation(self, artifacts_set):
        if artifacts_set is None:
            return
        try:
            artifacts_set = set(artifacts_set)
        except Exception as exception:
            raise AttributeError(f"Cannot interpret {artifacts_set} as a set")
        if not artifacts_set.issubset(set(DEFAULT_INPUT_CSV_FILES)):
            raise AttributeError(f"The artifacts set specified {artifacts_set}"
                                 f"is not a subset of "
                                 f"{DEFAULT_INPUT_CSV_FILES}")

    def __output_options_validation(self, csv, patch, json):
        if isinstance(csv, bool):
            self.__csv = csv
        else:
            self.__csv = False
        if isinstance(patch, bool):
            self.__patch = patch
        else:
            self.__patch = False
        if isinstance(json, bool):
            self.__json = json
        else:
            self.__json = False
        # if not any([self.__csv, self.__patch, self.__json]):
        #     self.__csv = True

    # input sanitization done
    #########################

    ########################
    # repository exploration

    def __explore_the_commit(self):
        try:
            commit_obj = self.__project_obj.commits.get(self.__commit)
        except GitlabGetError:
            raise AttributeError("Commit not found in the repository. "
                                 "Review the parameter '--commit' "
                                 "or the project name with '--name'")
        if not commit_obj.last_pipeline:
            raise AttributeError("Commit doesn't have a pipeline")
        self.info(f"exploring the commit {yellow}{commit_obj.id}{end}. It has "
                  f"one pipeline with id {commit_obj.last_pipeline['id']}")
        self.__pipelines = [commit_obj.last_pipeline['id']]

    def __explore_the_merge_request(self):
        try:
            merge_request = self.__project_obj.mergerequests.get(
                self.__merge_request)
        except GitlabGetError:
            raise AttributeError("Merge request not found in the repository. "
                                 "Review the parameter '--merge-request' "
                                 "or the project name with '--name'")
        if len(merge_request.pipelines.list()) == 0:
            raise AttributeError(f"Merge request {merge_request.id} doesn't "
                                 f"have pipelines associated")
        self.info(f"exploring the merge request {yellow}{merge_request.id}{end}"
                  f". It has one or more pipelines")
        self.__pipelines = [pipeline.id
                            for pipeline in merge_request.pipelines.list()]

    def __explore_the_pipelines(self, artifacts_set):
        _pipelines_objs = []
        t0 = datetime.now()
        if isinstance(self.__pipelines, list):
            if len(self.__pipelines) > 1:
                self.info(f"some pipelines to explore: {self.__pipelines}")
            for pipeline_id in self.__pipelines:
                try:
                    self.info(f"exploring the pipeline "
                              f"{yellow}{pipeline_id}{end}")
                    _pipelines_objs.append(Pipeline(
                        self.__url, self.__project_obj, self.__token,
                        self.__project_obj.pipelines.get(pipeline_id),
                        self.__jobs_regex, artifacts_set=artifacts_set,
                        debug=self.is_in_debug()))
                except GitlabGetError as error:
                    if len(self.__pipelines) == 1:
                        raise AttributeError(
                            "Pipeline not found in the repository. "
                            "Review the parameter '--pipeline' "
                            "or the project name with '--name'")
                    else:
                        self.error(f"Couldn't work with the pipeline "
                                   f"{yellow}{pipeline_id}{end}: {error}")
        else:
            raise TypeError(f"Ops! pipelines must contain a list of "
                            f"integers. This must not happen")
        self.debug(f"information take took {purple}{datetime.now()-t0}{end} "
                   f"seconds")

    # repository exploration done
    #############################

    ##############
    # output build

    def __build_output_csv(self, enable=None, collect_objs=False):
        if isinstance(enable, bool):
            self.__csv = enable
        if self.__csv:
            obj = output_CSV(debug=self.is_in_debug())
            if collect_objs:
                return obj

    def __build_output_patch(self, enable=None, collect_objs=False):
        if isinstance(enable, bool):
            self.__patch = enable
        if self.__patch:
            # TODO: developers may like to patch from a different branch
            # TODO: developers may like to patch a different project
            #  But if its the case, the assumed internal file structure in
            #  the Patch class has to be adjusted.
            obj = output_Patch(project_obj=self.__project_obj,
                               branch_name=self.__project_obj.default_branch,
                               debug=self.is_in_debug())
            if collect_objs:
                return obj

    def __build_output_json(self, enable=None, collect_objs=False):
        if isinstance(enable, bool):
            self.__json = enable
        if self.__json:
            obj = output_Json(debug=self.is_in_debug())
            if collect_objs:
                return obj

    # output build done
    ###################

    # -- third descendant level

    ##############################
    # repository string validation

    def __commit_string_validation(self, commit):
        # if the length is 8, it may be a shorten of the commit hash
        # or it may be the complete sha1 hash.
        if not (self.__is_short_commit_id(commit) or
                self.__is_hash_commit_id(commit)):
            raise AttributeError(
                "Cannot understand the commit id as such. Review the "
                "'--commit' parameter")
        self.debug(f"validated the commit string {yellow}{commit!r}{end}")
        self.__commit = commit

    def __merge_request_string_validation(self, merge_request):
        match = re_match(r"^[!]?(\d+)$", merge_request)
        if match is None:
            raise AttributeError(
                "Cannot understand the merge request id as such. Review the "
                "'--merge-request' parameter")
        self.debug(f"validated the merge request string "
                   f"{yellow}{merge_request!r}{end}")
        self.__merge_request = int(match.group(1))

    def __pipeline_string_validation(self, pipeline):
        match = re_match(r"^[#]?(\d+)$", pipeline)
        if match is None:
            raise AttributeError("Cannot understand the pipeline id as "
                                 "such. Review the '--pipeline' parameter")
        self.debug(f"validated the pipeline string {yellow}{pipeline!r}{end}")
        self.__pipelines = [int(match.group(1))]

    # repository string validation done
    ###################################

    # -- fourth descendant level

    ##########################
    # commit string validation

    @staticmethod
    def __is_short_commit_id(commit):
        return len(commit) == 8 and len(re_findall(r"([a-fA-F\d]{8})", commit))

    @staticmethod
    def __is_hash_commit_id(commit):
        return validator_sha1(commit) is True

    # commit string validation done
    ###############################
