# collate_results

[[_TOC_]]

From a clone of the repository (or a branch checkout) this can be called by:

```bash
ci-collate_results --pipeline \#597118 --csv --json --patch
```

(Note: this has been developed for python 3.10 and 3.9, see the 
[Python environment](README.md#python-environment) section for further 
information)

If necessary, one can use a (read-only) token. This can be passed in the 
command line with the argument `--token TOKEN` or take it from a file with
`--token-file TOKEN_FILE`. There is also a third alternative that is use the
environement variable `GITLAB_TOKEN`.

This tool will provide some traces in the console, but if someone likes to read
more about the things happening, use the argument `-d` or `--debug`.


## output

Three types of outputs can be generated.

1. **CSV**: This simply means a merge of the `(failures|results).csv` files per 
pipeline with a comment line with the name of the job to separate the blocks. 
Important: This produces a file per pipeline with the pattern name 
`pipeline_#N..N_(failures|results).csv` in the working directory. 
 
2. **Json**: This is a report based on collect the results for each of the test
per pipeline. The json file has as keys the test names and as value the list
of saw results: `{ $test_name: [$test_status,...] }`. The information about 
which job has end with a given status is lost. It includes together 
the information from both artifacts `(failures|results).csv`.

3. **Patch**: This proposes a patch for the files in `.gitlab/expectations` 
with the possible changes for the next integrations. It only uses 
`failures.csv` artifacts from the jobs. To apply the patch, use `-p1`.

In all the cases, if the output file already exist, it will be overwritten (it 
shall have the same content anyway).

## extra arguments

### --pipeline, --merge-request, --commit

This has been thought also to not work only with a given pipeline, but also 
with a merge request or a commit. Instead of use the argument 
`--pipeline PIPELINE`, you can use `--merge-request` or `--commit COMMIT`. 
Note that those 3 arguments are mutually exclusive.

At the current stage, the use of this alternative ways to get to pipeline 
executions mean like if one has called the tool individually for each of the
pipelines.

```bash
ci-collate_results --pipeline 597118 --patch
ci-collate_results --merge-request \!818 --patch
ci-collate_results --commit 8402939a --patch
```

### gitlab server and project

This assumes to work in 'virgl/virglrenderer' project, but it can be changed 
with the `--name NAME` argument to your fork (or the use of the environment
variable `GITLAB_PROJECT_PATH`). 

Also the url to 'https://gitlab.freedesktop.org/' can be changed using the 
argument `--url URL` (or the environment variable `GITLAB_URL`).

In case the jobs to explore have a different naming than the one that exist 
at the time this has been written, you can change the regular expression that 
filter them. If the argument `--jobs JOBS` is not used, the regular expression 
is `^(piglit|deqp)-(gles|gl)-(virt|host)`.

It can also work with environment variables like:
* `GITLAB_URL` to specify the `--url` that by default points to 
`https://gitlab.freedesktop.org/`
* `GITLAB_PROJECT_PATH` to specify `--name` that by default points to
`virgl/virglrenderer`
* `GITLAB_TOKEN` to specify what can be passed by `--token` or `--token-file`

So one can call the tool by:

```bash
export GITLAB_PROJECT_PATH='sergi/virglrenderer'
ci-collate_results -d --merge-request \!1 --token-file ../gitlab_sergi_virglrenderer_readonly.token --csv --json --patch
```

## TODOs

- [ ] This tool may not be resistant on a pipeline in execution.
- [ ] The first patch output is for the 
[virglrenderer](https://gitlab.freedesktop.org/virgl/virglrenderer) project, 
but perhaps it can propose changes over the 
[mesa](https://gitlab.freedesktop.org/mesa/mesa) project (files in 
`src/gallium/drivers/virgl/ci/vir{gl,pipe}-gl{,es}-fails.txt`)
- [ ] This tool may also be used in other CIs as it is generic enough.

## Design

### Class diagram

```mermaid
classDiagram
class Logger
class GitlabManager
class Pipeline
class Job
class Artifact
class _ArtifactsSet

class Singleton
class Artifacts

class Output
class CSV
class Json
class PatchProducer

Logger <|-- GitlabManager
Logger <|-- Pipeline
Logger <|-- Job
Logger <|-- Artifact
Logger <|-- _ArtifactsSet

Singleton <|-- Artifacts

GitlabManager --o "1..*" Pipeline
Pipeline --o "1..*" Job
Job --o "1..*" Artifact

GitlabManager --o Artifacts
Job --o Artifacts

Artifacts --o _ArtifactsSet
_ArtifactsSet --o "1..*" Artifact

Logger <|-- Output
Output <|-- CSV
Output <|-- Json
Output <|-- PatchProducer

GitlabManager --o "1..*" Output
```

### Sequence diagram

```mermaid
sequenceDiagram
User->>review_flakes.py: provide (commit|MR|pipeline)
review_flakes.py->>GitlabManager: build (commit|MR|pipeline)
activate GitlabManager
GitlabManager->>GitlabManager: bind_to_gitlab()
GitlabManager->>Pipeline: explore Jobs
activate Pipeline
Pipeline->>Job: explore artifacts
activate Job
Job->>Artifact: build representation
activate Artifact
Job->>Artifacts: store artifact
Artifact-->>Job: done
deactivate Artifact
Job-->>Pipeline: done
deactivate Job
Pipeline-->>GitlabManager: done
deactivate Pipeline
GitlabManager->>Output: report (csv|patch|json)
activate Output
Output->>Artifacts: iterate
activate Artifacts
Artifacts->>Artifact: read the content
activate Artifact
Artifact-->>Output: data
deactivate Artifact
Output->>Output: write output
deactivate Artifacts
Output-->>GitlabManager: done
deactivate Output
GitlabManager-->>review_flakes.py: done
deactivate GitlabManager

```

## Python environment

Help of the python setup to use this tool is the same than what is described 
for all those tools here. Check 
[Python environment](../../python.md#python-environment) section of the tools 
readme.
