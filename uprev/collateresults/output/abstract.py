#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from ..artifacts import Artifacts
from ..defaults import DEFAULT_INPUT_CSV_FILES
from ...abstract import Logger


class Output(Logger):
    __artifact_names = None
    __artifact_objs = None

    def __init__(self, artifacts=DEFAULT_INPUT_CSV_FILES, *args, **kwargs):
        super(Output, self).__init__(*args, **kwargs)
        if isinstance(artifacts, list):
            if len(artifacts) == 0 or \
                    not set(artifacts).issubset(set(DEFAULT_INPUT_CSV_FILES)):
                raise ValueError(f"Unsupported set '{artifacts}' "
                                 f"to produce on output")
            self.__artifact_names = artifacts

    @property
    def artifact_names(self):
        return self.__artifact_names

    def artifact_obj(self, name):
        return Artifacts(name.rsplit('.', 1)[0], self.is_in_debug())
