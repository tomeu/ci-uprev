#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from .abstract import Output
from ..defaults import brown, purple, yellow, end
from os.path import getsize as os_path_getsize
from pint import Quantity, UnitRegistry


class CSV(Output):
    """
    For each of the given artifacts to process, this class produces a single
    file with a merge of the one produced by each job.
    """
    def __init__(self, *args, **kwargs):
        super(CSV, self).__init__(*args, **kwargs)
        self.debug("Ready to iterate the artifacts")
        self.__iterate_available_artifacts()

    def __str__(self):
        return "output csv"

    def __iterate_available_artifacts(self):
        for _artifact_name in self.artifact_names:
            self.debug(f"working with the {yellow}{_artifact_name}{end} "
                       f"artifact")
            _artifacts_set = \
                self.artifact_obj(_artifact_name.rsplit('.', 1)[0])
            for _pipeline_id, _jobs in _artifacts_set.jobs_by_pipeline.items():
                self.__write_pipeline_summary_for_artifact(
                    int(_pipeline_id), _artifacts_set.name, _jobs)

    def __write_pipeline_summary_for_artifact(self, pipeline_id,
                                              artifact_name, jobs):
        _file_name = f"pipeline_{pipeline_id}_{artifact_name}.csv"
        with open(_file_name, 'w') as _file_descriptor:
            for _artifact in jobs.values():
                self.__merge_job_artifact(_file_descriptor, _artifact)
        file_size = Quantity(os_path_getsize(_file_name),
                             UnitRegistry().byte).to_compact()
        self.info(f"generated a {purple}{file_size:.3f}{end} file with "
                  f"'{brown}./{_file_name}{end}' name")

    @staticmethod
    def __merge_job_artifact(file_descriptor, artifact):
        file_descriptor.write(f"#{artifact.job_name}\n")
        for _line in artifact.content:
            file_descriptor.write(f"{_line}\n")
