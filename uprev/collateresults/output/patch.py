#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from .abstract import Output
from ..defaults import bold, blue, brown, red, purple, yellow, end
from ..defaults import (DEFAULT_EXPECTATIONS_DIRECTORY, DEFAULT_SUBDIRECTORIES,
                        DEFAULT_FILES, DEFAULT_EXTENSION,
                        DEFAULT_PATCH_FILE_NAME)
from gitlab.exceptions import GitlabCreateError
from os import getcwd as os_getcwd
from os import makedirs as os_makedirs
from os import system as os_system
from os import walk as os_walk
from os.path import getsize as os_path_getsize
from pint import Quantity, UnitRegistry
from shutil import copytree as shutil_copytree
from shutil import rmtree as shutil_rmtree
from tempfile import mkdtemp as tempfile_mkdtemp


class Patch(Output):
    """
    This output generates a patch for expectations in the working project. It
    means the files under the pattern:
    `.gitlab-ci/expectations/(host|virt)/virgl-(gl|gles)-(fails|flakes).txt`
    """
    # FIXME: This is not generic to be used in other projects, neither change a
    #  bit the behaviour with in the same.
    # TODO: allow to set up the expectations directory
    # TODO: allow to set up the content tree inside the expectations directory
    #  subdirectories and files in there.
    # TODO: allow the user to say the branch to patch
    # TODO: not only use the failures.csv information but also the results.csv
    # TODO: manage what to do with other test status
    # FIXME: code review, many parts aren't self explainatory and would be
    #  difficult to maintain in the future.
    __project_obj = None
    __branch_name = None
    __temporary_storage = None
    __patched = None

    def __init__(self, project_obj, branch_name, *args, **kwargs):
        super(Patch, self).__init__(*args, **kwargs)
        self.__project_obj = project_obj
        self.__branch_name = branch_name
        self.__temporary_storage = tempfile_mkdtemp()
        self.debug(f"created {brown}{self.__temporary_storage}{end}")
        if self.__get_files_to_patch():
            self.__process_artifacts()
            self.__generate_patch()

    def __del__(self):
        self.debug(f"remove {brown}{self.__temporary_storage}{end}")
        shutil_rmtree(self.__temporary_storage)

    def __str__(self):
        return "output patch"

    def apply_patch(self, branch_destination_name):
        self.debug(f"apply the changes to the branch "
                   f"{yellow}{branch_destination_name}{end}")
        root_path = f"{self.__temporary_storage}/b/"
        data = {'branch': branch_destination_name,
                'commit_message': "update expectations",
                'actions': []}
        for root, directories, files in os_walk(root_path):
            for file in files:
                full_file_path = f"{root}/{file}"
                relative_file = f"{full_file_path.replace(root_path, '')}"
                self.debug(f"patch {yellow}{relative_file}{end} "
                           f"using {brown}{full_file_path}{end}")
                data['actions'].append(
                    {'action': 'update',
                     'file_path': relative_file,
                     'content': open(full_file_path).read()}
                )
        try:
            commit_obj = self.__project_obj.commits.create(data)
            self.debug(f"created the commit with the patch: "
                       f"{yellow}{commit_obj.id}{end} "
                       f"{blue}{commit_obj.web_url}")
        except GitlabCreateError as exception:
            self.error(f"There has been an error sending the commit: "
                       f"{red}{exception.error_message}{end}")
        except Exception as exception:
            self.error(f"Unexpected error sending the commit:\n{exception}")
            raise exception

    def __get_files_to_patch(self):
        """
        Prepare a temporary directory with the current content in the gitlab
        project, so the suggestions from the artifacts can be applied to later
        generate a patch file.
        :return:
        """
        _directory = DEFAULT_EXPECTATIONS_DIRECTORY
        _subdirs = DEFAULT_SUBDIRECTORIES
        directories_get = []
        for _subdir in _subdirs:
            directories_get.append(self.__get_specific_directory(_directory,
                                                                 _subdir))
        if not all(directories_get):
            self.error(f"Cannot prepare a patch because some files are "
                       f"{bold}not{end} available")
            return False
        shutil_copytree(f"{self.__temporary_storage}/a",
                        f"{self.__temporary_storage}/b")
        return True

    def __process_artifacts(self):
        self.__patched = {}
        for _artifact_name in self.artifact_names:
            if _artifact_name not in ['failures.csv']:
                self.debug(f"Ignoring artifact {yellow}{_artifact_name}{end}")
                continue
            self.debug(f"Working with {yellow}{_artifact_name}{end} artifact")
            _artifacts_set = \
                self.artifact_obj(_artifact_name.rsplit('.', 1)[0])
            # FIXME: this 3 level loop may be optimized
            for _jobs in _artifacts_set.jobs_by_pipeline.values():
                for _artifact in _jobs.values():
                    # self.debug(f"Read the artifact {yellow}{_artifact}{end}")
                    for _line in _artifact.content:
                        _name, _result = self.__parse_line(_line)
                        # self.debug(f"Parsing artifact content: "
                        #            f"{yellow}{_name}{end}, "
                        #            f"{yellow}{_result}{end}")
                        if _result is None:
                            continue
                        hw_type, lib_type = self.__get_job_name_fields(
                            _artifact.job_name)
                        # self.debug(f"Patch {yellow}{_result}{end} for "
                        #            f"{yellow}{_name}{end} in "
                        #            f"{yellow}{hw_type}{end} and "
                        #            f"{yellow}{lib_type}{end}")
                        if hw_type is not None and lib_type is not None:
                            self.__patch(_result, _name, hw_type, lib_type)

    def __generate_patch(self):
        output_file = f"{os_getcwd()}/{DEFAULT_PATCH_FILE_NAME}"
        os_system(f"cd {self.__temporary_storage}; "
                  f"diff -Nru a b > {output_file}")
        file_size = Quantity(os_path_getsize(output_file),
                             UnitRegistry().byte).to_compact()
        self.info(f"generated a {purple}{file_size:.3f}{end} file with "
                  f"'{brown}./{output_file}{end}' name")

    # -- first descendant level

    def __get_specific_directory(self, base, directory):
        """
        Given an specific directory of the gitlab project iterate to get the
        file contents of the desired files.
        :param base: str
        :param directory: str
        :return:
        """
        # FIXME: this is only taking 'virgl-gl{,es}-fails'
        #  so it can only work with failures.csv artifact
        _file_names = DEFAULT_FILES
        _extension = DEFAULT_EXTENSION
        _relative_path = f"{base}/{directory}"
        self.debug(f"for {brown}{directory}{end} create a relative path "
                   f"{brown}{_relative_path}{end} "
                   f"in {brown}{self.__temporary_storage}{end}")
        os_makedirs(f"{self.__temporary_storage}/a/{_relative_path}")
        # self.debug(f"iterate over {_file_names}")
        files_get = []
        for _file_name in _file_names:
            files_get.append(self.__get_specific_file(_relative_path,
                                                      _file_name, _extension))
        if all(files_get):
            return True
        return False

    @staticmethod
    def __parse_line(line):
        if line.count(',') > 0:
            name, result, *_ = line.split(',')
            return name, result
        return None, None

    def __get_job_name_fields(self, job_name):
        return self.__get_hardware_from_job_name(job_name), \
               self.__get_library_field_from_job_name(job_name)

    def __patch(self, test_result, test_name, hardware_tested, library_used):
        if test_result not in ['UnexpectedPass']:
            self.debug(f"* Ignoring {yellow}'{test_result}'{end} results "
                       f"of {test_name} test")
            return
        _file_to_modify = f"{self.__temporary_storage}/" \
                          f"b/{DEFAULT_EXPECTATIONS_DIRECTORY}/" \
                          f"{hardware_tested}/virgl-{library_used}-fails.txt"
        if _file_to_modify not in self.__patched:
            self.__patched[_file_to_modify] = []
        if test_name in self.__patched[_file_to_modify]:
            self.debug(f"{yellow}{test_name}{end} already patched in "
                       f"{brown}{_file_to_modify}{end}")
            return
        self.__remove_test_from_file(test_name, _file_to_modify)
        self.__patched[_file_to_modify].append(test_name)

    # -- second descendant level

    def __get_specific_file(self, relative_path, file_name, extension):
        """
        Get from the files object of the gitlab project the files involved in
        the patching.
        :param relative_path: str
        :param file_name: str
        :param extension: str
        :return:
        """
        _relative_file_name = f"{relative_path}/{file_name}.{extension}"
        self.debug(f"ready to read {brown}{_relative_file_name}{end}")
        _absolute_temp_file_name = \
            f"{self.__temporary_storage}/a/{_relative_file_name}"
        try:
            _bytes_stream = self.__project_obj.files.get(
                _relative_file_name, ref=self.__branch_name).decode()
        except Exception as exception:
            self.error(f"Found an {red}{type(exception)}{end} error: "
                       f"{bold}{exception}{end} "
                       f"for {brown}{_relative_file_name}{end}")
            return False
        with open(_absolute_temp_file_name, 'x') as f:
            f.write(_bytes_stream.decode())
        return True

    @staticmethod
    def __get_hardware_from_job_name(job_name):
        if job_name.lower().count('host'):
            return 'host'
        elif job_name.lower().count('virt'):
            return 'virt'

    @staticmethod
    def __get_library_field_from_job_name(job_name):
        if job_name.lower().count('gles'):
            return 'gles'
        elif job_name.lower().count('gl'):
            return 'gl'
        return None

    def __remove_test_from_file(self, test_name, file_name):
        # FIXME: there would be a more efficient way to remove occurrences
        #  in a file
        self.debug(f"remove {yellow}{test_name}{end} in "
                   f"{brown}{file_name}{end}")
        with open(file_name, 'r') as _origin:
            lines = _origin.readlines()
        with open(file_name, 'w') as _destination:
            for _line in lines:
                if not _line.count(test_name):
                    _destination.write(_line)
