#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from .abstract import Output
from ..defaults import brown, purple, yellow, end
from os.path import getsize as os_path_getsize
from json import dump as json_dump
from pint import Quantity, UnitRegistry


class Json(Output):
    """
    This output produces a summary in a json format with the key-value as:
    { $test_name: [$test_status,...] }
    Where, for each of the tests the value is a list of the results on the jobs
    for each of the pipelines.
    """
    __tests_per_pipeline = None

    def __init__(self, *args, **kwargs):
        super(Json, self).__init__(*args, **kwargs)
        self.__tests_per_pipeline = {}
        self.__iterate_available_artifacts()
        self.__produce_json_files()

    def __str__(self):
        return "output json"

    def __iterate_available_artifacts(self):
        for _artifact_name in self.artifact_names:
            self.debug(f"working with the {yellow}{_artifact_name}{end} artifact")
            _artifacts_set = \
                self.artifact_obj(_artifact_name.rsplit('.', 1)[0])
            for _pipeline_id, _jobs in _artifacts_set.jobs_by_pipeline.items():
                if not int(_pipeline_id) in self.__tests_per_pipeline:
                    self.__tests_per_pipeline[int(_pipeline_id)] = {}
                self.__tests_per_pipeline[int(_pipeline_id)] |= \
                    self.__iterate_jobs_per_pipeline(_jobs)
        # pprint(self.__tests_per_pipeline)

    @staticmethod
    def __iterate_jobs_per_pipeline(jobs) -> dict:
        _tests = {}
        for _artifact in jobs.values():
            for _line in _artifact.content:
                if _line.count(',') > 0:
                    _test_name, _test_result, *_ = _line.split(',')
                    if _test_name not in _tests:
                        _tests[_test_name] = []
                    _tests[_test_name].append(_test_result)
        return _tests

    def __produce_json_files(self):
        for _pipeline_id, _data in self.__tests_per_pipeline.items():
            _file_name = f"pipeline_{_pipeline_id}.json"
            with open(_file_name, 'w') as _file_descriptor:
                json_dump(_data, _file_descriptor)
            file_size = Quantity(os_path_getsize(_file_name),
                                 UnitRegistry().byte).to_compact()
            self.info(f"generated a {purple}{file_size:.3f}{end} file with "
                      f"'{brown}./{_file_name}{end}' name")
