#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from datetime import datetime
from .defaults import red, blue, brown, purple, yellow, end
from ..abstract import Logger, Singleton
from os.path import exists as os_path_exists
from os.path import getsize as os_path_getsize
from pint import Quantity, UnitRegistry
from re import match as re_match
from shutil import copyfileobj as shutil_copyfileobj
from shutil import rmtree as shutil_rmtree
from tempfile import mkdtemp as tempfile_mkdtemp
from urllib.error import HTTPError
from urllib.request import Request, urlopen


class Artifact(Logger):
    __base_url = None
    __artifact_path = None
    __project_obj = None
    __token = None
    __job_obj = None
    __filename = None
    __file_content = None
    __artifact_expiration = None
    __artifact_expired = None
    __temporary_storage = None
    __artifact_exists = None

    def __init__(self, url, project_obj, token, job_obj, artifact_name,
                 *args, **kwargs):
        super(Artifact, self).__init__(*args, **kwargs)
        self.__base_url = \
            f"{url}api/v4/projects/{project_obj.id}/" \
            f"jobs/{job_obj.id}/artifacts/"
        self.__project_obj = project_obj
        self.__token = token
        self.__job_obj = job_obj
        self.__filename = artifact_name
        self.__check_artifacts_expiration(self.__job_obj.artifacts_expire_at)

    def __str__(self):
        return f"{self.full_name}"

    def __repr__(self):
        return f"Artifact({self.name}, {self.job_id}:'{self.job_name}', " \
               f"{self.pipeline_id})"

    @property
    def name(self):
        return f"{self.__filename.rsplit('.', 1)[0]}"

    @property
    def filename(self):
        return f"{self.__filename}"

    @property
    def full_name(self):
        return f"{self.__job_obj.name}:{self.__filename}"

    @property
    def job_id(self):
        return self.__job_obj.id

    @property
    def job_name(self):
        return self.__job_obj.name

    @property
    def job_status(self):
        return self.__job_obj.status

    @property
    def pipeline_id(self):
        return self.__job_obj.pipeline_id

    @property
    def temporary_storage(self):
        return self.__temporary_storage

    @temporary_storage.setter
    def temporary_storage(self, value: str):
        if os_path_exists(value):
            self.__temporary_storage = value
            self.debug(f"set the space to left files to {brown}{value}{end}")
            self.__download_artifact()
        else:
            self.error(f"Refused to use {brown}{value}{end} as a temporary "
                       f"directory")

    @property
    def temporary_filename(self):
        tmp = self.__temporary_storage
        pipeline = self.__job_obj.pipeline_id
        job = self.__job_obj.name.replace(' ', '_').replace('/', '_')
        return f"{tmp}/{pipeline}_{job}_{self.__filename}"

    @property
    def url(self):
        if self.__artifact_path is None:
            raise AttributeError("artifact path not yet defined")
        return f"{self.__base_url}{self.__artifact_path}"

    @property
    def artifact_exists(self):
        return self.__artifact_exists

    def __url_possible_file_paths(self):
        """
        There are two known directory paths, one with the job name when
        other jobs has been saw they left the artifact directly in results.

        This method has been thought to be resistant to changes on that. By
        default, the first will succeed and the jobs that is already known that
        will use the second have a filter to use directly the good one. But
        this method has been thought just in case this changes, and this tool
        stays resistant.
        :return: relative path: str
        """
        yield f"results/{self.__job_obj.name}/{self.__filename}" \
              f"".replace(' ', '%20')
        yield f"results/{self.__filename}".replace(' ', '%20')

    @property
    def content(self):
        if self.__temporary_storage is not None and \
                os_path_exists(self.temporary_filename):
            with open(self.temporary_filename, 'r') as f:
                for _line in f.read().split('\n'):
                    yield _line

    @property
    def content_per_result(self):
        dct = {}
        for element in self.content:
            _field = element.split(',')
            if len(_field) >= 2:
                _test_name, _test_result = _field[0], _field[1]
                if _test_result not in dct:
                    dct[_test_result] = []
                dct[_test_result].append(_test_name)
        return dct

    def __download_artifact(self):
        _result = None
        if re_match(r"^(piglit).*(virt).*(\d.?)/(\d.?)$", self.__job_obj.name):
            # known path for those artifacts so no need to try candidates
            self.__artifact_path = f"results/{self.__filename}"
            _result = self.__try_download_path()
        else:
            for _candidate in self.__url_possible_file_paths():
                self.__artifact_path = _candidate
                _result = self.__try_download_path()
                if self.artifact_exists:
                    break
        if not self.artifact_exists:
            self.warning(f"issue downloading {yellow}{self}{end} "
                         f"artifact: {_result}")
            with open(self.temporary_filename, 'w') as f:
                f.write(f"#{_result} \"{self.url}\"\n")

    def __try_download_path(self):
        _t0 = datetime.now()
        self.info(f"ready to read the artifact {yellow}{self}{end} "
                  f"from {blue}{self.url}{end} "
                  f"to {brown}{self.temporary_filename}{end}")
        result = self.__save_url_content()
        _dt = datetime.now() - _t0
        if self.artifact_exists:
            file_size = Quantity(os_path_getsize(self.temporary_filename),
                                 UnitRegistry().byte).to_compact()
            self.debug(f"received {purple}{file_size:.3f}{end} "
                       f"from {yellow}{self}{end} artifact in "
                       f"{purple}{_dt}{end} seconds")
        else:
            self.warning(f"store the error reading the artifact "
                         f"{yellow}{self}{end} that required "
                         f"{purple}{_dt}{end} seconds")
        return result

    def __save_url_content(self):
        if self.__artifact_expired:
            self.__artifact_exists = False
            return "expired"
        _req = Request(self.url)
        if self.__token is not None:
            _req.add_header('PRIVATE-TOKEN', self.__token)
        try:
            with urlopen(_req) as _answer, \
                    open(self.temporary_filename, 'wb') as _file_descriptor:
                shutil_copyfileobj(_answer, _file_descriptor)
                self.__artifact_exists = True
                return "ack"
        except HTTPError as exception:
            self.error(f"Exception with artifact "
                       f"'{brown}{self.__filename}{end}' of job "
                       f"{yellow}'{self.__job_obj.name}'{end} from pipeline "
                       f"{yellow}'{self.__job_obj.pipeline_id}'{end}: "
                       f"'{blue}{self.url}{end}': '{red}{exception}{end}'")
            self.__artifact_exists = False
            return f"{exception}"

    def __check_artifacts_expiration(self, artifacts_expire_at):
        self.__artifact_expired = False
        if artifacts_expire_at is not None:
            adapt_expiration = f"{artifacts_expire_at.rsplit('.')[0]}+00:00"
            # as it is Zulu time, UTC time zone
            self.__artifact_expiration = datetime.strptime(
                adapt_expiration, r'%Y-%m-%dT%H:%M:%S%z').timestamp()
            if self.__artifact_expiration < datetime.utcnow().timestamp():
                self.info(f"artifacts expired for job "
                          f"{yellow}{self.__job_obj.pipeline_id}{end}:"
                          f"{yellow}{self.__job_obj.name}{end}")
                self.__artifact_expired = True


class _ArtifactsSet(Logger):
    __name = None
    __jobs_by_pipeline = None
    __temporary_storage = None

    def __init__(self, artifact_name, *args, **kwargs):
        # super(Logger, self).__init__(*args, **kwargs)
        Logger.__init__(self, *args, **kwargs)
        self.__name = artifact_name
        self.__jobs_by_pipeline = {}
        self.__temporary_storage = tempfile_mkdtemp()
        self.debug(f"Build a set for {yellow}{self.name}{end} artifacts")

    def __del__(self):
        self.debug(f"remove {brown}{self.__temporary_storage}{end}")
        shutil_rmtree(self.__temporary_storage)

    # TODO: the set may only accept to add an artifact when the pair
    #  (pipeline, job) hasn't add it already

    def __str__(self):
        return f"{self.name}"

    def __repr__(self):
        return f"ArtifactsSet({self.name})"

    @property
    def name(self):
        return self.__name

    @property
    def jobs_by_pipeline(self):
        return self.__jobs_by_pipeline

    # -- section of methods to behalf as set class
    def add(self, artifact: Artifact):
        if artifact.name != self.__name:
            raise NameError(f"This set is for {yellow}{self.name}{end} "
                            f"artifacts, not {artifact.name}")
        if artifact.pipeline_id in self.__jobs_by_pipeline.keys():
            if artifact.job_id in \
                    self.__jobs_by_pipeline[artifact.pipeline_id]:
                self.warning(
                    f"already exist in the set an {yellow}{artifact.name}{end} "
                    f"artifact for job {yellow}{artifact.job_name}{end} in "
                    f"pipeline {yellow}{artifact.pipeline_id}{end}: "
                    f"it will be {red}overwritten{end}")
            self.__jobs_by_pipeline[
                artifact.pipeline_id][artifact.job_id] = artifact
        else:
            self.__jobs_by_pipeline[artifact.pipeline_id] = \
                {artifact.job_id: artifact}
        artifact.temporary_storage = self.__temporary_storage
        # self.debug(f"add {yellow}'{self.name}'{end} artifact from job "
        #            f"{yellow}'{artifact.job_name}'{end}")
    # By now no more methods from 'set' class are needed


class Artifacts(Singleton):
    def __new__(cls, artifact_name: str, *args, **kwargs):
        super(Artifacts, cls).__new__(cls, *args, **kwargs)
        if artifact_name not in cls._instances:
            cls._instances[artifact_name] = \
                _ArtifactsSet(artifact_name, debug=cls._debug)
        return cls._instances[artifact_name]

    @classmethod
    def clean(cls, artifact_name):
        if cls._instances is not None and artifact_name in cls._instances:
            cls._instances.pop(artifact_name)
