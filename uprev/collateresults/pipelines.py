#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from .jobs import Job
from ..abstract import Logger
from re import match as re_match


class Pipeline(Logger):
    __url = None
    __project_obj = None
    __token = None
    __pipeline_obj = None
    __jobs_filter = None
    __jobs = None

    def __init__(self, url, project_obj, token, pipeline_obj, jobs_filter,
                 artifacts_set=None, *args, **kwargs):
        super(Pipeline, self).__init__(*args, **kwargs)
        self.__url = url
        self.__project_obj = project_obj
        self.__token = token
        self.__pipeline_obj = pipeline_obj
        self.__jobs_filter = jobs_filter
        self.__jobs = {}
        for job in self.__pipeline_obj.jobs.list():
            if re_match(self.__jobs_filter, job.name):
                # self.debug(f"In pipeline {self}, exploring job {job.name}")
                self.__jobs[job.name] = Job(
                    self.__url, self.__project_obj, self.__token,
                    job, input_csv_files=artifacts_set,
                    debug=self.is_in_debug())

    def __str__(self):
        return f"#{self.__pipeline_obj.id}"

    def __repr__(self):
        return f"Pipeline(#{self.__pipeline_obj.id}, {self.__jobs_filter})"

    def __int__(self):
        return self.__pipeline_obj.id

    @property
    def name(self):
        return f"{self.__pipeline_obj.id}"

    @property
    def jobs(self):
        return self.__jobs
