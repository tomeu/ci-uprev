#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from .artifacts import Artifact, Artifacts
from .defaults import DEFAULT_INPUT_CSV_FILES, red, yellow, end
from ..abstract import Logger


class Job(Logger):
    __url = None
    __project_obj = None
    __token = None
    __job_obj = None
    __artifacts = None

    def __init__(self, url, project_obj, token, job_obj,
                 input_csv_files=None, *args, **kwargs):
        super(Job, self).__init__(*args, **kwargs)
        self.__url = url
        self.__project_obj = project_obj
        self.__token = token
        self.__job_obj = job_obj
        self.__artifacts = []
        if self.__job_obj.status in ['canceled']:
            self.debug(f"For job {yellow}{self.full_name}{end}: no artifacts "
                       f"when {red}{self.__job_obj.status}{end} status")
            return
        if input_csv_files is None:
            input_csv_files = DEFAULT_INPUT_CSV_FILES
        for artifact_name in input_csv_files:
            # self.debug(f"In job {yellow}{self.full_name}{end} exploring "
            #            f"artifact {yellow}{artifact_name}{end}")
            artifact = Artifact(self.__url, self.__project_obj, self.__token,
                                self.__job_obj, artifact_name,
                                debug=self.is_in_debug())
            artifacts_set = Artifacts(artifact.name, self.is_in_debug())
            self.__artifacts.append(artifact)
            artifacts_set.add(artifact)

    def __str__(self):
        return f"{self.name}"

    def __repr__(self):
        return f"Job({self.name})"

    @property
    def name(self):
        return self.__job_obj.name

    @property
    def full_name(self):
        return f"{self.__job_obj.pipeline_id}:{self.name}"

    @property
    def id(self):
        return self.__job_obj.id

    @property
    def status(self):
        return self.__job_obj.status

    @property
    def pipeline_id(self):
        return self.__job_obj.pipeline_id

    @property
    def artifacts(self):
        return self.__artifacts
