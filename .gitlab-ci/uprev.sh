#!/bin/bash

set -e

echo -e "\e[0Ksection_start:`date +%s`:install[collapsed=true]\r\e[0KInstall the ci-uprev"
python setup.py install
echo -e "\e[0Ksection_end:`date +%s`:install\r\e[0K"

echo -e "\e[0Ksection_start:`date +%s`:launch\r\e[0Kci-update_revision"
ci-update_revision -d
echo -e "\e[0Ksection_end:`date +%s`:launch\r\e[0K"
