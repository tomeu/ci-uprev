# ci-uprev

[[_TOC_]]

This tool set has been devised to reduce the effort of the developers, by making
it as automatic as possible, the task of move forward the references to other 
projects inside one and help to improve the continuous integration by using 
newer versions of the dependencies.

This has started as a tool for [mesa's uprev in virglrenderer](https://gitlab.freedesktop.org/sergi/virglrenderer/-/blob/check_latest_mesa/.gitlab-ci/tools/check_latest_mesa/README.md), 
but it has grown until it have its own entity. 

It currently works to uprev mesa in virglrenderer, but a small refactor will 
allow it to be generic.

## Installation

One can start by looking to the [python environment](python.md) for this 
project and then use the setuptools to have it there.

With the dependencies of this module (better if it is in an environment, 
virtualenv or conda) one can use `setuptools` to install it:

```bash
python setup.py install
```

It can be also build to distribute with the option that better feeds your needs:

```bash
python setup.py {dist,bdist_wheel,bdist_egg}
```

## Usage

Once installed there are two entry points:
* [ci-update_revision](uprev/updaterevision/README.md)
* [ci-collate_results](uprev/collateresults/README.md)

## ToDo list

- [ ] extend from only "mesa uprev in virglrenderer"
  - Currently, extending for "piglit uprev in mesa"
  - Plan to extend for "virglrenderer uprev in mesa"

## Further information

For further information dive in the next [readme](uprev/README.md).