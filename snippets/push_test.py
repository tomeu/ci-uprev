import sys


def header(msg):
    print(f"\n{'*'*len(msg)}")
    print(f"{msg}")
    print(f"{'*'*len(msg)}\n")


def main():
    if len(sys.argv) != 2:
        print("It is necessary to provide the gitlab token")
        exit(-1)
    new_branch_name = "update-mesa-expectations"
    from uprev.updaterevision.gitlabwrapper import GitlabWrapper
    header(f"prepare virgl_obj")
    virgl_obj = GitlabWrapper(url="https://gitlab.freedesktop.org/",
                              project="sergi/virglrenderer", token=sys.argv[1],
                              debug=True)
    try:  # if the branch exist clean it for the current test
        _branch_obj = virgl_obj.branches.get(new_branch_name)
        _branch_obj.delete()
    except Exception:
        pass
    header(f"prepare mesa_obj")
    mesa_obj = GitlabWrapper(url="https://gitlab.freedesktop.org/",
                             project="mesa/mesa", debug=True)
    mesa_commit_hash = "6fc2622abd3c4d66742437e6fd4d8757ff65d74d"
    mesa_pipeline_id = "619985"
    from uprev.updaterevision.uprevpatch import PatchMesaInVirglrenderer
    header(f"prepare patch_obj")
    patch_obj = PatchMesaInVirglrenderer(
        origin_project_name=virgl_obj.project.path_with_namespace,
        origin_project_files=virgl_obj.files,
        origin_branch_name=virgl_obj.default_branch,
        revision_project_name=mesa_obj.project.path_with_namespace,
        mesa_commit=mesa_commit_hash, mesa_pipeline_id=mesa_pipeline_id,
        debug=True)
    from uprev.updaterevision.workingbranch import WorkingBranch
    header(f"prepare workingbranch_obj")
    workingbranch_obj = WorkingBranch(virgl_obj,
                                      mesa_commit_hash,
                                      mesa_pipeline_id,
                                      virgl_obj.default_branch,
                                      new_branch_name,
                                      "Not used in this test",
                                      patch_obj,
                                      debug=True)
    from uprev.collateresults import ResultsCollector
    header(f"collect results from a virgl pipeline")
    virgl_pipeline_id = 620059
    collector = ResultsCollector(
        url="https://gitlab.freedesktop.org/",
        project="sergi/virglrenderer",
        jobs_regex="^(piglit|deqp)-(gles|gl)-(virt|host)",
        pipeline=f"#{virgl_pipeline_id}", token=sys.argv[1],
        artifacts_set={'failures.csv'}, debug=True)
    expectation_outputs = collector.build_output(patch=True,
                                                 collect_objs=True)
    from uprev.collateresults.output.patch import Patch
    for output_obj in expectation_outputs:
        if isinstance(output_obj, Patch):
            expectationspatch_obj = output_obj
            break
    else:
        raise RuntimeError(f"No Patch object found")
    header(f"Update expectations")
    expectationspatch_obj.apply_patch(new_branch_name)


if __name__ == '__main__':
    main()
