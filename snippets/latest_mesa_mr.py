from gitlab import Gitlab, GitlabCreateError
from shutil import rmtree as shutil_rmtree
from tempfile import mkdtemp as tempfile_mkdtemp

GITLAB_URL = 'https://gitlab.freedesktop.org'
GITLAB_PROJECT_PATH = "sergi/virglrenderer"
branch_name = 'test001'
file_to_patch = '.gitlab-ci.yml'
patch_name = 'new_mesa_reference.patch'

from uprev.abstract.envvars import _get_token
GITLAB_TOKEN = _get_token()  # uses GITLAB_TOKEN[_FILE] envvar or returns None

# prepare gitlab objects
gitlab_obj = Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
project_obj = gitlab_obj.projects.get(GITLAB_PROJECT_PATH)

branches_manager_obj = project_obj.branches
files_obj = project_obj.files

# create a branch
# https://python-gitlab.readthedocs.io/en/stable/gl_objects/branches.html
try:
    branch_obj = branches_manager_obj.create(
        {'branch': branch_name, 'ref': project_obj.default_branch})
    # FIXME: it seems this branch creation has to come from a branch in the
    #  same project, to so use virgl/virglrenderer master, it has to first
    #  get that to the fork
except GitlabCreateError as exception:
    # "403: insufficient_scope" saw when use a readonly token
    print(f"{exception.response_code}: {exception.error_message}")
    exit(-1)

# FIXME: it will be better if no local files are needed.
#  How to apply a patch to the file directly in gitlab?
# create a tmp directory to get the branch
# temporary_storage = tempfile_mkdtemp()

# apply the patch to it
file_content = files_obj.get(file_to_patch, ref=branch_obj.encoded_id)
# with open(f"{temporary_storage}/{file_to_patch}", 'xb') as file_descriptor:
#     file_descriptor.write(file_content.decode())
file_modified = []
old_string = b'MESA_PIPELINE_ID: 581257'
new_string = b'MESA_PIPELINE_ID: 601352'
for line in file_content:
    if line.count(old_string):
        file_modified.append(line.replace(old_string, new_string))
    else:
        file_modified.append(line)
project_obj.update(file_modified, file_to_patch, branch_obj)
# push the changes to the branch

# create a MR to the master
#  (FIXME: start using forks one, but it will have to go to virgl/virglrenderer)
# https://python-gitlab.readthedocs.io/en/stable/gl_objects/merge_requests.html#project-merge-requests
mergerequest_obj = project_obj.mergerequests.create(
    {'source_branch': branch_name, 'target_branch': project_obj.default_branch,
     'title': 'ci: Uprev Mesa to the latest version',
     # 'labels': ['label1', 'label2'],
     })
mergerequest_obj.description = 'New description'
mergerequest_obj.save()

# remove the tmp
# shutil_rmtree(temporary_storage)
