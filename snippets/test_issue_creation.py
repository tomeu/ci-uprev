import sys
from uprev.updaterevision.enumeration import IssueType, IssueTypeExtraInfo
from uprev.updaterevision.issuecreation import IssueCreator
from time import sleep


def main():
    if len(sys.argv) != 2:
        print("It is necessary to provide the gitlab token")
        exit(-1)
    from uprev.updaterevision.gitlabwrapper import GitlabWrapper
    wrapper = GitlabWrapper(url="https://gitlab.freedesktop.org/",
                            project="sergi/virglrenderer", token=sys.argv[1])
    branch = wrapper.branches.get("issues_creation")
    # create a generic issue
    title = "[Test] generic issue generation"
    description = f"""This is a generic issue with nothing special on it.

It has been created to test the issue builder with out templating. It only 
has the description as plain text."""
    issue = IssueCreator(wrapper, branch.name, title, description, debug=True)
    issue.create()
    sleep(1)
    # create also one issue for each of the template possibilities
    title = "[Test] CI issue generation"
    _template = {IssueType.CI: {IssueTypeExtraInfo.pipeline_id: 629848}}
    ci_issue = IssueCreator(wrapper, branch.name, title,
                            description="After the templated part of the some "
                                        "final text",
                            template=_template, debug=True)
    ci_issue.create()
    sleep(1)
    title = "[Test] test Fail issue generation"
    _template = {IssueType.Fail: {IssueTypeExtraInfo.pipeline_id: 629848}}
    fail_issue = IssueCreator(wrapper, branch.name, title,
                              description="Just at the end of the templated "
                                          "part, some final words",
                              template=_template, debug=True)
    fail_issue.create()
    sleep(1)
    title = "[Test] test Unknown issue generation"
    _template = {IssueType.Unknown: {IssueTypeExtraInfo.pipeline_id: 629848}}
    unknown_issue = IssueCreator(wrapper, branch.name, title,
                                 description="For a issue from the unknown "
                                             "source of the problem, this "
                                             "field at the end could be very "
                                             "useful.",
                                 template=_template, debug=True)
    unknown_issue.create()


if __name__ == '__main__':
    main()
